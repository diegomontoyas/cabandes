/**
 * 
 */
package servlets;

import java.io.IOException;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.ConsultaDAO;
import dao.Criterio;
import dao.CriteriosPedidoAdminLocal;

public class ServletPedidosLocal extends HttpServlet
{
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
	procesar(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
	procesar(request, response);
    }

    private void procesar(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
	String filtro = request.getParameter("selectFiltros");
	String filtro2 = request.getParameter("valorFiltro");
	String filtro3 = request.getParameter("valorFiltro2");
	
	String btnFiltrar = request.getParameter("btnFiltrar");
	
	
	if (btnFiltrar != null && filtro != null && filtro2 != null && filtro3 != null )
	{
	    ConsultaDAO consultaDAO = new ConsultaDAO(this.getServletContext().getRealPath(this.getServletContext().getContextPath()));
	    int x = Integer.parseInt(filtro);
	    consultaDAO.visualizarLocales(x, filtro2, filtro3);
	}
	request.getRequestDispatcher("/WEB-INF/pedidosLocal.jsp").forward(request, response);
    }
}
