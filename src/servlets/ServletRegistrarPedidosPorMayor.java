package servlets;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import modelo.Cabandes;
import modelo.IItem;
import vos.ItemVO;
import dao.ConsultaDAO;
import dao.Criterio;
import dao.CriteriosProducto;

public class ServletRegistrarPedidosPorMayor extends HttpServlet
{
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
	procesar(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
	procesar(request, response);
    }

    private void procesar(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
	ConsultaDAO consultaDAO = new ConsultaDAO(this.getServletContext().getRealPath(this.getServletContext().getContextPath()));

	String filtro = request.getParameter("selectFiltros");
	String valor = request.getParameter("valorFiltro");
	String stringCriterio = request.getParameter("selectCriterios");
	String idAdminLocal = request.getParameter("idAdminLocal");

	String btnFiltrar = request.getParameter("btnFiltrar");
	String btnRealizarPedido = request.getParameter("btnRealizarPedido");

	ArrayList<IItem> itemsSeleccionados = new ArrayList<IItem>();

	if (btnFiltrar != null && filtro != null && valor != null && stringCriterio != null)
	{

	    Criterio fil = CriteriosProducto.darCriterioConDescripcion(filtro);
	    Criterio criterio = CriteriosProducto.darCriterioConDescripcion(stringCriterio);

	    if (fil.equals(CriteriosProducto.TIPO_PRODUCTO))
	    {
		consultaDAO.consultarExistenciasDeLosItemsConTipo(valor, criterio);
	    }

	    request.getRequestDispatcher("/WEB-INF/seleccionProductosPedido.jsp").forward(request, response);
	}
	else if (btnRealizarPedido != null)
	{
	    ArrayList<IItem> items = Cabandes.getInstance().darItemsUltimaConsulta();
	    String[] itemsCheckBox = request.getParameterValues("itemsSeleccionados");

	    for (String item : itemsCheckBox)
	    {
		ItemVO itemVo = new ItemVO();
		itemVo.ID = item;
		itemsSeleccionados.add(itemVo);
	    }

	    //consultaDAO.registrarVentaItemACompradorMayorista(itemsSeleccionados);

	    request.getRequestDispatcher("/WEB-INF/finalizacionRegistroPedido.jsp").forward(request, response);
	}
	else
	    request.getRequestDispatcher("/WEB-INF/seleccionProductosPedido.jsp").forward(request, response);
    }
}
