package servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.ConsultaDAO;

public class ServletBodegas extends HttpServlet
{
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
	procesar(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
	procesar(request, response);
    }

    private void procesar(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
	String btnFiltrar = request.getParameter("btnFiltrar");
	String tipo = request.getParameter("selectTipos");
	String porcentajeUtilizadoString = request.getParameter("porcentajeUtilizado");
	Integer porcentajeUtilizado = null;
	
	if (porcentajeUtilizadoString != null)
	{
	    porcentajeUtilizado = porcentajeUtilizadoString.equals("")? null : Integer.parseInt(porcentajeUtilizadoString);  
	}
	ConsultaDAO consultaDAO = new ConsultaDAO(this.getServletContext().getRealPath(this.getServletContext().getContextPath()));

	consultaDAO.consultarBodegas(tipo, porcentajeUtilizado);

	request.getRequestDispatcher("/WEB-INF/bodegas.jsp").forward(request, response);
    }
}
