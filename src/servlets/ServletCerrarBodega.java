/**
 * 
 */
package servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import modelo.Cabandes;
import dao.ConsultaDAO;
import dao.ListenerRespuestas;

/**
 * @author Diego
 * 
 */
public class ServletCerrarBodega extends HttpServlet implements ListenerRespuestas
{
    private static final long serialVersionUID = 1L;
    
    private String respuestaOtroComponente;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
	procesar(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
	procesar(request, response);
    }

    private synchronized void procesar(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
	ConsultaDAO consultaDAO = new ConsultaDAO(this.getServletContext().getRealPath(this.getServletContext().getContextPath()));

	String idBodega = request.getParameter("idSeleccionado");
	boolean propia = Boolean.parseBoolean(request.getParameter("propia"));

	if (idBodega != null)
	{
	    //Registrarse para esperar la notificacion
	    Cabandes.getInstance().setListenerRespuestas(this);
	    
	    String resultado = consultaDAO.cerrarBodega(idBodega, propia, true);
	    if (resultado.contains("no pudo"))
	    {
		try
		{
		    wait(5000);
		}
		catch (InterruptedException e)
		{
		    e.printStackTrace();
		}
	    }
	    if (respuestaOtroComponente != null) resultado = respuestaOtroComponente;
	    request.setAttribute("resultado", resultado);	    
	    request.getRequestDispatcher("/WEB-INF/cerrarBodega.jsp").forward(request, response);
	}
    }

    @Override
    public synchronized void respuestaRecibida(String respuesta)
    {
	this.respuestaOtroComponente = respuesta;
	notify();
    }
}
