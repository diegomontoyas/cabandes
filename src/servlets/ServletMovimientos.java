package servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.ConsultaDAO;
import dao.Criterio;
import dao.CriteriosMovimientos;

public class ServletMovimientos extends HttpServlet
{
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
	procesar(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
	procesar(request, response);
    }

    private void procesar(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
	ConsultaDAO consultaDAO = new ConsultaDAO(this.getServletContext().getRealPath(this.getServletContext().getContextPath()));

	String incluye = request.getParameter("selectIncluye");
	String filtroString = request.getParameter("selectFiltros");
	String valor = request.getParameter("valorFiltro");
	
	String fechaInicio = request.getParameter("fechaInicio");
	String fechaFin = request.getParameter("fechaFin");

	String btnFiltrar = request.getParameter("btnFiltrar");
	
	boolean noIncluye = incluye != null? incluye.equals("0"): false;

	if (filtroString != null && valor != null)
	{
	    Criterio filtro = CriteriosMovimientos.darCriterioConDescripcion(filtroString);

	    if (filtro.equals(CriteriosMovimientos.NOMBRE_PRODUCTO))
	    {
		consultaDAO.consultarHistorialMovimientosDeProducto(fechaInicio, fechaFin, valor, noIncluye);
	    }
	    else if (filtro.equals(CriteriosMovimientos.BODEGA_ORIGEN))
	    {
		consultaDAO.consultarHistorialMovimientosConBodegaOrigen(fechaInicio, fechaFin, valor, noIncluye);
	    }
	    else if (filtro.equals(CriteriosMovimientos.BODEGA_ORIGEN))
	    {
		consultaDAO.consultarHistorialMovimientosConBodegaDestino(fechaInicio, fechaFin, valor, noIncluye);
	    }
	    else if (filtro.equals(CriteriosMovimientos.LOCAL_ORIGEN))
	    {
		consultaDAO.consultarHistorialMovimientosConLocalOrigen(fechaInicio, fechaFin, valor, noIncluye);
	    }
	    else if (filtro.equals(CriteriosMovimientos.LOCAL_DESTINO))
	    {
		consultaDAO.consultarHistorialMovimientosConLocalDestino(fechaInicio, fechaFin, valor, noIncluye);
	    }
	    else if (filtro.equals(CriteriosMovimientos.FECHA))
	    {
		consultaDAO.consultarHistorialMovimientosConFecha(fechaInicio, fechaFin, valor, noIncluye);
	    }
	}
	request.getRequestDispatcher("/WEB-INF/movimientos.jsp").forward(request, response);
    }
}
