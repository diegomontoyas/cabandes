package servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.ConsultaDAO;

public class ServletRegistrarBodegas extends HttpServlet
{

    private static final long serialVersionUID = 1L;

    @Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
	procesar(request, response);
    }

    @Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
	procesar(request, response);
    }

    private void procesar(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
	String capacidad = request.getParameter("valorCapacidad");
	String tipo = request.getParameter("valorTipo");
	String btnFiltrar = request.getParameter("btnAgregar");

	if (capacidad != null && tipo != null)
	{
	    ConsultaDAO consultaDAO = new ConsultaDAO(this.getServletContext().getRealPath(this.getServletContext().getContextPath()));

	    int cap = Integer.parseInt(capacidad);
	    String x = consultaDAO.registrarBodega(tipo, cap);
	}

	request.getRequestDispatcher("/WEB-INF/registrarBodegas.jsp").forward(request, response);
    }
}
