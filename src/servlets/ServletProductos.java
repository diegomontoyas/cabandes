package servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.Cache;
import dao.ConsultaDAO;
import dao.Criterio;
import dao.CriteriosProducto;
import dao.OpcionesRegistroMovimientoProductos;

public class ServletProductos extends HttpServlet
{
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
	procesar(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
	procesar(request, response);
    }

    private void procesar(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
	System.out.println(this.getServletContext().getRealPath(this.getServletContext().getContextPath()));
	Cache.instance().limpiarInfoMovimientos();
	ConsultaDAO consultaDAO = new ConsultaDAO(this.getServletContext().getRealPath(this.getServletContext().getContextPath()));

	String filtroString = request.getParameter("selectFiltros");
	String valor = request.getParameter("valorFiltro");
	String stringCriterio = request.getParameter("selectCriterios");

	String btnFiltrar = request.getParameter("btnFiltrar");
	String btnEliminar = request.getParameter("btnEliminarProductosVencidos");

	if (filtroString != null && valor != null && stringCriterio != null)
	{

	    Criterio filtro = CriteriosProducto.darCriterioConDescripcion(filtroString);
	    Criterio criterio = CriteriosProducto.darCriterioConDescripcion(stringCriterio);

	    if (filtro.equals(CriteriosProducto.NOMBRE_PRODUCTO))
	    {
		consultaDAO.consultarExistenciasDeLosItemsConNombre(valor, criterio);
	    }
	    else if (filtro.equals(CriteriosProducto.FECHA_DE_EXPIRACION))
	    {
		consultaDAO.consultarExistenciasDeLosItemsConFechaDeExpiracion(valor, criterio);
	    }
	    else if (filtro.equals(CriteriosProducto.TIPO_PRODUCTO))
	    {
		consultaDAO.consultarExistenciasDeLosItemsConTipo(valor, criterio);
	    }
	    else if (filtro.equals(CriteriosProducto.IDENTIFICADOR_DE_BODEGA))
	    {
		consultaDAO.consultarExistenciasDeLosItemsConIdentificadorDeBodega(valor, criterio);
	    }
	    else if (filtro.equals(CriteriosProducto.IDENTIFICADOR_DE_LOCAL_DE_VENTAS))
	    {
		consultaDAO.consultarExistenciasDeLosItemsConIdentificadorDeLocal(valor, criterio);
	    }
	    else if (filtro.equals(CriteriosProducto.PRESENTACION))
	    {
		consultaDAO.consultarExistenciasDeLosItemsConPresentacion(valor, criterio);
	    }
	    request.getRequestDispatcher("/WEB-INF/productos.jsp").forward(request, response);
	}
	else if (btnEliminar != null)
	{
	    request.setAttribute("opcion", OpcionesRegistroMovimientoProductos.ELIMINACION_ITEMS_VENCIDOS);
	    request.setAttribute("itemsVencidos", consultaDAO.eliminarItemsVencidos());
	    request.getRequestDispatcher("/WEB-INF/finalizacionRegistroMovimientoProductos.jsp").forward(request, response);
	}
	else
	{
	    request.getRequestDispatcher("/WEB-INF/productos.jsp").forward(request, response);
	}
    }
}
