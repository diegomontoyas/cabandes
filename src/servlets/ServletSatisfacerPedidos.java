/**
 * 
 */
package servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.ConsultaDAO;

/**
 * @author Diego
 * 
 */
public class ServletSatisfacerPedidos extends HttpServlet
{
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
	procesar(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
	procesar(request, response);
    }

    private void procesar(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
	ConsultaDAO consultaDAO = new ConsultaDAO(this.getServletContext().getRealPath(this.getServletContext().getContextPath()));

	String idLocal = request.getParameter("idLocal");

	String numeroPedidoSeleccionado = request.getParameter("numPedido");

	if (numeroPedidoSeleccionado != null)
	{
	    boolean satisfecho = consultaDAO.satisfacerPedido(numeroPedidoSeleccionado);
	    request.getRequestDispatcher("/WEB-INF/pedidoSatisfecho.jsp").forward(request, response);
	}
    }
}
