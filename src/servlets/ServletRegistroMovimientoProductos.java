package servlets;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import modelo.IItem;
import vos.ItemVO;
import dao.Cache;
import dao.ConsultaDAO;
import dao.OpcionesRegistroMovimientoProductos;

public class ServletRegistroMovimientoProductos extends HttpServlet
{
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
	procesar(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
	procesar(request, response);
    }

    private void procesar(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
	ConsultaDAO consultaDAO = new ConsultaDAO(this.getServletContext().getRealPath(this.getServletContext().getContextPath()));

	if (Cache.instance().idSeleccionado == null) Cache.instance().idSeleccionado = request.getParameter("idSeleccionado");
	if (Cache.instance().opcion == null) Cache.instance().opcion = request.getParameter("selectOpciones");

	String btnEliminarItemsVencidos = request.getParameter("btnEliminarItemsVencidos");

	ArrayList<IItem> itemsSeleccionados = new ArrayList<IItem>();
	
	String opcion = Cache.instance().opcion;

	if (opcion != null)
	{
	    if (opcion.equals(OpcionesRegistroMovimientoProductos.VENTA_ITEM_LOCAL))
	    {
		consultaDAO.registrarVentaPresentacion(Cache.instance().idSeleccionado);
		request.getRequestDispatcher("/WEB-INF/finalizacionRegistroMovimientoProductos.jsp").forward(request, response);
	    }
	    else if (opcion.equals(OpcionesRegistroMovimientoProductos.VENTA_ITEMS_BODEGA_A_COMPRADOR_POR_MAYOR))
	    {
		consultaDAO.registrarVentaItemACompradorMayorista(Cache.instance().idSeleccionado);
		request.getRequestDispatcher("/WEB-INF/finalizacionRegistroMovimientoProductos.jsp").forward(request, response);
	    }
	    else if (opcion.equals(OpcionesRegistroMovimientoProductos.ENVIO_ITEM_DE_BODEGA_A_LOCAL_DE_VENTAS))
	    {
		Cache.instance().idLocalSeleccionado = request.getParameter("selectLocales");
		
		if (Cache.instance().idLocalSeleccionado == null)
		{
		    consultaDAO.consultarLocales();
		    request.getRequestDispatcher("/WEB-INF/registroMovimientoProductos.jsp").forward(request, response);
		}
		else
		{
		    consultaDAO.registrarMovimientoItemBodegaALocal(Cache.instance().idSeleccionado, Cache.instance().idLocalSeleccionado);
		    request.getRequestDispatcher("/WEB-INF/finalizacionRegistroMovimientoProductos.jsp").forward(request, response);
		}
		
	    }
	}
	else if (btnEliminarItemsVencidos != null)
	{
	    ArrayList<IItem> itemsVencidos = consultaDAO.eliminarItemsVencidos();
	    if (itemsVencidos == null) itemsVencidos = new ArrayList<IItem>();
	    
	    Cache.instance().itemsVencidos = itemsVencidos;
	    request.getRequestDispatcher("/WEB-INF/finalizacionRegistroMovimientoProductos.jsp").forward(request, response);
	}
	else
	{
	    String[] itemsCheckBox = request.getParameterValues("itemsSeleccionados");

	    if (itemsCheckBox != null)
	    {
		for (String item : itemsCheckBox)
		{
		    ItemVO itemVo = new ItemVO();
		    itemVo.ID = item;
		    itemsSeleccionados.add(itemVo);
		}
	    }

	    Cache.instance().itemsSeleccionados = itemsSeleccionados;
	    request.getRequestDispatcher("/WEB-INF/seleccionOpcionRegistroMovimientoProductos.jsp").forward(request, response);
	}
    }
}
