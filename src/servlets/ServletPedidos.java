/**
 * 
 */
package servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.ConsultaDAO;
import dao.Criterio;
import dao.CriteriosPedidoAdminLocal;

/**
 * @author Diego
 * 
 */
public class ServletPedidos extends HttpServlet
{
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
	procesar(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
	procesar(request, response);
    }

    private void procesar(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
	String filtro = request.getParameter("selectFiltros");
	String usuario = request.getParameter("usuario");
	String valor = request.getParameter("valorFiltro");
	String stringCriterio = request.getParameter("selectCriterios");

	String btnFiltrar = request.getParameter("btnFiltrar");

	if (btnFiltrar != null && filtro != null && valor != null && stringCriterio != null)
	{
	    ConsultaDAO consultaDAO = new ConsultaDAO(this.getServletContext().getRealPath(this.getServletContext().getContextPath()));

	    Criterio criterio = CriteriosPedidoAdminLocal.darCriterioConDescripcion(stringCriterio);

	    if (criterio.equals(CriteriosPedidoAdminLocal.COSTO))
	    {
		consultaDAO.consultarPedidosDelUsuarioConCosto(usuario, valor);
	    }

	    else if (criterio.equals(CriteriosPedidoAdminLocal.PRESENTACION))
	    {
		consultaDAO.consultarPedidosDelUsuarioConPresentacion(usuario, valor);
	    }

	    else if (criterio.equals(CriteriosPedidoAdminLocal.PRODUCTO))
	    {
		consultaDAO.consultarPedidosDelUsuarioConNombreDeProducto(usuario, valor);
	    }

	    else if (criterio.equals(CriteriosPedidoAdminLocal.SATISFECHO))
	    {
		if (valor.equals("si"))
		{
		    consultaDAO.consultarPedidosSatisfechosDelUsuario(usuario);
		}
		else
		{
		    consultaDAO.consultarPedidosPendientesDelUsuario(usuario);
		}
	    }
	    else if (criterio.equals(CriteriosPedidoAdminLocal.RANGO_FECHAS))
	    {
		String[] fechas = valor.split(",");
		if (fechas.length == 2)
		{
		    consultaDAO.consultarPedidosDelUsuarioRealizadosEntre(usuario, fechas[0], fechas[1]);
		}
	    }
	}
	request.getRequestDispatcher("/WEB-INF/pedidos.jsp").forward(request, response);
    }
}
