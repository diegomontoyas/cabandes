/**
 * 
 */
package servlets;

import java.io.IOException;

import javax.jms.JMSException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import modelo.Cabandes;
import dao.ConsultaDAO;

/**
 * @author Diego
 * 
 */
public class ServletPedidosCompartidos extends HttpServlet
{
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
	try {
		procesar(request, response);
	} catch (JMSException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
	try {
		procesar(request, response);
	} catch (JMSException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
    }

    private void procesar(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, JMSException
    {
    	ConsultaDAO consultaDAO = new ConsultaDAO(this.getServletContext().getRealPath(this.getServletContext().getContextPath()));
    	String iIdLocal = request.getParameter("selectFiltros");
    	int idLocal = 0;
    	if(iIdLocal != null)
    	{
    		idLocal = Integer.parseInt(iIdLocal);
    		Cabandes.getInstance().tenerTodosProductos(idLocal);
    	}
	
    	String[] productos = request.getParameterValues("check");
    	if(productos != null && idLocal != 0)
    	{
    		for(int i = 0; i < productos.length; i +=2)
    		{	
    			if(productos[i] != "")
    			{
    				String x = consultaDAO.revisarCapacidad(productos, idLocal, false);
    			}
    		}
    	}
    	request.getRequestDispatcher("/WEB-INF/pedidosCompartidos.jsp").forward(request, response);
    }
}
