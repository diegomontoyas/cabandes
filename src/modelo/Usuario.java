package modelo;

/**
 * <!-- begin-user-doc --> <!-- end-user-doc -->
 * 
 * @generated
 */

public class Usuario
{
    public String usuario;

    public String contrasenia;

    public String tipoUsuario;

    public String id;

    public String nombre;

    public String nacionalidad;

    public String direccion;

    public String correo;

    public int telefono;

    public String departamento;

    public int codigoPostal;

    public Usuario()
    {
	super();
    }

}
