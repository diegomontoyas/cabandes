package modelo;

import java.util.ArrayList;

/**
 * <!-- begin-user-doc --> <!-- end-user-doc -->
 * 
 * @generated
 */

public class Proveedor extends Usuario implements IProveedor
{

    public String tipoProducto;

    public int calificacion;

    public ArrayList<Oferta> ofertas;

    public ArrayList<PedidoProveedor> pedidos;

    public ArrayList<Item> items;

    public Proveedor(String tipoProducto, int calificacion, ArrayList<Oferta> ofertas, ArrayList<PedidoProveedor> pedidos,
	    ArrayList<Item> items)
    {
	super();
	this.tipoProducto = tipoProducto;
	this.calificacion = calificacion;
	this.ofertas = ofertas;
	this.pedidos = pedidos;
	this.items = items;
    }
}
