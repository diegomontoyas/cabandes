package modelo;

import java.util.Set;

public class AdministradorLocal extends Usuario
{
    public String tipoProducto;

    public Set<PedidoAdministradorLocal> pedidos;

    public AdministradorLocal(String tipoProducto, Set<PedidoAdministradorLocal> pedidos)
    {
	super();
	this.tipoProducto = tipoProducto;
	this.pedidos = pedidos;
    }

    public void administrar()
    {

    }

}
