package modelo;

import java.util.Date;
import java.util.Set;

/**
 * <!-- begin-user-doc --> <!-- end-user-doc -->
 * 
 * @generated
 */

public class PedidoProveedor implements IPedidoProveedor
{
    public int numeroPedido;

    public Date fechaEsperada;

    public boolean efectivo;

    public Set<Proveedor> proveedores;

    public Set<Oferta> ofertas;

    public Set<Item> items;

    public PedidoProveedor()
    {
	super();
    }

    @Override
    public int darNumeroPedido()
    {
	return 0;
    }

    @Override
    public String darFechaEsperada()
    {
	return null;
    }

}
