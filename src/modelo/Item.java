package modelo;

import java.util.Set;

/**
 * <!-- begin-user-doc --> <!-- end-user-doc -->
 * 
 * @generated
 */

public class Item implements IItem
{
    public String ID;

    public int unidades;

    public ILocal local;

    public Set<Presentacion> presentaciones;

    public Bodega bodega;

    public Producto infoProducto;

    public Set<Proveedor> proveedores;

    public String localBodega;

    public Item(int unidades, ILocal local, Set<Presentacion> presentaciones, Bodega bodega, Producto infoProducto,
	    Set<Proveedor> proveedores)
    {
	super();
	this.unidades = unidades;
	this.local = local;
	this.presentaciones = presentaciones;
	this.bodega = bodega;
	this.infoProducto = infoProducto;
	this.proveedores = proveedores;
    }

    public Item()
    {
    }

    @Override
    public String darID()
    {
	return ID;
    }

    @Override
    public String darNombre()
    {
	return null;
    }

    @Override
    public String darTipo()
    {
	return null;
    }

    @Override
    public int darCantidadUnidades()
    {
	return 0;
    }

    @Override
    public String darFechaVencimiento()
    {
	return null;
    }

    @Override
    public String esDeBodegaOLocal()
    {
	return localBodega;
    }

    @Override
    public Integer darPeso()
    {
	return null;
    }

    @Override
    public Integer darIDBodegaLocal()
    {
	return null;
    }

}
