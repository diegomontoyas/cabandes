package modelo;

import java.util.ArrayList;

import vos.BodegaVO;
import vos.LocalVO;

public interface ICabandes
{
    /**
     * Retorna los items generados por la última consulta de ConsultaDAO
     * 
     * @return IItems
     */

    public ArrayList<IItem> darItemsUltimaConsulta();

    /**
     * Retorna los pedidos de administrador de local generados por la última de
     * ConsultaDAO
     * 
     * @return IPedidosAdministradorLocal
     */
    public ArrayList<IPedidoAdministradorLocal> darPedidosAdministradorLocalUltimaConsulta();

    public ArrayList<BodegaVO> darBodegasUltimaConsulta();

    /**
     * Busca (pero no guarda!) y retorna las bodegas de un tipo en especial
     */
    public ArrayList<Bodega> darBodegasDeTipo(String tipo);

    /**
     * Busca (pero no guarda!) y retorna todos los locales de Cabandes
     */
    public ArrayList<LocalVO> darLocales();

    /**
     * Busca (pero no guarda!) y retorna el item correspondiente a un id en
     * especial
     */
    public IItem darItemConID(String id);

    /**
     * Registra la venta de un Item con el id dado y lo retorna SI EL ITEM NO ES
     * DE UN LOCAL DE VENTAS NO SE VENDE! ACTUALIZA LAS EXISTENCIAS EN LA BASE
     * DE DATOS SEGÚN CORRESPONDA
     */
    public IItem registrarVentaItem(String id);

    /**
     * Registra la venta de un conjunto de items cuyo id llega por parámetro a
     * un compradorMayorista y los retorna SI ALGUNO DE LOS ITEMS NO ES DE
     * BODEGA NO SE VENDEN! ACTUALIZA LAS EXISTENCIAS EN LAS BODEGAS DE LA BASE
     * DE DATOS SEGÚN CORRESPONDA
     */
    public ArrayList<IItem> registrarVentaItemsACompradorMayorista(ArrayList<IItem> items);

    /**
     * Busca (pero no guarda!) y retorna todos los compradores mayoristas
     */
    public ArrayList<CompradorPorMayor> darCompradoresMayoristas();

    /**
     * Elimina de Cabandes y retorna la lista de todos los productos que
     * vencieron
     * 
     * @return La lista de todos los items que fueron eliminados
     */
    public ArrayList<Item> eliminarItemsVencidos();

    /**
     * Genera un pedido de administrador local con los items que llegan por
     * parámetro Se calcula su precio y su fecha de entrega
     * 
     * @param itemsSeleccionados
     * @param idAdminLocal
     */
    public void generarPedidoAdminLocal(ArrayList<IItem> itemsSeleccionados, String idAdminLocal);

    /**
     * Satisface el pedido con el numero de pedido dado. LOS ITEMS SE DEBEN
     * ENVIAR AL LOCAL CORRESPONDIENTE SI NO SE PUEDE SATISFACER EL PEDIDO SE
     * DEBE GENERAR UNO AUXILIAR CON LOS PRODUCTOS FALTANTES. SE MIRA SI HAY
     * PEDIDOS DE PROVEEDORES EN CURSO CON LOS PRODUCTOS QUE FALTA, Y SI AÚN ES
     * NECESARIO SE GENERA UN PEDIDO DE PROVEEDOR A LOS PROVEEDORES
     * 
     * @param numeroPedidoSeleccionado
     * @param idLocal
     * @return True si fue satisfecho
     */
    public boolean satisfacerPedido(String numeroPedidoSeleccionado);

    public ArrayList<IPedidoProveedor> darPedidosNoEfectivosProveedor();

    /**
     * Cierra un pedido de proveedor dado por el numero de pedido IMPLICA PONER
     * COMO EFECTIVO EL PEDIDO Y ASIGNARLE *AL PROVEEDOR* ESTE PEDIDO QUE DEBERÁ
     * CUMPLIR (es decir, se le debe agregar al proveedor el pedido en la tabla
     * de PEDIDOS_PROVEEDOR_PROVEEDORES) EL PROVEEDOR SE ELIGE AUTOMÁTICAMENTE
     * BASADO EN LOS CRITERIOS QUE SE DECIDAN (CALIFICACIÓN Y TIPO DE PRODUCTO
     * POR EJEMPLO)
     * 
     * "Los parámetros para esta decisión incluyen los costos, las cantidades,
     * las fechas de entrega y de expiración del lote y la calificación de
     * calidad del proveedor." Según el enunciado...
     * 
     * @param string
     */
    public void cerrarPedidoProveedor(String string);

    /**
     * Crea una nueva oferta por parte de un proveedor hacia un pedido de
     * proveedor.
     * 
     * @param idProveedor
     * @param numeroPedido
     * @param cantidadAProveer
     * @param fechaEsperadaEntrega
     * @param costo
     */
    public void hacerOfertaPorParteDelProveedorAlPedidoDeProveedor(String idProveedor, String numeroPedido, String cantidadAProveer,
	    String fechaEsperadaEntrega, String costo);

    /**
     * Da la lista de proveedores del sistema
     * 
     * @return Retorna la lista de proveedores
     */
    public ArrayList<Proveedor> darProveedores();

    /**
     * Retorna el resultado del proceso de agregar la bodega
     * 
     * @return Retorna un String con el mensaje del resultado
     */
    public String resultadoAgregarBodega();

    public ArrayList<Usuario> darUsuarios();
    
    public void tenerLocales();

	public ArrayList<String> darTodosProductos();

}
