package modelo;

public interface IItem
{
    public String darID();

    public String darNombre();

    public String darTipo();

    public int darCantidadUnidades();

    public String darFechaVencimiento();

    public String esDeBodegaOLocal();

    public Integer darPeso();

    public Integer darIDBodegaLocal();
}
