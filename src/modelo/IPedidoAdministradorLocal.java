package modelo;

import java.util.Date;

public interface IPedidoAdministradorLocal
{

    public String darFechaEsperada();
    public String darNombreAdministrador();
    public boolean fueSatisfecho();
    public String darPesoPresentacion();

    public String darNombreProducto();
    public String darNumeroPedido();
    public int darIdComprador();
    public String darTipoComprador();
    public Date darFechaSolicitud();
    public String darFechaRealizacion();
}