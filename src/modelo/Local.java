package modelo;

import java.util.Set;

/**
 * <!-- begin-user-doc --> <!-- end-user-doc -->
 * 
 * @generated
 */

public class Local implements ILocal
{
    public int capacidad;

    public String tipo;

    public Set<Item> items;

    public Local(int capacidad, String tipo, Set<Item> items)
    {
	super();
	this.capacidad = capacidad;
	this.tipo = tipo;
	this.items = items;
    }

}
