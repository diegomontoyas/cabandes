package modelo;

import java.util.ArrayList;
import java.util.Hashtable;

import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.QueueReceiver;
import javax.jms.QueueSender;
import javax.jms.QueueSession;
import javax.jms.TextMessage;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

import vos.BodegaVO;
import vos.LocalVO;
import vos.MovimientoVO;
import vos.PedidoVO;
import vos.ProductoVO;
import dao.ConsultaDAO;
import dao.ListenerRespuestas;
import dao.Mensajes;

public class Cabandes implements ICabandes
{
    ListenerRespuestas listenerRespuestas;
    
    public ListenerRespuestas getListenerRespuestas()
    {
        return listenerRespuestas;
    }

    public void setListenerRespuestas(ListenerRespuestas listenerRespuestas)
    {
        this.listenerRespuestas = listenerRespuestas;
    }

    private ConsultaDAO consultaDAO = new ConsultaDAO("/Users/diegofarfan/Documents/workspace/.metadata/.plugins/org.jboss.ide.eclipse.as.core/jboss-4.2.3.GA/deploy/Cabandes.war");
    
    private static Cabandes instancia;
    int counter = 0;
    
    public static Cabandes getInstance()
    {
	if (instancia == null)
	    instancia = new Cabandes();
	return instancia;
    }
    
    public ArrayList<IItem> itemsUltimaConsulta = new ArrayList<IItem>();
    public ArrayList<IPedidoAdministradorLocal> pedidosUltimaConsulta = new ArrayList<IPedidoAdministradorLocal>();
    public ArrayList<BodegaVO> bodegasUltimaConsulta = new ArrayList<BodegaVO>();
    public ArrayList<Proveedor> darProveedores = new ArrayList<Proveedor>();
    public String result = new String();
    public ArrayList<Usuario> darUsuarios = new ArrayList<Usuario>();
    public ArrayList<LocalVO> localesUltimaConsulta = new ArrayList<LocalVO>();
    public ArrayList<MovimientoVO> movimientosUltimaConsulta = new ArrayList<MovimientoVO>();
    public ArrayList<PedidoVO> pedidoLocal;
    public ArrayList<String> todosProductos;
    
    /**
     * Fuente de datos del otro grupo
     */
    private DataSource dataSourceExterno;
    
    private DataSource dataSourceInterno;
    
    private QueueConnection queueConnectionSolicitudesInterna;
    
    private QueueConnection queueConnectionRespuestasInterna;

    private QueueConnection queueConnectionSolicitudesExterna;
 
    private QueueConnection queueConnectionRespuestasExterna;
    
    private InitialContext contexto;
    
    private javax.jms.Connection jmsConnection;

    /**
     * Colas de mensajes
     */
    private Queue colaSolicitudesInterna;
    
    private Queue colaRespuestasInterna;
    
    private Queue colaSolicitudesExterna;
    
    private Queue colaRespuestasExterna;
    
    private ConnectionFactory connectionFactory;
    
    private QueueSession queueSessionRespuestasInterna;
    
    private QueueSession queueSessionSolicitudesExterna;

    public ArrayList<ProductoVO> productosMasDinamicosUltimaConsulta = new ArrayList<ProductoVO>();

	public String resultadoMierdero;
	public String afan;
    
    public Cabandes()
    {
	try
	{	    
	    Hashtable<String, String> environment = new Hashtable<String, String>();
	    environment.put(Context.INITIAL_CONTEXT_FACTORY, "org.jnp.interfaces.NamingContextFactory");
	    environment.put(Context.PROVIDER_URL, "jnp://localhost:1099");
	    environment.put(Context.URL_PKG_PREFIXES,  "org.jboss.naming:org.jnp.interfaces");
	    
	    contexto = new InitialContext(environment);
	    
	    dataSourceInterno = (DataSource) contexto.lookup("java:XAChieInterno");
	    dataSourceExterno = (DataSource) contexto.lookup("java:XAChieExterno");
	    
	    QueueConnectionFactory queueConnectionFactory = ( QueueConnectionFactory )contexto.lookup( "ConnectionFactory" );
	    
	    connectionFactory = (ConnectionFactory) contexto.lookup("java:JmsXA");
	    
	    colaSolicitudesInterna = (Queue) contexto.lookup("queue/colaSolicitudesInterna");
	    colaRespuestasInterna = (Queue) contexto.lookup("queue/colaRespuestasInterna");
	    
	    colaSolicitudesExterna = (Queue) contexto.lookup("queue/colaSolicitudesExterna");
	    colaRespuestasExterna = (Queue) contexto.lookup("queue/colaRespuestasExterna");    	    
	    
	    //Recibir solicitudes
	    
	    queueConnectionSolicitudesInterna = queueConnectionFactory.createQueueConnection( );
	    QueueSession queueSessionSolicitudesInterna = queueConnectionSolicitudesInterna.createQueueSession( false, QueueSession.AUTO_ACKNOWLEDGE );
	    QueueReceiver queueReceiverSolicitudesInterna = queueSessionSolicitudesInterna.createReceiver( colaSolicitudesInterna );
	    queueReceiverSolicitudesInterna.setMessageListener(new MessageListener()
	    {
	        @Override
	        public void onMessage(Message message)
	        {
	            solicitudRecibida(message);
	        }
	    });
	    queueConnectionSolicitudesInterna.start( );
	    
	    //Recibir Respuestas
	    
	    queueConnectionRespuestasExterna = queueConnectionFactory.createQueueConnection( );
	    QueueSession queueSessionRespuestasExterna = queueConnectionRespuestasExterna.createQueueSession( false, QueueSession.AUTO_ACKNOWLEDGE );
	    QueueReceiver queueReceiverRespuestasExterna = queueSessionRespuestasExterna.createReceiver( colaRespuestasExterna );
	    queueReceiverRespuestasExterna.setMessageListener(new MessageListener()
	    {
	        @Override
	        public void onMessage(Message message)
	        {
	            respuestaRecibida(message);
	        }
	    });
	    queueConnectionRespuestasExterna.start( );
	    
	    //Enviar respuestas
	    
	    queueConnectionRespuestasInterna = queueConnectionFactory.createQueueConnection( );
	    queueSessionRespuestasInterna = queueConnectionRespuestasInterna.createQueueSession( false, QueueSession.AUTO_ACKNOWLEDGE );
	    queueConnectionRespuestasInterna.start( );
	    
	    //Enviar Solicidudes

	    queueConnectionSolicitudesExterna = queueConnectionFactory.createQueueConnection( );
	    queueSessionSolicitudesExterna = queueConnectionSolicitudesExterna.createQueueSession( false, QueueSession.AUTO_ACKNOWLEDGE );
	    queueConnectionSolicitudesExterna.start( );
	}
	catch (Exception e)
	{
	    e.printStackTrace();
	}
    }
    
    public void enviarSolicitud( String mensaje ) throws JMSException
    {
        QueueSender queueSender = queueSessionSolicitudesExterna.createSender( colaSolicitudesExterna );
        TextMessage textMessage = queueSessionSolicitudesExterna.createTextMessage( mensaje );
        queueSender.send( textMessage );
        queueSender.close( );
    }
    
    public void enviarRespuesta( String mensaje ) throws JMSException
    {
        QueueSender queueSender = queueSessionRespuestasInterna.createSender( colaRespuestasInterna );
        TextMessage textMessage = queueSessionRespuestasInterna.createTextMessage( mensaje );
        queueSender.send( textMessage );
        queueSender.close( );
    }
   
    public void solicitudRecibida(Message message)
    {	
	try
	{
	    TextMessage textMessage = (TextMessage) message;
	    String mensaje = textMessage.getText();
	    String[] componentes = mensaje.split(":");
	    
	    if (componentes[0].equals(Mensajes.CERRAR_BODEGA))
	    {
		String id = componentes[1];
		String respuesta = consultaDAO.cerrarBodega(id, true, false);
		enviarRespuesta(Mensajes.CERRAR_BODEGA+":"+id+":"+respuesta);
	    }   
	    else if (componentes[0].equals(Mensajes.CONSULTAR_BODEGAS))
	    {
		consultaDAO.consultarBodegas(null, null);
		
		String respuesta = Mensajes.CONSULTAR_BODEGAS+":";
		
		for (BodegaVO bodega : bodegasUltimaConsulta)
		{
		    respuesta += bodega.id+"-"+bodega.capacidadTotal+"-"+bodega.porcentajeUtilizado+":";
		}
		respuesta = respuesta.substring(0, respuesta.length()-1);
		
		enviarRespuesta(respuesta);
	    }  
	    else if (componentes[0].equals(Mensajes.PRODUCTOS_MAS_DINAMICOS))
	    {
		consultaDAO.consultarProductosMasDinamicos(componentes[1], componentes[2]);
		
		String respuesta = Mensajes.PRODUCTOS_MAS_DINAMICOS+":";
		
		for (ProductoVO productoVO : productosMasDinamicosUltimaConsulta)
		{
		    respuesta += productoVO.nombre+"-"+productoVO.numMovimientos+":";
		}
		respuesta = respuesta.substring(0, respuesta.length()-1);
		
		enviarRespuesta(respuesta);
	    }  
	    else if (componentes[0].equals(Mensajes.CONSULTAR_MOVIMIENTOS))
	    {
		if (componentes [2].equals("BODEGA_ORIGEN"))
		{
		    consultaDAO.consultarHistorialMovimientosConBodegaOrigen(componentes[4], componentes[5], componentes[3], componentes[1].equals("NO"));
		    
		    String respuesta = Mensajes.CONSULTAR_MOVIMIENTOS+":";
		    
		    for (MovimientoVO movimientoVO : movimientosUltimaConsulta)
		    {
			respuesta += movimientoVO.nombreProducto+"-"+movimientoVO.bodegaOrigen+"-"+movimientoVO.bodegaDestino+"-"+movimientoVO.localOrigen+"-"+movimientoVO.localDestino+"-"+movimientoVO.fecha;
		    }
		    respuesta = respuesta.substring(0, respuesta.length()-1);
		    
		    enviarRespuesta(respuesta);
		}
		
		else if (componentes[0].equals(Mensajes.SATISFACER_PEDIDO))
		{			
			String[] p = new String[componentes.length];
			for(int i = 0; i <componentes.length; i +=2)
			{
				String a = componentes[i];
				String[] b = a.split("-");
				String nombreProducto = componentes[0];
				String cantidadesPendientes = componentes[1];
				p[i] = nombreProducto;
				p[i+1] = cantidadesPendientes;				
			}
				String respuesta = "";
				String rta = consultaDAO.revisarCapacidad(p, 5, true);
				if(rta.contains("localmente"))
				{
					respuesta = Mensajes.CONSULTAR_MOVIMIENTOS+":";
					
				}
				else
				{
					respuesta = Mensajes.CONSULTAR_MOVIMIENTOS+":" + darAfan();
				}
				
				
				enviarRespuesta(respuesta);
		}
		
		else if (componentes [2].equals("BODEGA_DESTINO"))
		{
		    consultaDAO.consultarHistorialMovimientosConBodegaDestino(componentes[4], componentes[5], componentes[3], componentes[1].equals("NO"));
		    
		    String respuesta = Mensajes.CONSULTAR_MOVIMIENTOS+":";
		    
		    for (MovimientoVO movimientoVO : movimientosUltimaConsulta)
		    {
			respuesta += movimientoVO.nombreProducto+"-"+movimientoVO.bodegaOrigen+"-"+movimientoVO.bodegaDestino+"-"+movimientoVO.localOrigen+"-"+movimientoVO.localDestino+"-"+movimientoVO.fecha;
		    }
		    respuesta = respuesta.substring(0, respuesta.length()-1);
		    
		    enviarRespuesta(respuesta);
		}
		else if (componentes [2].equals("LOCAL_ORIGEN"))
		{
		    consultaDAO.consultarHistorialMovimientosConLocalOrigen(componentes[4], componentes[5], componentes[3], componentes[1].equals("NO"));
		    
		    String respuesta = Mensajes.CONSULTAR_MOVIMIENTOS+":";
		    
		    for (MovimientoVO movimientoVO : movimientosUltimaConsulta)
		    {
			respuesta += movimientoVO.nombreProducto+"-"+movimientoVO.bodegaOrigen+"-"+movimientoVO.bodegaDestino+"-"+movimientoVO.localOrigen+"-"+movimientoVO.localDestino+"-"+movimientoVO.fecha;
		    }
		    respuesta = respuesta.substring(0, respuesta.length()-1);
		    
		    enviarRespuesta(respuesta);
		}
		else if (componentes [2].equals("LOCAL_DESTINO"))
		{
		    consultaDAO.consultarHistorialMovimientosConLocalDestino(componentes[4], componentes[5], componentes[3], componentes[1].equals("NO"));
		    
		    String respuesta = Mensajes.CONSULTAR_MOVIMIENTOS+":";
		    
		    for (MovimientoVO movimientoVO : movimientosUltimaConsulta)
		    {
			respuesta += movimientoVO.nombreProducto+"-"+movimientoVO.bodegaOrigen+"-"+movimientoVO.bodegaDestino+"-"+movimientoVO.localOrigen+"-"+movimientoVO.localDestino+"-"+movimientoVO.fecha;
		    }
		    respuesta = respuesta.substring(0, respuesta.length()-1);
		    
		    enviarRespuesta(respuesta);
		}
		else if (componentes [2].equals("PRODUCTO"))
		{
		    consultaDAO.consultarHistorialMovimientosDeProducto(componentes[4], componentes[5], componentes[3], componentes[1].equals("NO"));
		    
		    String respuesta = Mensajes.CONSULTAR_MOVIMIENTOS+":";
		    
		    for (MovimientoVO movimientoVO : movimientosUltimaConsulta)
		    {
			respuesta += movimientoVO.nombreProducto+"-"+movimientoVO.bodegaOrigen+"-"+movimientoVO.bodegaDestino+"-"+movimientoVO.localOrigen+"-"+movimientoVO.localDestino+"-"+movimientoVO.fecha;
		    }
		    respuesta = respuesta.substring(0, respuesta.length()-1);
		    
		    enviarRespuesta(respuesta);
		}
		
	    }  
	}
	catch (JMSException e)
	{
	    e.printStackTrace();
	}
    }
    
    public void respuestaRecibida(Message message)
    {
	try
	{
	    TextMessage textMessage = (TextMessage) message;
	    String mensaje = textMessage.getText();
	    notificarListener(mensaje);
	}
	catch (JMSException e)
	{
	    e.printStackTrace();
	}
    }
    
    public void notificarListener (String respuesta)
    {	
	if (listenerRespuestas != null) listenerRespuestas.respuestaRecibida(respuesta);
    }
    
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    @Override
    public ArrayList<IItem> darItemsUltimaConsulta()
    {
	return itemsUltimaConsulta;
    }
    
    @Override
    public ArrayList<IPedidoAdministradorLocal> darPedidosAdministradorLocalUltimaConsulta()
    {
	return pedidosUltimaConsulta;
    }
    
    @Override
    public ArrayList<LocalVO> darLocales()
    {
	return localesUltimaConsulta;
    }
    
    @Override
    public ArrayList<CompradorPorMayor> darCompradoresMayoristas()
    {
	return null;
    }
    
    @Override
    public void generarPedidoAdminLocal(ArrayList<IItem> itemsSeleccionados, String idAdminLocal)
    {
	String m;
	return;
    }
    
    @Override
    public boolean satisfacerPedido(String numeroPedidoSeleccionado)
    {
	return false;
    }
    
    @Override
    public ArrayList<IPedidoProveedor> darPedidosNoEfectivosProveedor()
    {
	return new ArrayList<IPedidoProveedor>();
    }
    
    @Override
    public void cerrarPedidoProveedor(String string)
    {
    }
    
    @Override
    public void hacerOfertaPorParteDelProveedorAlPedidoDeProveedor(String idProveedor, String numeroPedido, String cantidadAProveer,
	    String fechaEsperadaEntrega, String costo)
    {
    }
    
    @Override
    public ArrayList<BodegaVO> darBodegasUltimaConsulta()
    {
	return bodegasUltimaConsulta;
    }
    
    @Override
    public ArrayList<Proveedor> darProveedores()
    {
	return darProveedores;
    }
    
    @Override
    public ArrayList<Usuario> darUsuarios()
    {
	return darUsuarios;
    }
    
    @Override
    public String resultadoAgregarBodega()
    {
	return result;
    }
    
    public ArrayList<PedidoVO> pedidosLocal()
    {
	if(counter == 0)
	{
	    pedidoLocal = new ArrayList<PedidoVO>();
	    counter = 1;
	    return pedidoLocal;
	}
	return pedidoLocal;
    }
    
    public void tenerLocales()
    {
    	consultaDAO.consultarLocales();
    }
    
    public String darAfan()
    {
    	if(!afan.equals(""))
    	{
    		return afan;
    	}
    	return null;
    }
    
    public void tenerTodosProductos(int id)
    {	
    	consultaDAO.obtenerProductosDeTipo(id);
    }
    
    @Override
    public ArrayList<String> darTodosProductos()
    {
    	return todosProductos;
    }
    
    public String darResultadoMierdero()
    {
    	return resultadoMierdero;
    }
    
    @Override
    public ArrayList<Bodega> darBodegasDeTipo(String tipo) {
	// TODO Auto-generated method stub
	return null;
    }
    
    @Override
    public IItem darItemConID(String id) {
	// TODO Auto-generated method stub
	return null;
    }
    
    @Override
    public IItem registrarVentaItem(String id) {
	// TODO Auto-generated method stub
	return null;
    }
    
    @Override
    public ArrayList<IItem> registrarVentaItemsACompradorMayorista(
	    ArrayList<IItem> items) {
	// TODO Auto-generated method stub
	return null;
    }
    
    @Override
    public ArrayList<Item> eliminarItemsVencidos() {
	// TODO Auto-generated method stub
	return null;
    }
    
}
