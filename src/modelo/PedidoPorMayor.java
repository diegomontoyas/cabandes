package modelo;

import java.util.Set;

/**
 * <!-- begin-user-doc --> <!-- end-user-doc -->
 * 
 * @generated
 */

public class PedidoPorMayor implements IPedidoPorMayor
{

    public int numeroPedido;

    public CompradorPorMayor compradorPorMayor;

    public Set<Item> items;

    public PedidoPorMayor()
    {
	super();
    }

}
