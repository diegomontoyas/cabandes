package modelo;

public interface IPedidoProveedor
{
    public int darNumeroPedido();

    public String darFechaEsperada();
}