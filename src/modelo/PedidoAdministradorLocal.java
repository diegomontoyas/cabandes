package modelo;

import java.util.Date;
import java.util.Set;

/**
 * <!-- begin-user-doc --> <!-- end-user-doc -->
 * 
 * @generated
 */

public class PedidoAdministradorLocal implements IPedidoAdministradorLocal
{
    public Date fechaLlegada;

    public double costoTotal;

    public Set<Item> items;

    public AdministradorLocal administradorLocal;

    public PedidoAdministradorLocal()
    {
	super();
    }

    @Override
    public String darNumeroPedido()
    {
	return "";
    }

    @Override
    public String darFechaEsperada()
    {
	return null;
    }

    @Override
    public String darFechaRealizacion()
    {
	return null;
    }

    @Override
    public String darNombreAdministrador()
    {
	return null;
    }

    @Override
    public boolean fueSatisfecho()
    {
	return false;
    }

    @Override
    public String darNombreProducto()
    {
	return null;
    }

    @Override
    public String darPesoPresentacion()
    {
	return null;
    }

	@Override
	public int darIdComprador() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String darTipoComprador() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Date darFechaSolicitud() {
		// TODO Auto-generated method stub
		return null;
	}
}
