package modelo;

import java.util.Date;

/**
 * <!-- begin-user-doc --> <!-- end-user-doc -->
 * 
 * @generated
 */

public class Producto implements IProducto
{
    public int maximaCantidadUnidad;

    public String tipo;

    public Date fechaVencimiento;

    public boolean refrigerado;

    public String nombre;

    public Producto(int maximaCantidadUnidad, String tipo, Date fechaVencimiento, boolean refrigerado, String nombre)
    {
	super();
	this.maximaCantidadUnidad = maximaCantidadUnidad;
	this.tipo = tipo;
	this.fechaVencimiento = fechaVencimiento;
	this.refrigerado = refrigerado;
	this.nombre = nombre;
    }
}
