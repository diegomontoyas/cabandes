package modelo;

import java.util.Set;

/**
 * <!-- begin-user-doc --> <!-- end-user-doc -->
 * 
 * @generated
 */

public class Bodega
{

    public int capacidad;

    public String tipo;

    public Set<Item> items;

    public Bodega()
    {
	super();
    }

}
