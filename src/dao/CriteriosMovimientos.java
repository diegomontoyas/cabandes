package dao;

import java.lang.reflect.Field;
import java.util.ArrayList;

public class CriteriosMovimientos
{
    public static final Criterio NOMBRE_PRODUCTO = new Criterio("Nombre de producto", "P.NOMBRE");

    public static final Criterio BODEGA_ORIGEN = new Criterio("Bodega origen", "H.BODEGA_ORIGEN");
    
    public static final Criterio BODEGA_DESTINO = new Criterio("Bodega destino", "H.BODEGA_DESTINO");
    
    public static final Criterio LOCAL_ORIGEN = new Criterio("Local origen", "H.LOCAL_ORIGEN");
    
    public static final Criterio LOCAL_DESTINO = new Criterio("Local destino", "H.LOCAL_DESTINO");

    public static final Criterio FECHA = new Criterio("A partir de fecha", "H.FECHA");

    public static ArrayList<Criterio> darCriterios()
    {
	Field[] fields = CriteriosMovimientos.class.getDeclaredFields();
	ArrayList<Criterio> criterios = new ArrayList<Criterio>();
	CriteriosMovimientos instancia = new CriteriosMovimientos();

	for (Field field : fields)
	    try
	    {
		criterios.add((Criterio) field.get(instancia));
	    }
	    catch (IllegalArgumentException e)
	    {
		e.printStackTrace();
	    }
	    catch (IllegalAccessException e)
	    {
		e.printStackTrace();
	    }
	return criterios;
    }

    public static Criterio darCriterioConDescripcion(String descripcion)
    {
	ArrayList<Criterio> criterios = darCriterios();

	for (Criterio criterio : criterios)
	    if (criterio.toString().equals(descripcion))
		return criterio;

	return null;
    }
}
