package dao;

import java.lang.reflect.Field;
import java.util.ArrayList;

public class OpcionesRegistroMovimientoProductos
{
    public static final String ENTREGA_ITEM_POR_PROVEEDOR = "Entrega de algun producto por parte de un proveedor";

    public static final String ENVIO_ITEM_DE_BODEGA_A_LOCAL_DE_VENTAS = "Envio de producto desde una bodega a un local de ventas";

    public static final String VENTA_ITEM_LOCAL = "Venta de un producto en un local de ventas";

    public static final String VENTA_ITEMS_BODEGA_A_COMPRADOR_POR_MAYOR = "Venta de un producto de bodega a un comprador por mayor";

    public static final String ELIMINACION_ITEMS_VENCIDOS = "Eliminacion de todos los productos que estén vencidos a la fecha";

    public static ArrayList<String> darOpciones()
    {
	Field[] fields = OpcionesRegistroMovimientoProductos.class.getDeclaredFields();
	ArrayList<String> opciones = new ArrayList<String>();
	OpcionesRegistroMovimientoProductos instancia = new OpcionesRegistroMovimientoProductos();

	for (Field field : fields)
	    try
	    {
		opciones.add((String) field.get(instancia));
	    }
	    catch (IllegalArgumentException e)
	    {
		e.printStackTrace();
	    }
	    catch (IllegalAccessException e)
	    {
		e.printStackTrace();
	    }
	return opciones;
    }
}
