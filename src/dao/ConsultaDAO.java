/**
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Universidad de los Andes (Bogot� - Colombia)
 * $Id: ConsultaDAO.java,v 1.10 
 * Departamento de Ingenier�a de Sistemas y Computaci�n 
 *
 * Ejercicio: VideoAndes
 * Autor: Juan Diego Toro - 1-Marzo-2010
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */
package dao;

import java.io.File;
import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.Hashtable;
import java.util.Properties;
import java.util.TreeSet;

import javax.jms.JMSException;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import modelo.Cabandes;
import modelo.IItem;
import modelo.IPedidoAdministradorLocal;
import modelo.Item;
import modelo.Oferta;
import modelo.PedidoProveedor;
import modelo.Producto;
import modelo.Proveedor;
import modelo.Usuario;
import vos.BodegaVO;
import vos.ItemVO;
import vos.LocalVO;
import vos.MovimientoVO;
import vos.PedidoVO;
import vos.ProductoVO;

/**
 * Clase ConsultaDAO, encargada de hacer las consultas b�sicas para el cliente
 */
public class ConsultaDAO implements ListenerRespuestas
{
    public String path;

    // ----------------------------------------------------
    // Constantes
    // ----------------------------------------------------
    /**
     * ruta donde se encuentra el archivo de conexi�n.
     */
    private final String ARCHIVO_CONEXION = "/conexion.properties";

    // ----------------------------------------------------
    // Atributos
    // ----------------------------------------------------

    /**
     * conexion con la base de datos
     */
    public Connection conexion;
    
    /**
     * nombre del usuario para conectarse a la base de datos.
     */
    private String usuario;

    /**
     * clave de conexi�n a la base de datos.
     */
    private String clave;

    /**
     * URL al cual se debe conectar para acceder a la base de datos.
     */
    private String urlConexion;

    /**
     * Tiempo que tarda la ejecución de la sentencia
     */
    public long tiempoFinal;

    /**
     * Tamaño de la consulta
     */
    public int tamanho; 
    
    private String respuestaOtroComponente;
    
    private InitialContext contexto;
    
    private ArrayList<String> listaPresentaciones;
    
    /**
     * constructor de la clase. No inicializa ningun atributo.
     */
    public ConsultaDAO(String path)
    {
	inicializar(path.replace("/Cabandes.war/Cabandes", "/Cabandes.war"));
	
	Hashtable<String, String> environment = new Hashtable<String, String>();
	environment.put(Context.INITIAL_CONTEXT_FACTORY, "org.jnp.interfaces.NamingContextFactory");
	environment.put(Context.PROVIDER_URL, "jnp://localhost:1099");
	environment.put(Context.URL_PKG_PREFIXES,  "org.jboss.naming:org.jnp.interfaces");
	
	try
	{
	    contexto = new InitialContext(environment);
	}
	catch (NamingException e)
	{
	    e.printStackTrace();
	}
    }
    
    // -------------------------------------------------
    // M�todos
    // -------------------------------------------------

    /**
     * obtiene ls datos necesarios para establecer una conexion Los datos se
     * obtienen a partir de un archivo properties.
     * 
     * @param path
     *            ruta donde se encuentra el archivo properties.
     */
    public void inicializar(String path)
    {
	try
	{
	    File arch = new File(path + ARCHIVO_CONEXION);
	    Properties prop = new Properties();
	    FileInputStream in = new FileInputStream(arch);

	    prop.load(in);
	    in.close();

	    urlConexion = prop.getProperty("url");

	    usuario = prop.getProperty("usuario");
	    clave = prop.getProperty("clave");

	    final String driver = prop.getProperty("driver");
	    Class.forName(driver);	    
	}
	catch (Exception e)
	{
	    e.printStackTrace();
	}
    }

    /**
     * M�todo que se encarga de crear la conexi�n con el Driver Manager a partir
     * de los parametros recibidos.
     * 
     * @param url
     *            direccion url de la base de datos a la cual se desea conectar
     * @param usuario
     *            nombre del usuario que se va a conectar a la base de datos
     * @param clave
     *            clave de acceso a la base de datos
     * @throws SQLException
     *             si ocurre un error generando la conexi�n con la base de
     *             datos.
     */
    public void establecerConexion() throws SQLException
    {
	try
	{
	    //conexion = MonProxyFactory.monitor( DriverManager.getConnection(urlConexion, usuario, clave) );	    
	    conexion = DriverManager.getConnection(urlConexion, usuario, clave);
	    
	}
	catch (SQLException exception)
	{
	    exception.printStackTrace();
	}
    }

    /**
     * Cierra la conexi�n activa a la base de datos. Adem�s, con=null.
     * 
     * @param con
     *            objeto de conexi�n a la base de datos
     * @throws SistemaCinesException
     *             Si se presentan errores de conexi�n
     */
    public void closeConnection(Connection connection) throws Exception
    {
	try
	{
	    connection.setAutoCommit(true);
	    connection.close();
	    connection = null;
	}
	catch (SQLException exception)
	{

	}
    }

    // ---------------------------------------------------
    // M�todos asociados a los casos de uso: Consulta
    // ---------------------------------------------------

    // El criterio que se recibe es una de las constantes de la clase
    // CriteriosOrdenamiento

    private ResultSet executeQuery(String sql)
    {
	PreparedStatement prepStmt = null;

	try
	{
	    establecerConexion();
	    prepStmt = conexion.prepareStatement(sql);

	    ResultSet rs = prepStmt.executeQuery(sql);

	    return rs;
	}
	catch (SQLException e)
	{
	    e.printStackTrace();
	    System.out.println(sql);
	}
	finally
	{
	    if (prepStmt != null)
		try
		{
		    prepStmt.close();
		}
		catch (SQLException exception)
		{

		}
	    try
	    {
		closeConnection(conexion);
	    }
	    catch (Exception e)
	    {
		e.printStackTrace();
	    }
	}
	return null;
    }

    // LOS RESULTADOS DE LAS CONSULTAS DEBEN QUEDAR REFLEJADAS EN EL MODELO!!
    // PARA QUE LA INTERFAZ PUEDA PEDIR LAS COSAS A LA CLASE
    // CABANDES DESPUÉS DE CADA CONSULTA

    /**
     * Consulta las existencias de un producto con un nombre dado
     * 
     * @param nombre
     *            Nombre del producto
     * @param criterioOrdenamiento
     *            Criterio de ordenamiento pos: Los resultados de la consulta
     *            quedan guardados en la clase Cabandes
     */
    public void consultarExistenciasDeLosItemsConNombre(String nombre, Criterio criterioOrdenamiento)
    {
	PreparedStatement prepStmt = null;
	try
	{
	    establecerConexion();
	    String sql = " SELECT I.ID, P.NOMBRE AS NOMBRE, T.NOMBRE AS TIPO, PR.NUMERO_UNIDADES, I.FECHA_INGRESO, I.ID_BODEGA, I.ID_LOCAL, P.DIAS_VENCIMIENTO "
		    + " FROM ITEMS I INNER JOIN PRODUCTOS P ON I.ID_PRODUCTO = P.ID "
		    + " LEFT OUTER JOIN TIPOS T ON T.ID = P.ID_TIPO "
		    + " LEFT OUTER JOIN PRESENTACIONES PR ON I.ID = PR.ID_ITEM "
		    + " LEFT OUTER JOIN BODEGAS B ON I.ID_BODEGA = B.ID "
		    + " LEFT OUTER JOIN LOCALES L ON I.ID_LOCAL = L.ID "
		    + " WHERE  P.NOMBRE = '"
		    + nombre
		    + "' "
		    + " ORDER BY "
		    + criterioOrdenamiento.darAtributoBD();

	    prepStmt = conexion.prepareStatement(sql);
	    ResultSet rs = prepStmt.executeQuery();

	    ArrayList<IItem> items = new ArrayList<IItem>();

	    while (rs.next())
	    {
		String id = String.valueOf(rs.getInt("ID"));
		String nombreDB = rs.getString("NOMBRE");
		String tipo = rs.getString("TIPO");
		int unidades = rs.getInt("NUMERO_UNIDADES");
		String ingresoString = rs.getString("FECHA_INGRESO");
		// Date ingreso = new
		// SimpleDateFormat("dd/MM/yyyy").parse(ingresoString);
		String diasVencim = rs.getString("DIAS_VENCIMIENTO");
		Integer idBodega = rs.getInt("ID_BODEGA");
		Integer idLocal = rs.getInt("ID_LOCAL");

		String localBodega = idBodega != null ? "Bodega" : "Local";

		// FALTA SUMAR LAS FECHAS

		ItemVO item = new ItemVO();
		item.ID = id;
		item.nombre = nombreDB;
		item.tipo = tipo;
		item.unidades = unidades;
		item.localBodega = localBodega;

		items.add(item);
	    }
	    Cabandes.getInstance().itemsUltimaConsulta = items;
	}
	catch (Exception e)
	{
	    e.printStackTrace();
	}
	finally
	{
	    if (prepStmt != null)
		try
		{
		    prepStmt.close();
		}
		catch (SQLException exception)
		{

		}
	    try
	    {
		closeConnection(conexion);
	    }
	    catch (Exception e)
	    {
		e.printStackTrace();
	    }
	}
    }

    public void consultarExistenciasDeLosItemsConTipo(String tipo, Criterio criterioOrdenamiento)
    {
	PreparedStatement prepStmt = null;
	try
	{
	    establecerConexion();
	    String sql = " SELECT PR.PESO, I.ID, P.NOMBRE AS NOMBRE, T.NOMBRE AS TIPO, PR.NUMERO_UNIDADES, I.FECHA_INGRESO, I.ID_BODEGA, I.ID_LOCAL, P.DIAS_VENCIMIENTO "
		    + " FROM ITEMS I INNER JOIN PRODUCTOS P ON I.ID_PRODUCTO = P.ID "
		    + " LEFT OUTER JOIN TIPOS T ON T.ID = P.ID_TIPO "
		    + " LEFT OUTER JOIN PRESENTACIONES PR ON I.ID = PR.ID_ITEM "
		    + " LEFT OUTER JOIN BODEGAS B ON I.ID_BODEGA = B.ID "
		    + " LEFT OUTER JOIN LOCALES L ON I.ID_LOCAL = L.ID "
		    + " WHERE  T.NOMBRE = '"
		    + tipo
		    + "' "
		    + " ORDER BY "
		    + criterioOrdenamiento.darAtributoBD();

	    prepStmt = conexion.prepareStatement(sql);
	    ResultSet rs = prepStmt.executeQuery();

	    ArrayList<IItem> items = new ArrayList<IItem>();

	    while (rs.next())
	    {
		String id = String.valueOf(rs.getInt("ID"));
		String nombreDB = rs.getString("NOMBRE");
		String tipoDB = rs.getString("TIPO");
		int unidades = rs.getInt("NUMERO_UNIDADES");
		String ingresoString = rs.getString("FECHA_INGRESO");
		// Date ingreso = new
		// SimpleDateFormat("dd/MM/yyyy").parse(ingresoString);
		String diasVencim = rs.getString("DIAS_VENCIMIENTO");
		Integer idBodega = rs.getInt("ID_BODEGA");
		Integer idLocal = rs.getInt("ID_LOCAL");
		Integer pesoPesentacion = rs.getInt("PESO");

		String localBodega = idBodega != null ? "Bodega" : "Local";

		// FALTA SUMAR LAS FECHAS

		ItemVO item = new ItemVO();
		item.ID = id;
		item.nombre = nombreDB;
		item.tipo = tipoDB;
		item.unidades = unidades;
		item.idBodega = idBodega;
		item.idLocal = idBodega;
		item.pesoPresentacion = pesoPesentacion;
		item.localBodega = localBodega;

		items.add(item);
	    }
	    Cabandes.getInstance().itemsUltimaConsulta = items;

	}
	catch (Exception e)
	{
	    e.printStackTrace();
	}
	finally
	{
	    if (prepStmt != null)
		try
		{
		    prepStmt.close();
		}
		catch (SQLException exception)
		{

		}
	    try
	    {
		closeConnection(conexion);
	    }
	    catch (Exception e)
	    {
		e.printStackTrace();
	    }
	}
    }

    public void consultarExistenciasDeLosItemsConFechaDeExpiracion(String fecha, Criterio criterioOrdenamiento)
    {
	PreparedStatement prepStmt = null;

	try
	{
	    establecerConexion();
	    String sql = " SELECT I.ID, P.NOMBRE AS NOMBRE, T.NOMBRE AS TIPO, PR.NUMERO_UNIDADES, I.FECHA_INGRESO, I.ID_BODEGA, I.ID_LOCAL, P.DIAS_VENCIMIENTO "
		    + " FROM ITEMS I INNER JOIN PRODUCTOS P ON I.ID_PRODUCTO = P.ID "
		    + " LEFT OUTER JOIN TIPOS T ON T.ID = P.ID_TIPO "
		    + " LEFT OUTER JOIN PRESENTACIONES PR ON I.ID = PR.ID_ITEM "
		    + " LEFT OUTER JOIN BODEGAS B ON I.ID_BODEGA = B.ID "
		    + " LEFT OUTER JOIN LOCALES L ON I.ID_LOCAL = L.ID "
		    + " WHERE  P.NOMBRE = '"
		    + fecha
		    + "' "
		    + " ORDER BY "
		    + criterioOrdenamiento.darAtributoBD();

	    prepStmt = conexion.prepareStatement(sql);
	    ResultSet rs = prepStmt.executeQuery();

	    ArrayList<IItem> items = new ArrayList<IItem>();

	    while (rs.next())
	    {
		String id = String.valueOf(rs.getInt("ID"));
		String nombreDB = rs.getString("NOMBRE");
		String tipo = rs.getString("TIPO");
		int unidades = rs.getInt("NUMERO_UNIDADES");
		String ingresoString = rs.getString("FECHA_INGRESO");
		// Date ingreso = new
		// SimpleDateFormat("dd/MM/yyyy").parse(ingresoString);
		String diasVencim = rs.getString("DIAS_VENCIMIENTO");
		Integer idBodega = rs.getInt("ID_BODEGA");
		Integer idLocal = rs.getInt("ID_LOCAL");

		String localBodega = idBodega != null ? "Bodega" : "Local";

		// FALTA SUMAR LAS FECHAS

		ItemVO item = new ItemVO();
		item.ID = id;
		item.nombre = nombreDB;
		item.tipo = tipo;
		item.unidades = unidades;
		item.localBodega = localBodega;

		items.add(item);
	    }
	    Cabandes.getInstance().itemsUltimaConsulta = items;

	}
	catch (Exception e)
	{
	    e.printStackTrace();
	}
	finally
	{
	    if (prepStmt != null)
		try
		{
		    prepStmt.close();
		}
		catch (SQLException exception)
		{

		}
	    try
	    {
		closeConnection(conexion);
	    }
	    catch (Exception e)
	    {
		e.printStackTrace();
	    }
	}

    }

    public void consultarExistenciasDeLosItemsConIdentificadorDeBodega(String idBodega, Criterio criterioOrdenamiento)
    {
	int x = Integer.parseInt(idBodega);
	PreparedStatement prepStmt = null;
	try
	{
	    establecerConexion();
	    String sql = " SELECT I.ID, P.NOMBRE AS NOMBRE, T.NOMBRE AS TIPO, PR.NUMERO_UNIDADES, I.FECHA_INGRESO, I.ID_BODEGA, I.ID_LOCAL, P.DIAS_VENCIMIENTO "
		    + " FROM ITEMS I INNER JOIN PRODUCTOS P ON I.ID_PRODUCTO = P.ID "
		    + " LEFT OUTER JOIN TIPOS T ON T.ID = P.ID_TIPO "
		    + " LEFT OUTER JOIN PRESENTACIONES PR ON I.ID = PR.ID_ITEM "
		    + " LEFT OUTER JOIN BODEGAS B ON I.ID_BODEGA = B.ID "
		    + " LEFT OUTER JOIN LOCALES L ON I.ID_LOCAL = L.ID "
		    + " WHERE B.ID = '"
		    + idBodega
		    + "' "
		    + " ORDER BY "
		    + criterioOrdenamiento.darAtributoBD();

	    prepStmt = conexion.prepareStatement(sql);
	    ResultSet rs = prepStmt.executeQuery();

	    ArrayList<IItem> items = new ArrayList<IItem>();

	    while (rs.next())
	    {
		String id = String.valueOf(rs.getInt("ID"));
		String nombreDB = rs.getString("NOMBRE");
		String tipo = rs.getString("TIPO");
		int unidades = rs.getInt("NUMERO_UNIDADES");
		String ingresoString = rs.getString("FECHA_INGRESO");
		// Date ingreso = new
		// SimpleDateFormat("dd/MM/yyyy").parse(ingresoString);
		String diasVencim = rs.getString("DIAS_VENCIMIENTO");
		Integer idBodegaDB = rs.getInt("ID_BODEGA");
		Integer idLocal = rs.getInt("ID_LOCAL");

		String localBodega = idBodegaDB != null ? "Bodega" : "Local";

		// FALTA SUMAR LAS FECHAS

		ItemVO item = new ItemVO();
		item.ID = id;
		item.nombre = nombreDB;
		item.tipo = tipo;
		item.unidades = unidades;
		item.localBodega = localBodega;

		items.add(item);
	    }
	    Cabandes.getInstance().itemsUltimaConsulta = items;

	}
	catch (Exception e)
	{
	    e.printStackTrace();
	}
	finally
	{
	    if (prepStmt != null)
		try
		{
		    prepStmt.close();
		}
		catch (SQLException exception)
		{

		}
	    try
	    {
		closeConnection(conexion);
	    }
	    catch (Exception e)
	    {
		e.printStackTrace();
	    }
	}

    }

    public void consultarExistenciasDeLosItemsConIdentificadorDeLocal(String idLocal, Criterio criterioOrdenamiento)
    {
	PreparedStatement prepStmt = null;
	try
	{
	    establecerConexion();
	    String sql = " SELECT I.ID, P.NOMBRE AS NOMBRE, T.NOMBRE AS TIPO, PR.NUMERO_UNIDADES, I.FECHA_INGRESO, I.ID_BODEGA, I.ID_LOCAL, P.DIAS_VENCIMIENTO "
		    + " FROM ITEMS I INNER JOIN PRODUCTOS P ON I.ID_PRODUCTO = P.ID "
		    + " LEFT OUTER JOIN TIPOS T ON T.ID = P.ID_TIPO "
		    + " LEFT OUTER JOIN PRESENTACIONES PR ON I.ID = PR.ID_ITEM "
		    + " LEFT OUTER JOIN BODEGAS B ON I.ID_BODEGA = B.ID "
		    + " LEFT OUTER JOIN LOCALES L ON I.ID_LOCAL = L.ID "
		    + " WHERE  L.ID = '"
		    + idLocal
		    + "' "
		    + " ORDER BY "
		    + criterioOrdenamiento.darAtributoBD();

	    prepStmt = conexion.prepareStatement(sql);
	    ResultSet rs = prepStmt.executeQuery();

	    ArrayList<IItem> items = new ArrayList<IItem>();

	    while (rs.next())
	    {
		String id = String.valueOf(rs.getInt("ID"));
		String nombreDB = rs.getString("NOMBRE");
		String tipo = rs.getString("TIPO");
		int unidades = rs.getInt("NUMERO_UNIDADES");
		String ingresoString = rs.getString("FECHA_INGRESO");
		// Date ingreso = new
		// SimpleDateFormat("dd/MM/yyyy").parse(ingresoString);
		String diasVencim = rs.getString("DIAS_VENCIMIENTO");
		Integer idBodega = rs.getInt("ID_BODEGA");
		Integer idLocalDB = rs.getInt("ID_LOCAL");

		String localBodega = idBodega != null ? "Bodega" : "Local";

		// FALTA SUMAR LAS FECHAS

		ItemVO item = new ItemVO();
		item.ID = id;
		item.nombre = nombreDB;
		item.tipo = tipo;
		item.unidades = unidades;
		item.localBodega = localBodega;

		items.add(item);
	    }
	    Cabandes.getInstance().itemsUltimaConsulta = items;

	}
	catch (Exception e)
	{
	    e.printStackTrace();
	}
	finally
	{
	    if (prepStmt != null)
		try
		{
		    prepStmt.close();
		}
		catch (SQLException exception)
		{

		}
	    try
	    {
		closeConnection(conexion);
	    }
	    catch (Exception e)
	    {
		e.printStackTrace();
	    }
	}

    }

    public void consultarExistenciasDeLosItemsConPresentacion(String peso, Criterio criterioOrdenamiento)
    {
	PreparedStatement prepStmt = null;
	try
	{
	    establecerConexion();
	    String sql = " SELECT I.ID, P.NOMBRE AS NOMBRE, T.NOMBRE AS TIPO, PR.NUMERO_UNIDADES, I.FECHA_INGRESO, I.ID_BODEGA, I.ID_LOCAL, P.DIAS_VENCIMIENTO "
		    + " FROM ITEMS I INNER JOIN PRODUCTOS P ON I.ID_PRODUCTO = P.ID "
		    + " LEFT OUTER JOIN TIPOS T ON T.ID = P.ID_TIPO "
		    + " LEFT OUTER JOIN PRESENTACIONES PR ON I.ID = PR.ID_ITEM "
		    + " LEFT OUTER JOIN BODEGAS B ON I.ID_BODEGA = B.ID "
		    + " LEFT OUTER JOIN LOCALES L ON I.ID_LOCAL = L.ID "
		    + " WHERE  PR.PESO = '"
		    + peso
		    + "' "
		    + " ORDER BY "
		    + criterioOrdenamiento.darAtributoBD();

	    prepStmt = conexion.prepareStatement(sql);
	    ResultSet rs = prepStmt.executeQuery();

	    ArrayList<IItem> items = new ArrayList<IItem>();

	    while (rs.next())
	    {
		String id = String.valueOf(rs.getInt("ID"));
		String nombreDB = rs.getString("NOMBRE");
		String tipo = rs.getString("TIPO");
		int unidades = rs.getInt("NUMERO_UNIDADES");
		String ingresoString = rs.getString("FECHA_INGRESO");
		// Date ingreso = new
		// SimpleDateFormat("dd/MM/yyyy").parse(ingresoString);
		String diasVencim = rs.getString("DIAS_VENCIMIENTO");
		Integer idBodega = rs.getInt("ID_BODEGA");
		Integer idLocal = rs.getInt("ID_LOCAL");

		String localBodega = idBodega != null ? "Bodega" : "Local";

		// FALTA SUMAR LAS FECHAS

		ItemVO item = new ItemVO();
		item.ID = id;
		item.nombre = nombreDB;
		item.tipo = tipo;
		item.unidades = unidades;
		item.localBodega = localBodega;

		items.add(item);
	    }
	    Cabandes.getInstance().itemsUltimaConsulta = items;

	}
	catch (Exception e)
	{
	    e.printStackTrace();
	}
	finally
	{
	    if (prepStmt != null)
		try
		{
		    prepStmt.close();
		}
		catch (SQLException exception)
		{

		}
	    try
	    {
		closeConnection(conexion);
	    }
	    catch (Exception e)
	    {
		e.printStackTrace();
	    }
	}

    }

    public void consultarPedidosSatisfechosDelUsuario(String correoUsuario)
    {
	PreparedStatement prepStmt = null;
	try
	{
	    establecerConexion();
	    String sql = "SELECT PED.NUMERO_PEDIDO, PED.FECHA_REALIZACION, P.NOMBRE, PR.COSTO, PED.FECHA_ESPERADA, PED.ENTREGADO "
		    + "FROM USUARIOS U INNER JOIN PEDIDOS_ADMIN_LOC PED ON U.ID = PED.ID_ADMIN_LOCAL "
		    + "INNER JOIN PEDIDOS_ADMIN_LOC_ITEMS PEDIT ON PED.NUMERO_PEDIDO = PEDIT.NUMERO_PEDIDO "
		    + "INNER JOIN ITEMS I ON PEDIT.ID_ITEM = I.ID " + "INNER JOIN PRODUCTOS P ON I.ID_PRODUCTO = P.ID  "
		    + "INNER JOIN PRESENTACIONES PR ON I.ID = PR.ID_ITEM " + "WHERE U.CORREO ='" + correoUsuario
		    + "' AND PED.ENTREGADO = 'Y' ";

	    prepStmt = conexion.prepareStatement(sql);
	    ResultSet rs = prepStmt.executeQuery();

	    ArrayList<IPedidoAdministradorLocal> pedidos = new ArrayList<IPedidoAdministradorLocal>();

	    while (rs.next())
	    {
		String numPedido = String.valueOf(rs.getInt("NUMERO_PEDIDO"));
		Date fecha = rs.getDate("FECHA_REALIZACION");
		Date fechaEsperada = rs.getDate("FECHA_ESPERADA");
		String nombreProducto = rs.getString("NOMBRE");
		Double costoTotal = rs.getDouble("COSTO");
		String satisfechoString = rs.getString("ENTREGADO");
		boolean satisfecho = satisfechoString.equals("Y");

		PedidoVO pedidoVO = new PedidoVO();
		pedidoVO.numeroPedido = numPedido;
		pedidoVO.fechaRealizacion = fecha;
		pedidoVO.fechaEsperada = fechaEsperada;
		pedidoVO.nombreProducto = nombreProducto;
		pedidoVO.costoTotal = costoTotal;
		pedidoVO.satisfecho = satisfecho;

		pedidos.add(pedidoVO);

	    }
	    Cabandes.getInstance().pedidosUltimaConsulta = pedidos;

	}
	catch (Exception e)
	{
	    e.printStackTrace();
	}
	finally
	{
	    if (prepStmt != null)
		try
		{
		    prepStmt.close();
		}
		catch (SQLException exception)
		{

		}
	    try
	    {
		closeConnection(conexion);
	    }
	    catch (Exception e)
	    {
		e.printStackTrace();
	    }
	}
    }

    public void consultarPedidosPendientesDelUsuario(String correoUsuario)
    {
	PreparedStatement prepStmt = null;
	try
	{
	    establecerConexion();
	    String sql = "SELECT PED.NUMERO_PEDIDO, PED.FECHA_REALIZACION, P.NOMBRE, PR.COSTO, PED.FECHA_ESPERADA, PED.ENTREGADO "
		    + "FROM USUARIOS U INNER JOIN PEDIDOS_ADMIN_LOC PED ON U.ID = PED.ID_ADMIN_LOCAL "
		    + "INNER JOIN PEDIDOS_ADMIN_LOC_ITEMS PEDIT ON PED.NUMERO_PEDIDO = PEDIT.NUMERO_PEDIDO "
		    + "INNER JOIN ITEMS I ON PEDIT.ID_ITEM = I.ID " + "INNER JOIN PRODUCTOS P ON I.ID_PRODUCTO = P.ID "
		    + "INNER JOIN PRESENTACIONES PR ON I.ID = PR.ID_ITEM " + "WHERE U.CORREO ='" + correoUsuario
		    + "' AND PED.ENTREGADO = 'N' ";

	    prepStmt = conexion.prepareStatement(sql);
	    ResultSet rs = prepStmt.executeQuery();

	    ArrayList<IPedidoAdministradorLocal> pedidos = new ArrayList<IPedidoAdministradorLocal>();

	    while (rs.next())
	    {
		String numPedido = String.valueOf(rs.getInt("NUMERO_PEDIDO"));
		Date fecha = rs.getDate("FECHA_REALIZACION");
		Date fechaEsperada = rs.getDate("FECHA_ESPERADA");
		String nombreProducto = rs.getString("NOMBRE");
		Double costoTotal = rs.getDouble("COSTO");
		String satisfechoString = rs.getString("ENTREGADO");
		boolean satisfecho = satisfechoString.equals("Y");

		PedidoVO pedidoVO = new PedidoVO();
		pedidoVO.numeroPedido = numPedido;
		pedidoVO.fechaRealizacion = fecha;
		pedidoVO.fechaEsperada = fechaEsperada;
		pedidoVO.nombreProducto = nombreProducto;
		pedidoVO.costoTotal = costoTotal;
		pedidoVO.satisfecho = satisfecho;

		pedidos.add(pedidoVO);
	    }
	    Cabandes.getInstance().pedidosUltimaConsulta = pedidos;

	}
	catch (Exception e)
	{
	    e.printStackTrace();
	}
	finally
	{
	    if (prepStmt != null)
		try
		{
		    prepStmt.close();
		}
		catch (SQLException exception)
		{

		}
	    try
	    {
		closeConnection(conexion);
	    }
	    catch (Exception e)
	    {
		e.printStackTrace();
	    }
	}
    }

    public void consultarPedidosDelUsuarioConCosto(String correoUsuario, String costo)
    {
	// Hay un error porque en la base de datos no se puede obtener el costo
	// de la presentación
	// No hay manera de llegar a él ya que en la tabla
	// pedidos_admin_loc_items no se guarda info de presentaciones
	// y id_item se puede replicar

    }

    public void consultarPedidosDelUsuarioRealizadosEntre(String correoUsuario, String fechaInicial, String fechaFinal)
    {
	PreparedStatement prepStmt = null;
	try
	{
	    establecerConexion();
	    String sql = "SELECT PED.NUMERO_PEDIDO, PED.FECHA_REALIZACION, P.NOMBRE, PR.COSTO, PED.FECHA_ESPERADA, PED.ENTREGADO "
		    + "FROM USUARIOS U INNER JOIN PEDIDOS_ADMIN_LOC PED ON U.ID = PED.ID_ADMIN_LOCAL "
		    + "INNER JOIN PEDIDOS_ADMIN_LOC_ITEMS PEDIT ON PED.NUMERO_PEDIDO = PEDIT.NUMERO_PEDIDO "
		    + "INNER JOIN ITEMS I ON PEDIT.ID_ITEM = I.ID " + "INNER JOIN PRODUCTOS P ON I.ID_PRODUCTO = P.ID "
		    + "INNER JOIN PRESENTACIONES PR ON I.ID = PR.ID_ITEM " + "WHERE U.CORREO ='" + correoUsuario
		    + "' AND PED.FECHA_REALIZACION BETWEEN (" + fechaInicial + "," + fechaFinal + ") ";

	    prepStmt = conexion.prepareStatement(sql);
	    ResultSet rs = prepStmt.executeQuery();

	    ArrayList<IPedidoAdministradorLocal> pedidos = new ArrayList<IPedidoAdministradorLocal>();

	    while (rs.next())
	    {
		String numPedido = String.valueOf(rs.getInt("NUMERO_PEDIDO"));
		Date fecha = rs.getDate("FECHA_REALIZACION");
		Date fechaEsperada = rs.getDate("FECHA_ESPERADA");
		String nombreProducto = rs.getString("NOMBRE");
		Double costoTotal = rs.getDouble("COSTO");
		String satisfechoString = rs.getString("ENTREGADO");
		boolean satisfecho = satisfechoString.equals("Y");

		PedidoVO pedidoVO = new PedidoVO();
		pedidoVO.numeroPedido = numPedido;
		pedidoVO.fechaRealizacion = fecha;
		pedidoVO.fechaEsperada = fechaEsperada;
		pedidoVO.nombreProducto = nombreProducto;
		pedidoVO.costoTotal = costoTotal;
		pedidoVO.satisfecho = satisfecho;

		pedidos.add(pedidoVO);

	    }
	    Cabandes.getInstance().pedidosUltimaConsulta = pedidos;

	}
	catch (Exception e)
	{
	    e.printStackTrace();
	}
	finally
	{
	    if (prepStmt != null)
		try
		{
		    prepStmt.close();
		}
		catch (SQLException exception)
		{

		}
	    try
	    {
		closeConnection(conexion);
	    }
	    catch (Exception e)
	    {
		e.printStackTrace();
	    }
	}
    }

    public void consultarPedidosDelUsuarioConNombreDeProducto(String correoUsuario, String nombreProducto)
    {
	PreparedStatement prepStmt = null;
	try
	{
	    establecerConexion();
	    String sql = "SELECT PED.NUMERO_PEDIDO, PED.FECHA_REALIZACION, P.NOMBRE, PR.COSTO, PED.FECHA_ESPERADA, PED.ENTREGADO "
		    + "FROM USUARIOS U INNER JOIN PEDIDOS_ADMIN_LOC PED ON U.ID = PED.ID_ADMIN_LOCAL "
		    + "INNER JOIN PEDIDOS_ADMIN_LOC_ITEMS PEDIT ON PED.NUMERO_PEDIDO = PEDIT.NUMERO_PEDIDO "
		    + "INNER JOIN ITEMS I ON PEDIT.ID_ITEM = I.ID " + "INNER JOIN PRODUCTOS P ON I.ID_PRODUCTO = P.ID "
		    + "INNER JOIN PRESENTACIONES PR ON I.ID = PR.ID_ITEM " + "WHERE U.CORREO ='" + correoUsuario + "' AND P.NOMBRE ='"
		    + nombreProducto + "'";

	    prepStmt = conexion.prepareStatement(sql);
	    ResultSet rs = prepStmt.executeQuery();

	    ArrayList<IPedidoAdministradorLocal> pedidos = new ArrayList<IPedidoAdministradorLocal>();

	    while (rs.next())
	    {
		String numPedido = String.valueOf(rs.getInt("NUMERO_PEDIDO"));
		Date fecha = rs.getDate("FECHA_REALIZACION");
		Date fechaEsperada = rs.getDate("FECHA_ESPERADA");
		String nombreProductoDB = rs.getString("NOMBRE");
		Double costoTotal = rs.getDouble("COSTO");
		String satisfechoString = rs.getString("ENTREGADO");
		boolean satisfecho = satisfechoString.equals("Y");

		PedidoVO pedidoVO = new PedidoVO();
		pedidoVO.numeroPedido = numPedido;
		pedidoVO.fechaRealizacion = fecha;
		pedidoVO.fechaEsperada = fechaEsperada;
		pedidoVO.nombreProducto = nombreProductoDB;
		pedidoVO.costoTotal = costoTotal;
		pedidoVO.satisfecho = satisfecho;

		pedidos.add(pedidoVO);

	    }
	    Cabandes.getInstance().pedidosUltimaConsulta = pedidos;

	}
	catch (Exception e)
	{
	    e.printStackTrace();
	}
	finally
	{
	    if (prepStmt != null)
		try
		{
		    prepStmt.close();
		}
		catch (SQLException exception)
		{

		}
	    try
	    {
		closeConnection(conexion);
	    }
	    catch (Exception e)
	    {
		e.printStackTrace();
	    }
	}
    }

    public void consultarPedidosDelUsuarioConPresentacion(String correoUsuario, String pesoPresentacion)
    {
	PreparedStatement prepStmt = null;
	try
	{
	    establecerConexion();
	    String sql = "SELECT PED.NUMERO_PEDIDO, PED.FECHA_REALIZACION, P.NOMBRE, PR.COSTO, PED.FECHA_ESPERADA, PED.ENTREGADO "
		    + "FROM USUARIOS U INNER JOIN PEDIDOS_ADMIN_LOC PED ON U.ID = PED.ID_ADMIN_LOCAL "
		    + "INNER JOIN PEDIDOS_ADMIN_LOC_ITEMS PEDIT ON PED.NUMERO_PEDIDO = PEDIT.NUMERO_PEDIDO "
		    + "INNER JOIN ITEMS I ON PEDIT.ID_ITEM = I.ID " + "INNER JOIN PRODUCTOS P ON I.ID_PRODUCTO = P.ID "
		    + "INNER JOIN PRESENTACIONES PR ON I.ID = PR.ID_ITEM " + "WHERE U.CORREO ='" + correoUsuario + "' AND PR.PESO ='"
		    + pesoPresentacion + "'";

	    prepStmt = conexion.prepareStatement(sql);
	    ResultSet rs = prepStmt.executeQuery();

	    ArrayList<IPedidoAdministradorLocal> pedidos = new ArrayList<IPedidoAdministradorLocal>();

	    while (rs.next())
	    {
		String numPedido = String.valueOf(rs.getInt("NUMERO_PEDIDO"));
		Date fecha = rs.getDate("FECHA_REALIZACION");
		Date fechaEsperada = rs.getDate("FECHA_ESPERADA");
		String nombreProducto = rs.getString("NOMBRE");
		Double costoTotal = rs.getDouble("COSTO");
		String satisfechoString = rs.getString("ENTREGADO");
		boolean satisfecho = satisfechoString.equals("Y");

		PedidoVO pedidoVO = new PedidoVO();
		pedidoVO.numeroPedido = numPedido;
		pedidoVO.fechaRealizacion = fecha;
		pedidoVO.fechaEsperada = fechaEsperada;
		pedidoVO.nombreProducto = nombreProducto;
		pedidoVO.costoTotal = costoTotal;
		pedidoVO.satisfecho = satisfecho;

		pedidos.add(pedidoVO);

	    }
	    Cabandes.getInstance().pedidosUltimaConsulta = pedidos;

	}
	catch (Exception e)
	{
	    e.printStackTrace();
	}
	finally
	{
	    if (prepStmt != null)
		try
		{
		    prepStmt.close();
		}
		catch (SQLException exception)
		{

		}
	    try
	    {
		closeConnection(conexion);
	    }
	    catch (Exception e)
	    {
		e.printStackTrace();
	    }
	}
    }
    
    public void consultarHistorialMovimientosDeProducto(String fechaInicio, String fechaFin, String nombreProducto, boolean noSonDeProducto)
    {
	PreparedStatement prepStmt = null;
	try
	{
	    establecerConexion();
	    String sql = "SELECT H.ID, P.NOMBRE AS PRODUCTO, H.FECHA, H.ID_BODEGA_ORIGEN, H.ID_BODEGA_DESTINO, H.ID_LOCAL_ORIGEN, H.ID_LOCAL_DESTINO, H.CANTIDAD_TOTAL_PRODUCTO"
                       +" FROM HISTORIAL_MOVIMIENTOS H INNER JOIN PRODUCTOS P ON H.ID_PRODUCTO = P.ID"
                       +" WHERE "+ (noSonDeProducto? "NOT":"") +" P.NOMBRE = '"+nombreProducto+"' AND H.FECHA BETWEEN '"+fechaInicio+"' AND '"+fechaFin+"'";

	    prepStmt = conexion.prepareStatement(sql);

	    long startTime = System.currentTimeMillis();
    	    
	    ResultSet rs = prepStmt.executeQuery();
	    
	    long elapsedTime = System.currentTimeMillis() - startTime;
	    System.out.println("Historial "+(noSonDeProducto? "negado" : "") +" (peor caso): "+ elapsedTime);

	    ArrayList<MovimientoVO> movimientos = new ArrayList<MovimientoVO>();

	    while (rs.next())
	    {
		MovimientoVO movimientoVO = new MovimientoVO();

		movimientoVO.ID = rs.getString("ID");
		movimientoVO.nombreProducto = rs.getString("PRODUCTO");
		movimientoVO.fecha = rs.getDate("FECHA");
		movimientoVO.bodegaOrigen = rs.getString("ID_BODEGA_ORIGEN");
		movimientoVO.bodegaDestino = rs.getString("ID_BODEGA_DESTINO");
		movimientoVO.localOrigen = rs.getString("ID_LOCAL_ORIGEN");
		movimientoVO.localDestino = rs.getString("ID_LOCAL_DESTINO");
		movimientoVO.cantidad = rs.getString("CANTIDAD_TOTAL_PRODUCTO");

		movimientos.add(movimientoVO);

	    }
	    

	    Cabandes.getInstance().setListenerRespuestas(this);
	    Cabandes.getInstance().enviarSolicitud(Mensajes.CONSULTAR_MOVIMIENTOS+":"+(noSonDeProducto?"NO":"SI")+":PRODUCTO:"+nombreProducto);
	    
	    try
	    {
		wait(5000);
	    }
	    catch (InterruptedException e)
	    {
		e.printStackTrace();
	    }
	    
	    if (respuestaOtroComponente != null && respuestaOtroComponente.split(":")[0].equals(Mensajes.CONSULTAR_MOVIMIENTOS)) 
	    {
		String[] movimientosOtroComponente = respuestaOtroComponente.split(":");
		
		for (int i = 1; i < movimientosOtroComponente.length; i++)
		{
		    String movimiento = movimientosOtroComponente[i];
		    String[] componentes = movimiento.split("-");
		    
		    MovimientoVO movimientoVO = new MovimientoVO();
		    movimientoVO.nombreProducto = componentes[0];
		    movimientoVO.bodegaOrigen = componentes[1];
		    movimientoVO.bodegaDestino = componentes[2];
		    movimientoVO.localOrigen = componentes[3];
		    movimientoVO.localDestino = componentes[4];

		    movimientos.add(movimientoVO);
		}
	    }	
 
	    Cabandes.getInstance().movimientosUltimaConsulta = movimientos;
	    respuestaOtroComponente = null;

	}
	catch (Exception e)
	{
	    e.printStackTrace();
	}
	finally
	{
	    if (prepStmt != null)
		try
		{
		    prepStmt.close();
		}
		catch (SQLException exception)
		{

		}
	    try
	    {
		closeConnection(conexion);
	    }
	    catch (Exception e)
	    {
		e.printStackTrace();
	    }
	}
    }
    
    public void consultarHistorialMovimientosConBodegaOrigen(String fechaInicio, String fechaFin, String bodega, boolean noConBodegaOrigen)
    {
	PreparedStatement prepStmt = null;
	try
	{
	    establecerConexion();
	    String sql = "SELECT H.ID, P.NOMBRE AS PRODUCTO, H.FECHA, H.ID_BODEGA_ORIGEN, H.ID_BODEGA_DESTINO, H.ID_LOCAL_ORIGEN, H.ID_LOCAL_DESTINO, H.CANTIDAD_TOTAL_PRODUCTO"
                       +" FROM HISTORIAL_MOVIMIENTOS H INNER JOIN PRODUCTOS P ON H.ID_PRODUCTO = P.ID"
                       +" WHERE "+ (noConBodegaOrigen? "NOT":"") +" H.ID_BODEGA_ORIGEN = '"+bodega+"' AND H.FECHA BETWEEN '"+fechaInicio+"' AND '"+fechaFin+"'";

	    prepStmt = conexion.prepareStatement(sql);
	    ResultSet rs = prepStmt.executeQuery();

	    ArrayList<MovimientoVO> movimientos = new ArrayList<MovimientoVO>();

	    while (rs.next())
	    {
		MovimientoVO movimientoVO = new MovimientoVO();

		movimientoVO.ID = rs.getString("ID");
		movimientoVO.nombreProducto = rs.getString("PRODUCTO");
		movimientoVO.fecha = rs.getDate("FECHA");
		movimientoVO.bodegaOrigen = rs.getString("ID_BODEGA_ORIGEN");
		movimientoVO.bodegaDestino = rs.getString("ID_BODEGA_DESTINO");
		movimientoVO.localOrigen = rs.getString("ID_LOCAL_ORIGEN");
		movimientoVO.localDestino = rs.getString("ID_LOCAL_DESTINO");
		movimientoVO.cantidad = rs.getString("CANTIDAD_TOTAL_PRODUCTO");

		movimientos.add(movimientoVO);

	    }
	    
	    Cabandes.getInstance().setListenerRespuestas(this);
	    Cabandes.getInstance().enviarSolicitud(Mensajes.CONSULTAR_MOVIMIENTOS+":"+(noConBodegaOrigen?"NO":"SI")+":BODEGA_ORIGEN:"+bodega);
	    
	    try
	    {
		wait(5000);
	    }
	    catch (InterruptedException e)
	    {
		e.printStackTrace();
	    }
	    
	    if (respuestaOtroComponente != null && respuestaOtroComponente.split(":")[0].equals(Mensajes.CONSULTAR_MOVIMIENTOS)) 
	    {
		String[] movimientosOtroComponente = respuestaOtroComponente.split(":");
		
		for (int i = 1; i < movimientosOtroComponente.length; i++)
		{
		    String movimiento = movimientosOtroComponente[i];
		    String[] componentes = movimiento.split("-");
		    
		    MovimientoVO movimientoVO = new MovimientoVO();
		    movimientoVO.nombreProducto = componentes[0];
		    movimientoVO.bodegaOrigen = componentes[1];
		    movimientoVO.bodegaDestino = componentes[2];
		    movimientoVO.localOrigen = componentes[3];
		    movimientoVO.localDestino = componentes[4];

		    movimientos.add(movimientoVO);
		}
	    }	
 
	    Cabandes.getInstance().movimientosUltimaConsulta = movimientos;
	    respuestaOtroComponente = null;
	 
	}
	catch (Exception e)
	{
	    e.printStackTrace();
	}
	finally
	{
	    if (prepStmt != null)
		try
		{
		    prepStmt.close();
		}
		catch (SQLException exception)
		{

		}
	    try
	    {
		closeConnection(conexion);
	    }
	    catch (Exception e)
	    {
		e.printStackTrace();
	    }
	}
    }
    
    public void consultarHistorialMovimientosConBodegaDestino(String fechaInicio, String fechaFin, String bodega, boolean noConBodegaDestino)
    {
	PreparedStatement prepStmt = null;
	try
	{
	    establecerConexion();
	    String sql = "SELECT H.ID, P.NOMBRE AS PRODUCTO, H.FECHA, H.ID_BODEGA_ORIGEN, H.ID_BODEGA_DESTINO, H.ID_LOCAL_ORIGEN, H.ID_LOCAL_DESTINO, H.CANTIDAD_TOTAL_PRODUCTO"
                       +" FROM HISTORIAL_MOVIMIENTOS H INNER JOIN PRODUCTOS P ON H.ID_PRODUCTO = P.ID"
                       +" WHERE "+ (noConBodegaDestino? "NOT":"") +" H.ID_BODEGA_DESTINO = '"+bodega+"' AND H.FECHA BETWEEN '"+fechaInicio+"' AND '"+fechaFin+"'";

	    prepStmt = conexion.prepareStatement(sql);
	    System.out.println(sql);
	    	    
	    long startTime = System.currentTimeMillis();
	    	    
	    ResultSet rs = prepStmt.executeQuery();
	    
	    long elapsedTime = System.currentTimeMillis() - startTime;
	    System.out.println("Historial "+(noConBodegaDestino? "negado" : "") +" (peor caso): "+ elapsedTime);
	   	    
	    ArrayList<MovimientoVO> movimientos = new ArrayList<MovimientoVO>();
	    
	    while (rs.next())
	    {
	    	MovimientoVO movimientoVO = new MovimientoVO();

	    	movimientoVO.ID = rs.getString("ID");
	    	movimientoVO.nombreProducto = rs.getString("PRODUCTO");
	    	movimientoVO.fecha = rs.getDate("FECHA");
	    	movimientoVO.bodegaOrigen = rs.getString("ID_BODEGA_ORIGEN");
	    	movimientoVO.bodegaDestino = rs.getString("ID_BODEGA_DESTINO");
	    	movimientoVO.localOrigen = rs.getString("ID_LOCAL_ORIGEN");
	    	movimientoVO.localDestino = rs.getString("ID_LOCAL_DESTINO");
	    	movimientoVO.cantidad = rs.getString("CANTIDAD_TOTAL_PRODUCTO");

	    	movimientos.add(movimientoVO);

	    }
	    System.out.println(movimientos.size());
	    
	    Cabandes.getInstance().setListenerRespuestas(this);
	    Cabandes.getInstance().enviarSolicitud(Mensajes.CONSULTAR_MOVIMIENTOS+":"+(noConBodegaDestino?"NO":"SI")+":BODEGA_DESTINO:"+bodega);
	    
	    try
	    {
		wait(5000);
	    }
	    catch (InterruptedException e)
	    {
		e.printStackTrace();
	    }
	    
	    if (respuestaOtroComponente != null && respuestaOtroComponente.split(":")[0].equals(Mensajes.CONSULTAR_MOVIMIENTOS)) 
	    {
		String[] movimientosOtroComponente = respuestaOtroComponente.split(":");
		
		for (int i = 1; i < movimientosOtroComponente.length; i++)
		{
		    String movimiento = movimientosOtroComponente[i];
		    String[] componentes = movimiento.split("-");
		    
		    MovimientoVO movimientoVO = new MovimientoVO();
		    movimientoVO.nombreProducto = componentes[0];
		    movimientoVO.bodegaOrigen = componentes[1];
		    movimientoVO.bodegaDestino = componentes[2];
		    movimientoVO.localOrigen = componentes[3];
		    movimientoVO.localDestino = componentes[4];

		    movimientos.add(movimientoVO);
		}
	    }	
 
	    Cabandes.getInstance().movimientosUltimaConsulta = movimientos;
	    respuestaOtroComponente = null;
	}
	catch (Exception e)
	{
	    e.printStackTrace();
	}
	finally
	{
	    if (prepStmt != null)
		try
		{
		    prepStmt.close();
		}
		catch (SQLException exception)
		{

		}
	    try
	    {
		closeConnection(conexion);
	    }
	    catch (Exception e)
	    {
		e.printStackTrace();
	    }
	}
    }
    
    public void consultarHistorialMovimientosConLocalOrigen(String fechaInicio, String fechaFin, String local, boolean noConLocalOrigen)
    {
	PreparedStatement prepStmt = null;
	try
	{
	    establecerConexion();
	    String sql = "SELECT H.ID, P.NOMBRE AS PRODUCTO, H.FECHA, H.ID_BODEGA_ORIGEN, H.ID_BODEGA_DESTINO, H.ID_LOCAL_ORIGEN, H.ID_LOCAL_DESTINO, H.CANTIDAD_TOTAL_PRODUCTO"
                       +" FROM HISTORIAL_MOVIMIENTOS H INNER JOIN PRODUCTOS P ON H.ID_PRODUCTO = P.ID"
                       +" WHERE "+ (noConLocalOrigen? "NOT":"") +" H.ID_LOCAL_ORIGEN = '"+local+"' AND H.FECHA BETWEEN '"+fechaInicio+"' AND '"+fechaFin+"'";

	    prepStmt = conexion.prepareStatement(sql);
	    ResultSet rs = prepStmt.executeQuery();

	    ArrayList<MovimientoVO> movimientos = new ArrayList<MovimientoVO>();

	    while (rs.next())
	    {
		MovimientoVO movimientoVO = new MovimientoVO();

		movimientoVO.ID = rs.getString("ID");
		movimientoVO.nombreProducto = rs.getString("PRODUCTO");
		movimientoVO.fecha = rs.getDate("FECHA");
		movimientoVO.bodegaOrigen = rs.getString("ID_BODEGA_ORIGEN");
		movimientoVO.bodegaDestino = rs.getString("ID_BODEGA_DESTINO");
		movimientoVO.localOrigen = rs.getString("ID_LOCAL_ORIGEN");
		movimientoVO.localDestino = rs.getString("ID_LOCAL_DESTINO");
		movimientoVO.cantidad = rs.getString("CANTIDAD_TOTAL_PRODUCTO");

		movimientos.add(movimientoVO);

	    }
	    
	    Cabandes.getInstance().setListenerRespuestas(this);
	    Cabandes.getInstance().enviarSolicitud(Mensajes.CONSULTAR_MOVIMIENTOS+":"+(noConLocalOrigen?"NO":"SI")+":LOCAL_ORIGEN:"+local);
	    
	    try
	    {
		wait(5000);
	    }
	    catch (InterruptedException e)
	    {
		e.printStackTrace();
	    }
	    
	    if (respuestaOtroComponente != null && respuestaOtroComponente.split(":")[0].equals(Mensajes.CONSULTAR_MOVIMIENTOS)) 
	    {
		String[] movimientosOtroComponente = respuestaOtroComponente.split(":");
		
		for (int i = 1; i < movimientosOtroComponente.length; i++)
		{
		    String movimiento = movimientosOtroComponente[i];
		    String[] componentes = movimiento.split("-");
		    
		    MovimientoVO movimientoVO = new MovimientoVO();
		    movimientoVO.nombreProducto = componentes[0];
		    movimientoVO.bodegaOrigen = componentes[1];
		    movimientoVO.bodegaDestino = componentes[2];
		    movimientoVO.localOrigen = componentes[3];
		    movimientoVO.localDestino = componentes[4];

		    movimientos.add(movimientoVO);
		}
	    }	
 
	    Cabandes.getInstance().movimientosUltimaConsulta = movimientos;
	    respuestaOtroComponente = null;
	}
	catch (Exception e)
	{
	    e.printStackTrace();
	}
	finally
	{
	    if (prepStmt != null)
		try
		{
		    prepStmt.close();
		}
		catch (SQLException exception)
		{

		}
	    try
	    {
		closeConnection(conexion);
	    }
	    catch (Exception e)
	    {
		e.printStackTrace();
	    }
	}
    }
    
    public void consultarHistorialMovimientosConLocalDestino(String fechaInicio, String fechaFin, String local, boolean noConLocalDestino)
    {
	PreparedStatement prepStmt = null;
	try
	{
	    establecerConexion();
	    String sql = "SELECT H.ID, P.NOMBRE AS PRODUCTO, H.FECHA, H.ID_BODEGA_ORIGEN, H.ID_BODEGA_DESTINO, H.ID_LOCAL_ORIGEN, H.ID_LOCAL_DESTINO, H.CANTIDAD_TOTAL_PRODUCTO"
                       +" FROM HISTORIAL_MOVIMIENTOS H INNER JOIN PRODUCTOS P ON H.ID_PRODUCTO = P.ID"
                       +" WHERE "+ (noConLocalDestino? "NOT":"") +" H.ID_LOCAL_DESTINO = '"+local+"' AND H.FECHA BETWEEN '"+fechaInicio+"' AND '"+fechaFin+"'";

	    prepStmt = conexion.prepareStatement(sql);
	    ResultSet rs = prepStmt.executeQuery();

	    ArrayList<MovimientoVO> movimientos = new ArrayList<MovimientoVO>();

	    while (rs.next())
	    {
		MovimientoVO movimientoVO = new MovimientoVO();
		
		movimientoVO.ID = rs.getString("ID");
		movimientoVO.nombreProducto = rs.getString("PRODUCTO");
		movimientoVO.fecha = rs.getDate("FECHA");
		movimientoVO.bodegaOrigen = rs.getString("ID_BODEGA_ORIGEN");
		movimientoVO.bodegaDestino = rs.getString("ID_BODEGA_DESTINO");
		movimientoVO.localOrigen = rs.getString("ID_LOCAL_ORIGEN");
		movimientoVO.localDestino = rs.getString("ID_LOCAL_DESTINO");
		movimientoVO.cantidad = rs.getString("CANTIDAD_TOTAL_PRODUCTO");

		movimientos.add(movimientoVO);

	    }

	    Cabandes.getInstance().setListenerRespuestas(this);
	    Cabandes.getInstance().enviarSolicitud(Mensajes.CONSULTAR_MOVIMIENTOS+":"+(noConLocalDestino?"NO":"SI")+":LOCAL_DESTINO:"+local);
	    
	    try
	    {
		wait(5000);
	    }
	    catch (InterruptedException e)
	    {
		e.printStackTrace();
	    }
	    
	    if (respuestaOtroComponente != null && respuestaOtroComponente.split(":")[0].equals(Mensajes.CONSULTAR_MOVIMIENTOS)) 
	    {
		String[] movimientosOtroComponente = respuestaOtroComponente.split(":");
		
		for (int i = 1; i < movimientosOtroComponente.length; i++)
		{
		    String movimiento = movimientosOtroComponente[i];
		    String[] componentes = movimiento.split("-");
		    
		    MovimientoVO movimientoVO = new MovimientoVO();
		    movimientoVO.nombreProducto = componentes[0];
		    movimientoVO.bodegaOrigen = componentes[1];
		    movimientoVO.bodegaDestino = componentes[2];
		    movimientoVO.localOrigen = componentes[3];
		    movimientoVO.localDestino = componentes[4];

		    movimientos.add(movimientoVO);
		}
	    }	
 
	    Cabandes.getInstance().movimientosUltimaConsulta = movimientos;
	    respuestaOtroComponente = null;
	}
	catch (Exception e)
	{
	    e.printStackTrace();
	}
	finally
	{
	    if (prepStmt != null)
		try
		{
		    prepStmt.close();
		}
		catch (SQLException exception)
		{

		}
	    try
	    {
		closeConnection(conexion);
	    }
	    catch (Exception e)
	    {
		e.printStackTrace();
	    }
	}
    }
    
    public void consultarHistorialMovimientosConFecha(String fechaInicio, String fechaFin, String fecha, boolean noConFecha)
    {
	PreparedStatement prepStmt = null;
	try
	{
	    establecerConexion();
	    String sql = "SELECT H.ID, P.NOMBRE AS PRODUCTO, H.FECHA, H.ID_BODEGA_ORIGEN, H.ID_BODEGA_DESTINO, H.ID_LOCAL_ORIGEN, H.ID_LOCAL_DESTINO, H.CANTIDAD_TOTAL_PRODUCTO"
                       +" FROM HISTORIAL_MOVIMIENTOS H INNER JOIN PRODUCTOS P ON H.ID_PRODUCTO = P.ID"
                       +" WHERE "+ (noConFecha? "NOT":"") +" H.FECHA = '"+fecha+"' AND H.FECHA BETWEEN '"+fechaInicio+"' AND '"+fechaFin+"'";

	    prepStmt = conexion.prepareStatement(sql);
	    ResultSet rs = prepStmt.executeQuery();

	    ArrayList<MovimientoVO> movimientos = new ArrayList<MovimientoVO>();

	    while (rs.next())
	    {
		MovimientoVO movimientoVO = new MovimientoVO();
		
		movimientoVO.ID = rs.getString("ID");
		movimientoVO.nombreProducto = rs.getString("PRODUCTO");
		movimientoVO.fecha = rs.getDate("FECHA");
		movimientoVO.bodegaOrigen = rs.getString("ID_BODEGA_ORIGEN");
		movimientoVO.bodegaDestino = rs.getString("ID_BODEGA_DESTINO");
		movimientoVO.localOrigen = rs.getString("ID_LOCAL_ORIGEN");
		movimientoVO.localDestino = rs.getString("ID_LOCAL_DESTINO");
		movimientoVO.cantidad = rs.getString("CANTIDAD_TOTAL_PRODUCTO");

		movimientos.add(movimientoVO);

	    }
	    Cabandes.getInstance().movimientosUltimaConsulta = movimientos;

	}
	catch (Exception e)
	{
	    e.printStackTrace();
	}
	finally
	{
	    if (prepStmt != null)
		try
		{
		    prepStmt.close();
		}
		catch (SQLException exception)
		{

		}
	    try
	    {
		closeConnection(conexion);
	    }
	    catch (Exception e)
	    {
		e.printStackTrace();
	    }
	}
    }
    
    public void consultarHistorialMovimientosConCantidad(String fechaInicio, String fechaFin, String cantidad, boolean noConCantidad)
    {
	PreparedStatement prepStmt = null;
	try
	{
	    establecerConexion();
	    String sql = "SELECT H.ID, P.NOMBRE AS PRODUCTO, H.FECHA, H.ID_BODEGA_ORIGEN, H.ID_BODEGA_DESTINO, H.ID_LOCAL_ORIGEN, H.ID_LOCAL_DESTINO, H.CANTIDAD_TOTAL_PRODUCTO"
                       +" FROM HISTORIAL_MOVIMIENTOS H INNER JOIN PRODUCTOS P ON H.ID_PRODUCTO = P.ID"
                       +" WHERE "+ (noConCantidad? "NOT":"") +" H.CANTIDAD_TOTAL_PRODUCTO = '"+cantidad+"' AND H.FECHA BETWEEN '"+fechaInicio+"' AND '"+fechaFin+"'";

	    prepStmt = conexion.prepareStatement(sql);
	    ResultSet rs = prepStmt.executeQuery();

	    ArrayList<MovimientoVO> movimientos = new ArrayList<MovimientoVO>();

	    while (rs.next())
	    {
		MovimientoVO movimientoVO = new MovimientoVO();
		
		movimientoVO.ID = rs.getString("ID");
		movimientoVO.nombreProducto = rs.getString("PRODUCTO");
		movimientoVO.fecha = rs.getDate("FECHA");
		movimientoVO.bodegaOrigen = rs.getString("ID_BODEGA_ORIGEN");
		movimientoVO.bodegaDestino = rs.getString("ID_BODEGA_DESTINO");
		movimientoVO.localOrigen = rs.getString("ID_LOCAL_ORIGEN");
		movimientoVO.localDestino = rs.getString("ID_LOCAL_DESTINO");
		movimientoVO.cantidad = rs.getString("CANTIDAD_TOTAL_PRODUCTO");

		movimientos.add(movimientoVO);

	    }
	    Cabandes.getInstance().movimientosUltimaConsulta = movimientos;

	}
	catch (Exception e)
	{
	    e.printStackTrace();
	}
	finally
	{
	    if (prepStmt != null)
		try
		{
		    prepStmt.close();
		}
		catch (SQLException exception)
		{

		}
	    try
	    {
		closeConnection(conexion);
	    }
	    catch (Exception e)
	    {
		e.printStackTrace();
	    }
	}
    }

    public String cerrarBodega(String idBodega, boolean propia, boolean llamadoLocal)
    {
	ArrayList<PreparedStatement> prepStmts = new ArrayList<PreparedStatement>();
	
	UserTransaction transaction = null;
	try
	{
	    transaction = (UserTransaction) contexto.lookup("/UserTransaction");
	}
	catch (NamingException e2)
	{
	    e2.printStackTrace();
	}
	
	try
	{	    
	    transaction.begin();
	    
	    PreparedStatement prepStmt = null;
		
	    establecerConexion();
	    conexion.setAutoCommit(false);
	    conexion.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);

	    String sqlObtenerBodega = "SELECT * FROM BODEGAS B WHERE B.ID = '"+idBodega+"'";
	    prepStmt = conexion.prepareStatement(sqlObtenerBodega);
	    prepStmts.add(prepStmt);
	    ResultSet rs = prepStmt.executeQuery();
	    boolean existeBodega = false;
	    
	    while (rs.next())
	    {
		existeBodega = true;
	    }
	    
	    if (existeBodega)
	    {
		// ADMINISTRADORES CON PRODUCTOS DE PEDIDOS QUE PODRIAN SALIR LA BODEGA QUE SE VA A
		    // CERRAR
		    String sqlPedidosAdmin = "SELECT U.CORREO AS CORREO_ADMIN "
			    + " FROM ITEMS I "
			    + "  	INNER JOIN PEDIDOS_ADMIN_LOC PED ON PED.NUMERO_PEDIDO = I.NUM_PEDIDO_ADMIN_LOCAL "
			    + "  	LEFT OUTER JOIN PRESENTACIONES PR ON PR.ID_ITEM = I.ID "
			    + "  	INNER JOIN USUARIOS U ON U.ID = PED.ID_ADMIN_LOCAL "
			    + "     INNER JOIN PRODUCTOS P ON P.ID = I.ID_PRODUCTO "

			    + "   	LEFT OUTER JOIN (SELECT P.NOMBRE AS NOMBRE_PRODUCTO_BODEGA, I.ID_BODEGA, I.ID AS ID_ITEM_BODEGA, PR.PESO AS PESO_PESEN_BODEGA "
			    + "                    FROM ITEMS I " + "                          INNER JOIN BODEGAS B ON I.ID_BODEGA = B.ID "
			    + "                          INNER JOIN PRESENTACIONES PR ON PR.ID_ITEM = I.ID "
			    + "                          INNER JOIN PRODUCTOS P ON P.ID = I.ID_PRODUCTO "
			    + "                    WHERE I.ID_BODEGA IS NOT NULL) PB "
			    + "        ON P.NOMBRE = PB.NOMBRE_PRODUCTO_BODEGA AND PB.PESO_PESEN_BODEGA = PR.PESO " + " WHERE PB.ID_BODEGA = '"
			    + idBodega + "'" + " GROUP BY U.CORREO";

		    prepStmt = conexion.prepareStatement(sqlPedidosAdmin);
		    prepStmts.add(prepStmt);
		    rs = prepStmt.executeQuery();

		    String mensaje = "Estimado administrador, Cabandes le informa por motivos mayores se tuvo que cerrar una Bodega en la que podía haber productos"
		    		   + "de un pedido que usted hubiera realizado. Es por esto que nos disculpamos con anticipación por las"
		    		   + "demoras e inconvenientes que esto pueda causar. \n"
		    		   + "Administración de cabandes.";
		    
		    String adminsNotificados = "";
		    
		    while (rs.next())
		    {
			// NOTIFICAR ADMINS LOCAL
			
			String correoAdmin = rs.getString("CORREO_ADMIN");
			adminsNotificados += "\n"+ correoAdmin;
			
			String sqlEnviarMensaje = "INSERT INTO BUZON_DE_SALIDA (DESTINATARIO, MENSAJE) VALUES ( '"
						+ correoAdmin +"', '" + mensaje + "' ) ";
			
			prepStmt = conexion.prepareStatement(sqlEnviarMensaje);
			prepStmts.add(prepStmt);
			prepStmt.executeUpdate();
		    }

		    // PEDIDOS CON SUS COMPRADORES POR MAYOR, ITEMS Y LAS BODEGAS (SI
		    // HAY) QUE TIENEN ITEMS QUE PUEDEN SATISFACER EL PEDIDO
		    String sqlPedidosPorMayor = "SELECT U.CORREO AS CORREO_COMPRADOR"
			    + " FROM ITEMS I "
			    + " 	   INNER JOIN PEDIDOS_POR_MAYOR PED ON PED.NUMERO_PEDIDO = I.NUM_PEDIDO_POR_MAYOR"
			    + " 	   LEFT OUTER JOIN PRESENTACIONES PR ON PR.ID_ITEM = I.ID"
			    + "     INNER JOIN USUARIOS U ON U.ID = PED.ID_COMPRADOR_POR_MAYOR"
			    + "     INNER JOIN PRODUCTOS P ON P.ID = I.ID_PRODUCTO"

			    + "   	LEFT OUTER JOIN (SELECT P.NOMBRE AS NOMBRE_PRODUCTO_BODEGA, I.ID_BODEGA, I.ID AS ID_ITEM_BODEGA, PR.PESO AS PESO_PESEN_BODEGA"
			    + "                    FROM ITEMS I " + "                          INNER JOIN BODEGAS B ON I.ID_BODEGA = B.ID"
			    + "                          INNER JOIN PRESENTACIONES PR ON PR.ID_ITEM = I.ID"
			    + "                          INNER JOIN PRODUCTOS P ON P.ID = I.ID_PRODUCTO"
			    + "                    WHERE I.ID_BODEGA IS NOT NULL) PB"
			    + "        ON P.NOMBRE = PB.NOMBRE_PRODUCTO_BODEGA AND PB.PESO_PESEN_BODEGA = PR.PESO" + " WHERE PB.ID_BODEGA = '"
			    + idBodega + "'" + " GROUP BY U.CORREO";

		    prepStmt = conexion.prepareStatement(sqlPedidosPorMayor);
		    prepStmts.add(prepStmt);
		    rs = prepStmt.executeQuery();
		    
		    mensaje = "Estimado comprador, Cabandes le informa por motivos mayores se tuvo que cerrar una Bodega en la que podía haber productos"
		    		   + "de un pedido que usted hubiera realizado. Es por esto que nos disculpamos con anticipación por las"
		    		   + "demoras e inconvenientes que esto pueda causar. \n"
		    		   + "Administración de cabandes.";
		    
		    String compradoresNotificados = "";

		    while (rs.next())
		    {
			// NOTIFICAR COMPRADORES
			
			String correoComprador = rs.getString("CORREO_COMPRADOR");
			compradoresNotificados += "\n" + correoComprador;
			
			String sqlEnviarMensaje = "INSERT INTO BUZON_DE_SALIDA (DESTINATARIO, MENSAJE) VALUES ( '"
						+ correoComprador +"' , '" + mensaje + "' ) ";
			
			prepStmt = conexion.prepareStatement(sqlEnviarMensaje);
			prepStmts.add(prepStmt);
			prepStmt.executeUpdate();
		    }

		    // OBTENER TODAS LAS BODEGAS CON SUS CAPACIDADES EN USO, SE HACE SELECT FOR UPDATE PARA QUE NADIE PUEDA CAMBIAR NADA MIENTRAS
		    //YO LO USO
		    
		    String sqlBloquearBodegas = "SELECT * FROM BODEGAS FOR UPDATE";
		    prepStmt = conexion.prepareStatement(sqlBloquearBodegas);
		    rs = prepStmt.executeQuery();
		    
		    String sqlBodegasConCapacidadEnUso =
		    	      " SELECT ID_BODEGA, SUM(PESO_TOTAL) AS CAPACIDAD_EN_USO, CAPACIDAD_TOTAL, CAPACIDAD_TOTAL -SUM(PESO_TOTAL) AS CAPACIDAD_RESTANTE"
			    + " FROM (SELECT B.ID AS ID_BODEGA, I.ID AS ID_ITEM, CASE WHEN PR.PESO IS NULL THEN 0 ELSE PR.PESO END AS PESO, "
			    + "        CASE WHEN PR.NUMERO_UNIDADES IS NULL THEN 0 ELSE PR.NUMERO_UNIDADES END AS NUMERO_UNIDADES,"
			    + "        (CASE WHEN PR.NUMERO_UNIDADES IS NULL THEN 0 ELSE PR.NUMERO_UNIDADES END) * (CASE WHEN PR.PESO IS NULL THEN 0 ELSE PR.PESO END) AS PESO_TOTAL,"
			    + "        B.CAPACIDAD AS CAPACIDAD_TOTAL"
			    + "        FROM ITEMS I LEFT OUTER JOIN PRESENTACIONES PR ON I.ID = PR.ID_ITEM RIGHT OUTER JOIN BODEGAS B ON I.ID_BODEGA = B.ID"
			    + "      )"
			    + " GROUP BY ID_BODEGA, CAPACIDAD_TOTAL"
			    + " ORDER BY CAPACIDAD_RESTANTE DESC";

		    prepStmt = conexion.prepareStatement(sqlBodegasConCapacidadEnUso);
		    prepStmts.add(prepStmt);
		    rs = prepStmt.executeQuery();

		    ArrayList<BodegaVO> bodegas = new ArrayList<BodegaVO>();

		    while (rs.next())
		    {
			int id = rs.getInt("ID_BODEGA");
			double capacidadEnUso = rs.getDouble("CAPACIDAD_EN_USO");
			double capacidadTotal = rs.getDouble("CAPACIDAD_TOTAL");
			double capacidadRestante = rs.getDouble("CAPACIDAD_RESTANTE");
			BodegaVO bodegaVO = new BodegaVO(id, capacidadEnUso, capacidadTotal, capacidadRestante);

			bodegas.add(bodegaVO);
		    }

		    // OBTENER TODOS LOS ITEMS DE LA BODEGA A CERRAR, SE HACE SELECT FOR UPDATE PARA QUE NADIE PUEDA CAMBIAR NADA MIENTRAS
		    //YO LO USO
		    
		    String sqlBloquearItems = 
			    "SELECT I.ID_BODEGA, I.ID, I.ID_PRODUCTO, I.FECHA_INGRESO"
			    +"    FROM ITEMS I"
			    +"    WHERE I.ID_BODEGA = '" + idBodega + "' FOR UPDATE";
		    prepStmt = conexion.prepareStatement(sqlBloquearItems);
		    rs = prepStmt.executeQuery();
		
		    String sqlItemsEnBodegaACerrar =
		    	      " SELECT ID_ITEM, ID_PRODUCTO, SUM (PESO_TOTAL) AS PESO_TOTAL, FECHA_INGRESO"
			    + " FROM"
			    + "    (SELECT B.ID AS ID_BODEGA, I.ID AS ID_ITEM, I.ID_PRODUCTO, I.FECHA_INGRESO, CASE WHEN PR.PESO IS NULL THEN 0 ELSE PR.PESO END AS PESO, "
			    + "           CASE WHEN PR.NUMERO_UNIDADES IS NULL THEN 0 ELSE PR.NUMERO_UNIDADES END AS NUMERO_UNIDADES, "
			    + "           (CASE WHEN PR.NUMERO_UNIDADES IS NULL THEN 0 ELSE PR.NUMERO_UNIDADES END) * (CASE WHEN PR.PESO IS NULL THEN 0 ELSE PR.PESO END) AS PESO_TOTAL"
			    + "    FROM ITEMS I LEFT OUTER JOIN PRESENTACIONES PR ON I.ID = PR.ID_ITEM RIGHT OUTER JOIN BODEGAS B ON I.ID_BODEGA = B.ID"
			    + "    WHERE B.ID = '" + idBodega + "')" 
			    + " GROUP BY ID_ITEM, ID_PRODUCTO, FECHA_INGRESO" 
			    + " ORDER BY ID_ITEM ";

		    prepStmt = conexion.prepareStatement(sqlItemsEnBodegaACerrar);
		    prepStmts.add(prepStmt);
		    rs = prepStmt.executeQuery();

		    ArrayList<ItemVO> itemsEnBodegaACerrar = new ArrayList<ItemVO>();

		    while (rs.next())
		    {
			ItemVO itemVO = new ItemVO();
			itemVO.ID = String.valueOf(rs.getInt("ID_ITEM"));
			itemVO.idProducto = rs.getInt("ID_PRODUCTO");
			itemVO.pesoTotal = rs.getDouble("PESO_TOTAL");
			itemVO.fechaIngreso = rs.getString("FECHA_INGRESO");
			
			itemsEnBodegaACerrar.add(itemVO);
		    } 
		    
		    // INTENTAR MOVER ITEMS A LAS OTRAS BODEGAS
		    boolean sePudoMoverProducto = false;

		    for (ItemVO itemEnBodegaACerrar : itemsEnBodegaACerrar)
		    {
			for (BodegaVO bodegaVO : bodegas)
			{
			    if (bodegaVO.capacidadRestante > itemEnBodegaACerrar.pesoTotal && bodegaVO.id != Integer.parseInt(idBodega) )
			    {
				String sqlActualizacionBodega = "UPDATE ITEMS I SET I.ID_BODEGA = '" + bodegaVO.id + "' " + " WHERE I.ID = '"
					+ itemEnBodegaACerrar.ID + "'";
				
				prepStmt = conexion.prepareStatement(sqlActualizacionBodega);
				prepStmts.add(prepStmt);
				prepStmt.executeUpdate();
				    
				sePudoMoverProducto = true;
				break;
			    }
			}
			if (!sePudoMoverProducto)
			{
			    conexion.rollback(); //DEVOLVER TODO !!
			    return "La bodega " + idBodega +" no pudo ser cerrada, algún producto no puede ser movido.";
			}
		    }
		    	    		    	    
		    transaction.commit();
		    
		    return "La bodega "+ idBodega + " fue cerrada exitosamente. Todos los items que estaban en ella fueron repartidos "
			+ "entre el resto de bodegas, emepezando por la que más espacio restante tuviera."
			+ "\n Un mensaje fue enviado a los siguientes clientes: \n\n"
			+ "Compradores: "
			+ compradoresNotificados
			+"Administradores: "
			+ adminsNotificados;
	    }
	    
	    if (llamadoLocal && !propia) Cabandes.getInstance().enviarSolicitud(Mensajes.CERRAR_BODEGA+":"+idBodega);
	}
	catch (Exception e)
	{
	    try
	    {
		transaction.setRollbackOnly();
	    }
	    catch (IllegalStateException e1)
	    {
		e1.printStackTrace();
	    }
	    catch (SystemException e1)
	    {
		e1.printStackTrace();
	    }
	    e.printStackTrace();
	}
	finally
	{
	    for (PreparedStatement prepStmt : prepStmts)
	    {
		if (prepStmt != null)
		try
		{
		    prepStmt.close();
		}
		catch (SQLException exception)
		{

		}
	    }
	    try
	    {
		closeConnection(conexion);
	    }
	    catch (Exception e)
	    {
		e.printStackTrace();
	    }
	}
	
	

	return null;
    }

    public synchronized void consultarBodegas(String tipo, Integer porcentajeUtilizado)
    {
	PreparedStatement prepStmt = null;

	try
	{
	    establecerConexion();

	    String sqlBodegasConCapacidadEnUso =   
	    	"	       SELECT I.ID_BODEGA, B.CAPACIDAD AS CAPACIDAD_TOTAL, "
	    	+ "          SUM (PR.NUMERO_UNIDADES * PR.PESO) AS CAPACIDAD_EN_USO, "
	    	+ "			 B.CAPACIDAD - SUM (PR.NUMERO_UNIDADES * PR.PESO) AS CAPACIDAD_RESTANTE,"
	    	+ "		     (SUM(PR.NUMERO_UNIDADES * PR.PESO)*100 / B.CAPACIDAD) AS PORCENTAJE_UTILIZADO "  
	    	+ "        FROM ITEMS I "
		    + "		   INNER JOIN PRESENTACIONES PR ON I.ID = PR.ID_ITEM "
		    + "		   INNER JOIN BODEGAS B ON I.ID_BODEGA = B.ID"
		    + "		   INNER JOIN TIPOS T ON T.ID = B.ID_TIPO" 
		    + " WHERE I.ID_BODEGA IS NOT NULL " + (tipo != null? " AND T.NOMBRE = '" + tipo + "'" : "")
		    + "        GROUP BY I.ID_BODEGA, B.CAPACIDAD "
		    + (porcentajeUtilizado != null?
		    		   "HAVING SUM(PR.NUMERO_UNIDADES * PR.PESO)*100 / B.CAPACIDAD > 0" : "");

	    prepStmt = conexion.prepareStatement(sqlBodegasConCapacidadEnUso);

	    long startTime = System.currentTimeMillis();
	    
	    ResultSet rs = prepStmt.executeQuery();
	    
	    long elapsedTime = System.currentTimeMillis() - startTime;
	    System.out.println(sqlBodegasConCapacidadEnUso);
	    System.out.println("Bodegas (peor caso): "+ elapsedTime);
	    
	    ArrayList<BodegaVO> bodegas = new ArrayList<BodegaVO>();
	    while (rs.next())
	    {
		int id = Integer.parseInt(rs.getString("ID_BODEGA"));
		double capacidadEnUso = rs.getDouble("CAPACIDAD_EN_USO");
		double capacidadTotal = rs.getDouble("CAPACIDAD_TOTAL");
		double capacidadRestante = rs.getDouble("CAPACIDAD_RESTANTE");
		double porcentajeUtilizadoVO = rs.getDouble("PORCENTAJE_UTILIZADO");
		BodegaVO bodegaVO = new BodegaVO(id, capacidadEnUso, capacidadTotal, capacidadRestante);
		bodegaVO.porcentajeUtilizado = porcentajeUtilizadoVO;

		bodegas.add(bodegaVO);
	    }
	    System.out.println(bodegas.size());
	    
	    Cabandes.getInstance().setListenerRespuestas(this);
	    Cabandes.getInstance().enviarSolicitud(Mensajes.CONSULTAR_BODEGAS+":");
	    
	    try
	    {
		wait(5000);
	    }
	    catch (InterruptedException e)
	    {
		e.printStackTrace();
	    }
	    
	    if (respuestaOtroComponente != null && respuestaOtroComponente.split(":")[0].equals(Mensajes.CONSULTAR_BODEGAS)) 
	    {
		String[] bodegasOtroComponente = respuestaOtroComponente.split(":");
		
		for (int i = 1; i < bodegasOtroComponente.length; i++)
		{
		    String bodega = bodegasOtroComponente[i];
		    String[] componentes = bodega.split("-");
		    
		    BodegaVO bodegaVO = new BodegaVO(Integer.parseInt(componentes[0]), -1, Double.parseDouble(componentes[1]), -1);
		    bodegaVO.porcentajeUtilizado = Integer.parseInt(componentes[2]);
		    bodegaVO.propia = false;
		    bodegas.add(bodegaVO);
		}
	    }	

	    Cabandes.getInstance().bodegasUltimaConsulta = bodegas;
	    respuestaOtroComponente = null;
	}
	catch (Exception e)
	{
	    e.printStackTrace();
	}
	finally
	{
	    if (prepStmt != null)
		try
		{
		    prepStmt.close();
		}
		catch (SQLException exception)
		{

		}
	    try
	    {
		closeConnection(conexion);
	    }
	    catch (Exception e)
	    {
		e.printStackTrace();
	    }
	}
    }

    public ArrayList<IItem> eliminarItemsVencidos()
    {
	ArrayList<Item> deleted = new ArrayList<Item>();
	PreparedStatement prepStmt = null;
	try
	{
	    establecerConexion();
	    String sql = "select p.NOMBRE, i.FECHA_INGRESO, p.DIAS_VENCIMIENTO"
		    + " from productos p inner join items i on p.ID = i.ID_PRODUCTO" + "inner join tipos t on p.ID_TIPO = t.ID";

	    prepStmt = conexion.prepareStatement(sql);
	    ResultSet rs = prepStmt.executeQuery();

	    while (rs.next())
	    {

		int units = rs.getInt("NUMERO_UNIDADES");
		String tipo = rs.getString("NOMBRE_1");
		String nombre = rs.getString("NOMBRE");

		boolean x = false;
		if (tipo.equals("Refrigerado"))
		    x = true;

		Date fecha = rs.getDate("FECHA_INGRESO");
		Date fechafinal = new Date();
		Date presente = new Date();

		Producto prod = new Producto(0, tipo, fechafinal, x, nombre);

		Item it = new Item(units, null, null, null, prod, null);

		Calendar c = Calendar.getInstance();
		c.setTime(fecha);
		int dias = rs.getInt("DIAS_VENCIMIENTO");
		c.add(Calendar.DATE, dias);
		fechafinal = c.getTime();

		if (presente.after(fechafinal))
		    deleted.add(it);
	    }

	}
	catch (Exception e)
	{
	    e.printStackTrace();
	}
	finally
	{
	    if (prepStmt != null)
		try
		{
		    prepStmt.close();
		}
		catch (SQLException exception)
		{

		}
	    try
	    {
		closeConnection(conexion);
	    }
	    catch (Exception e)
	    {
		e.printStackTrace();
	    }
	}
	return null;
    }

    public void cerrarPedidoProveedor(String string)
    {
    }

    public void generarPedidoAdminLocal(ArrayList<IItem> itemsSeleccionados, String idAdminLocal)
    {
	int idAd = Integer.parseInt(idAdminLocal);
	PreparedStatement prepStmt = null;
	try
	{
	    establecerConexion();
	    String sql = "select max(numero_pedido) from PEDIDOS_ADMIN_LOC";

	    prepStmt = conexion.prepareStatement(sql);
	    ResultSet resultSet = prepStmt.executeQuery();

	    int x = resultSet.getInt(1) + 1;

	    String sql2 = "insert into PEDIDOS_ADMIN_LOC values (" + x + ",'22/10/2014', " + idAd + ",null,N)";

	    prepStmt = conexion.prepareStatement(sql2);
	    prepStmt.executeUpdate();

	    for (int i = 0; i < itemsSeleccionados.size(); i++)
	    {
		Item s = (Item) itemsSeleccionados.get(i);
		String sql3 = "insert into PEDIDOS_ADMIN_LOC_ITEMS values(" + x + "," + s + ")";
		prepStmt = conexion.prepareStatement(sql3);
		prepStmt.executeUpdate();
	    }

	}

	catch (Exception e)
	{
	    e.printStackTrace();
	}
	finally
	{
	    if (prepStmt != null)
		try
		{
		    prepStmt.close();
		}
		catch (SQLException exception)
		{

		}
	    try
	    {
		closeConnection(conexion);
	    }
	    catch (Exception e)
	    {
		e.printStackTrace();
	    }
	}
    }

    public boolean registrarVentaItemACompradorMayorista(String id)
    {
	ArrayList<PreparedStatement> prepStmts = new ArrayList<PreparedStatement>();
	try
	{
	    PreparedStatement prepStmt = null;
		
	    establecerConexion();
	    conexion.setAutoCommit(false); //CONCURRENCIA
	    conexion.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
	    
	    String sqlSelect = "SELECT ID_PRODUCTO, ID_LOCAL, ID_BODEGA"
	    			+ " FROM ITEMS I INNER JOIN PRODUCTOS P ON I.ID_PRODUCTO = P.ID "
	    			+ " WHERE I.ID = '"+ id + "' FOR UPDATE";
	    
	    prepStmt = conexion.prepareStatement(sqlSelect);
	    prepStmts.add(prepStmt);
	    ResultSet rs = prepStmt.executeQuery();
	    
	    String idProducto = null;
	    String idLocal = null;
	    String idBodega = null;
	    
	    while (rs.next())
	    {
		idProducto = rs.getString("ID_PRODUCTO");
		idLocal = rs.getString("ID_LOCAL");
		idBodega = rs.getString("ID_BODEGA");
	    }
	    
	    if (idLocal != null || idBodega != null)
	    {
		String sqlBorrarItem = "DELETE FROM ITEMS I WHERE I.ID = '"+ id + "'";
		
		prepStmt = conexion.prepareStatement(sqlBorrarItem);
		prepStmts.add(prepStmt);
		prepStmt.executeUpdate();
		
		String sqlRegistrarHistorial;
		
		if (idLocal != null)
		{
		    sqlRegistrarHistorial = "INSERT INTO HISTORIAL_MOVIMIENTOS "
				+ " (ID_PRODUCTO, ID_LOCAL_ORIGEN, CANTIDAD_TOTAL_PRODUCTO, ID_ITEM) "
				+ " VALUES ('"+idProducto+"', '"+idLocal+"', '12', '"+id+"')";
		}
		else
		{
		    sqlRegistrarHistorial = "INSERT INTO HISTORIAL_MOVIMIENTOS "
				+ " (ID_PRODUCTO, ID_BODEGA_ORIGEN, CANTIDAD_TOTAL_PRODUCTO, ID_ITEM) "
				+ " VALUES ('"+idProducto+"', '"+idBodega+"', '12', '"+id+"')";
		}
			
		prepStmt = conexion.prepareStatement(sqlRegistrarHistorial);
		prepStmts.add(prepStmt);
		prepStmt.executeUpdate();
		
		conexion.commit();
		return true;
	    }
	    else
	    {
		conexion.rollback();
	    }
	    
	}
	catch (Exception e)
	{
	    try
	    {
		conexion.rollback();
	    }
	    catch (SQLException e1)
	    {
		e1.printStackTrace();
	    }
	    e.printStackTrace();
	}
	finally
	{
	    for (PreparedStatement prepStmt : prepStmts)
	    {
		if (prepStmt != null)
		try
		{
		    prepStmt.close();
		}
		catch (SQLException exception)
		{

		}
	    }
	    try
	    {
		closeConnection(conexion);
	    }
	    catch (Exception e)
	    {
		e.printStackTrace();
	    }
	}
	return false;
    }
    
    public boolean registrarMovimientoItemBodegaALocal (String idItem, String idLocalDestino)
    {
	ArrayList<PreparedStatement> prepStmts = new ArrayList<PreparedStatement>();
	try
	{
	    PreparedStatement prepStmt = null;
		
	    establecerConexion();
	    conexion.setAutoCommit(false); //CONCURRENCIA
	    conexion.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
	    
	    String sqlSelect = "SELECT ID_PRODUCTO, ID_LOCAL, ID_BODEGA"
	    			+ " FROM ITEMS I INNER JOIN PRODUCTOS P ON I.ID_PRODUCTO = P.ID  "
	    			+ " WHERE I.ID = '"+ idItem + "' FOR UPDATE";
	    
	    prepStmt = conexion.prepareStatement(sqlSelect);
	    prepStmts.add(prepStmt);
	    ResultSet rs = prepStmt.executeQuery();
	    
	    String idProducto = null;
	    String idBodega = null;
	    
	    while (rs.next())
	    {
		idProducto = rs.getString("ID_PRODUCTO");
		idBodega = rs.getString("ID_BODEGA");
	    }
	    
	    if (idBodega != null)
	    {
		String sqlBorrarItem = "UPDATE ITEMS I SET ID_BODEGA = NULL, ID_LOCAL = '"+idLocalDestino +"' "
				    + " WHERE I.ID = '"+ idItem + "'";
		
		prepStmt = conexion.prepareStatement(sqlBorrarItem);
		prepStmts.add(prepStmt);
		prepStmt.executeUpdate();
		
		String sqlRegistrarHistorial = "INSERT INTO HISTORIAL_MOVIMIENTOS "
			+ " (ID_PRODUCTO, ID_BODEGA_ORIGEN, ID_LOCAL_DESTINO, CANTIDAD_TOTAL_PRODUCTO, ID_ITEM) "
			+ " VALUES ('"+idProducto+"', '"+idBodega+"', '"+idLocalDestino+"', '12', '"+idItem+"')";
		
		prepStmt = conexion.prepareStatement(sqlRegistrarHistorial);
		prepStmts.add(prepStmt);
		prepStmt.executeUpdate();
		
		conexion.commit(); 
		return true;
	    }
	    else
	    {
		conexion.rollback();
	    }
	    
	}
	catch (Exception e)
	{
	    try
	    {
		conexion.rollback();
	    }
	    catch (SQLException e1)
	    {
		e1.printStackTrace();
	    }
	    e.printStackTrace();
	}
	finally
	{
	    for (PreparedStatement prepStmt : prepStmts)
	    {
		if (prepStmt != null)
		try
		{
		    prepStmt.close();
		}
		catch (SQLException exception)
		{

		}
	    }
	    try
	    {
		closeConnection(conexion);
	    }
	    catch (Exception e)
	    {
		e.printStackTrace();
	    }
	}
	return false;
    }
    
    public ItemVO registrarEntradaItemABodega (int idBodega, ItemVO itemVOEntrante, boolean historial)
    {
    	ArrayList<PreparedStatement> prepStmts = new ArrayList<PreparedStatement>();
    	try
    	{
    		PreparedStatement prepStmt = null;

    		establecerConexion();
    		conexion.setAutoCommit(false);

    		Integer idTipoBodega = null;
    		Integer idTipoProducto = null;
    		
    		String sqlVerificarTipoBodega = "SELECT ID_TIPO FROM BODEGAS B WHERE B.ID = '"+idBodega+"'";
    				
    		prepStmt = conexion.prepareStatement(sqlVerificarTipoBodega);
    		prepStmts.add(prepStmt);
    		ResultSet rs = prepStmt.executeQuery();
    		
    		while (rs.next())
    		{
    			idTipoBodega = rs.getInt("ID_TIPO");
    		}
    		
    		String sqlVerificarTipoProducto = "SELECT ID_TIPO FROM PRODUCTOS P WHERE P.ID = '"+itemVOEntrante.idProducto+"'";
			
    		prepStmt = conexion.prepareStatement(sqlVerificarTipoProducto);
    		prepStmts.add(prepStmt);
    		rs = prepStmt.executeQuery();
    		
    		while (rs.next())
    		{
    			idTipoProducto = rs.getInt("ID_TIPO");
    		}
    		
    		if (idTipoProducto != null && idTipoBodega != null && idTipoBodega.equals(idTipoProducto))
    		{
    			String sqlInsertar = "INSERT INTO ITEMS (ID_PRODUCTO, ID_BODEGA) VALUES ('"+itemVOEntrante.idProducto+"', '"+idBodega+"')";

        		prepStmt = conexion.prepareStatement(sqlInsertar,  new String[] { "ID" });
        		prepStmts.add(prepStmt);
        		int i = prepStmt.executeUpdate();
        		
        		Integer idItem = null;
        		
        		if (i > 0)
    			{
    				rs = prepStmt.getGeneratedKeys();
    				while (rs.next())
    				{
    					idItem = rs.getInt(1);
    				} 				
    			}
        		
        		ItemVO itemVO = new ItemVO();
        		itemVO.idBodega = idBodega;
        		itemVO.idProducto = itemVOEntrante.idProducto;
        		itemVO.ID = idItem.toString();
        		
        		if (historial == true)
        		{
        			String sqlRegistrarHistorial = "INSERT INTO HISTORIAL_MOVIMIENTOS "
            				+ " (ID_PRODUCTO, ID_BODEGA_DESTINO, CANTIDAD_TOTAL_PRODUCTO, ID_ITEM) "
            				+ " VALUES ('"+itemVO.idProducto+"', '"+idBodega+"', '"+ itemVOEntrante.pesoTotal +"', '"+idItem+"');";

            		prepStmt = conexion.prepareStatement(sqlRegistrarHistorial);
            		prepStmts.add(prepStmt);
            		prepStmt.executeUpdate();
        		}
        			
        		conexion.commit();
        		
        		return itemVO;
    		}
    		else 
    		{
    			conexion.rollback();
    			return null;
    		}	

    	}
    	catch (Exception e)
    	{
    		try
			{
				conexion.rollback();
			}
			catch (SQLException e1)
			{
				e1.printStackTrace();
			}
    		e.printStackTrace();
    	}
    	finally
    	{
    		for (PreparedStatement prepStmt : prepStmts)
    		{
    			if (prepStmt != null)
    				try
    			{
    					prepStmt.close();
    			}
    			catch (SQLException exception)
    			{

    			}
    		}
    		try
    		{
    			closeConnection(conexion);
    		}
    		catch (Exception e)
    		{
    			e.printStackTrace();
    		}
    	}
		return null;
    }

    public boolean registrarVentaPresentacion(String id)
    {
	ArrayList<PreparedStatement> prepStmts = new ArrayList<PreparedStatement>();
	try
	{
	    PreparedStatement prepStmt = null;
		
	    establecerConexion();
	    conexion.setAutoCommit(false);
	    conexion.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
	    
	    String sqlSelect = "SELECT ID_PRODUCTO, ID_ITEM, ID_LOCAL, PESO*NUMERO_UNIDADES AS CANTIDAD, PESO"
	    			+ " FROM ITEMS I INNER JOIN PRODUCTOS P ON I.ID_PRODUCTO = P.ID INNER JOIN PRESENTACIONES PR ON PR.ID_ITEM = I.ID "
	    			+ " WHERE PR.ID = '"+ id + "' FOR UPDATE";
	    
	    prepStmt = conexion.prepareStatement(sqlSelect);
	    prepStmts.add(prepStmt);
	    ResultSet rs = prepStmt.executeQuery();
	    
	    String idProducto = null;
	    String idLocal = null;
	    String idItem = null;
	    int cantidad = -1;
	    int peso = 0;
	    
	    while (rs.next())
	    {
		idProducto = rs.getString("ID_PRODUCTO");
		idItem = rs.getString("ID_ITEM");
		idLocal = rs.getString("ID_LOCAL");
		cantidad = rs.getInt("CANTIDAD");
		peso = rs.getInt("PESO");
	    }
	    
	    if (idLocal != null)
	    {
		String sqlBorrarItem = "DELETE FROM PRESENTACIONES PR WHERE PR.ID = '"+ id + "'";
		
		prepStmt = conexion.prepareStatement(sqlBorrarItem);
		prepStmts.add(prepStmt);
		prepStmt.executeUpdate();
		
		String sqlRegistrarHistorial = "INSERT INTO HISTORIAL_MOVIMIENTOS "
			+ " (ID_PRODUCTO, ID_LOCAL_ORIGEN, CANTIDAD_TOTAL_PRODUCTO, ID_ITEM) "
			+ " VALUES ('"+idProducto+"', '"+idLocal+"', '"+peso*cantidad+"', '"+idItem+"')";
		
		prepStmt = conexion.prepareStatement(sqlRegistrarHistorial);
		prepStmts.add(prepStmt);
		prepStmt.executeUpdate();
		
		conexion.commit();
		return true;
	    }
	    else
	    {
		conexion.rollback();
	    }
	    
	}
	catch (Exception e)
	{
	    try
	    {
		conexion.rollback();
	    }
	    catch (SQLException e1)
	    {
		e1.printStackTrace();
	    }
	    e.printStackTrace();
	}
	finally
	{
	    for (PreparedStatement prepStmt : prepStmts)
	    {
		if (prepStmt != null)
		try
		{
		    prepStmt.close();
		}
		catch (SQLException exception)
		{

		}
	    }
	    try
	    {
		closeConnection(conexion);
	    }
	    catch (Exception e)
	    {
		e.printStackTrace();
	    }
	}
	return false;
    }

    public boolean satisfacerPedido(String numeroPedidoSeleccionado)
    {
	ArrayList<PreparedStatement> prepStmts = new ArrayList<PreparedStatement>();
	try
	{
	    PreparedStatement prepStmt = null;
		
	    establecerConexion();
	    conexion.setAutoCommit(false); 
	    conexion.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
	    
	    //ITEMS EN BODEGAS QUE PUEDEN SATISFACER EL PEDIDO
	    String sqlSelect = "SELECT PEDIDO.NUM_PEDIDO_ADMIN_LOCAL, AL.ID_LOCAL, PRES.ID_PRODUCTO, PRES.ID_BODEGA, PEDIDO.NUMERO_UNIDADES AS NUM_UNIDADES_REQ, PRES.NUMERO_UNIDADES AS NUM_UNIDADES_DISP,"+
                               " PRES.PESO"+
                               " FROM (SELECT * FROM ITEMS I INNER JOIN PRESENTACIONES PR ON I.ID = PR.ID_ITEM WHERE I.NUM_PEDIDO_ADMIN_LOCAL = '"+numeroPedidoSeleccionado+"') PEDIDO"+
                               "       INNER JOIN "+
                               "      (SELECT * FROM ITEMS I INNER JOIN PRESENTACIONES PR ON I.ID = PR.ID_ITEM) PRES"+
                               "      ON PEDIDO.ID_PRODUCTO = PRES.ID_PRODUCTO "+
                               "      AND PRES.ID_BODEGA IS NOT NULL AND PEDIDO.PESO = PRES.PESO AND PRES.NUMERO_UNIDADES >= PEDIDO.NUMERO_UNIDADES"
                               + "   INNER JOIN PEDIDOS_ADMIN_LOC PED ON PED.NUMERO_PEDIDO = PEDIDO.NUM_PEDIDO_ADMIN_LOCAL"
                               + "   INNER JOIN ADMINS_LOCAL AL ON PED.ID_ADMIN_LOCAL = AL.ID";	
	    
	    prepStmt = conexion.prepareStatement(sqlSelect);
	    prepStmts.add(prepStmt);
	    ResultSet rs = prepStmt.executeQuery();
	    	    
	    while (rs.next())
	    {
		
	    }
	    
	    //PASAR LOS ITEMS NECESARIOS DE LAS BODEGAS AL LOCAL
	    
	    
	    //ENVIAR SOLICITUD PARA INTENTAR SATISFACER LAS PRESENTACIONES QUE FALTAN
	    
	    Cabandes.getInstance().setListenerRespuestas(this);
	    
	    Cabandes.getInstance().enviarSolicitud(Mensajes.SATISFACER_PEDIDO+":");
	    
	    
	}
	catch (Exception e)
	{
	    try
	    {
		conexion.rollback();
	    }
	    catch (SQLException e1)
	    {
		e1.printStackTrace();
	    }
	    e.printStackTrace();
	}
	finally
	{
	    for (PreparedStatement prepStmt : prepStmts)
	    {
		if (prepStmt != null)
		try
		{
		    prepStmt.close();
		}
		catch (SQLException exception)
		{

		}
	    }
	    try
	    {
		closeConnection(conexion);
	    }
	    catch (Exception e)
	    {
		e.printStackTrace();
	    }
	}
	return false;
    }

    public void hacerOfertaPorParteDelProveedorAlPedidoDeProveedor(String idProveedor, String numeroPedido, String cantidadAProveer,
	    String fechaEsperadaEntrega, String costo)
    {
	int cantAProveer = Integer.parseInt(cantidadAProveer);
	int cost = Integer.parseInt(costo);
	int num = Integer.parseInt(numeroPedido);
	int nid = Integer.parseInt(idProveedor);

	PreparedStatement prepStmt = null;
	try
	{
	    establecerConexion();
	    String sql = "select max(id) from ofertas;";

	    prepStmt = conexion.prepareStatement(sql);
	    ResultSet rs = prepStmt.executeQuery();
	    int id = rs.getInt("1");

	    String agregar = "insert into OFERTAS values (" + id + "," + cantAProveer / cost + "," + costo + "," + num + "," + nid + ")";
	    prepStmt = conexion.prepareStatement(agregar);
	    prepStmt.executeUpdate();
	}
	catch (Exception e)
	{
	    e.printStackTrace();
	}
	finally
	{
	    if (prepStmt != null)
		try
		{
		    prepStmt.close();
		}
		catch (SQLException exception)
		{

		}
	    try
	    {
		closeConnection(conexion);
	    }
	    catch (Exception e)
	    {
		e.printStackTrace();
	    }
	}
    }

    // ------------------------------------
    // Entro yo
    // ------------------------------------

    public String registrarBodega(String type1, int capacity)
    {
	// Se agrega un id = 0 porque el sistema lo asigna después.
	BodegaVO bod = new BodegaVO(0, 0, capacity, capacity);

	ArrayList<BodegaVO> bodegas = new ArrayList<BodegaVO>();
	bodegas.add(bod);
	String x = "";
	ArrayList<Integer> idsBodegas = new ArrayList<Integer>();
	ArrayList<String> productosPasados = new ArrayList<String>();

	PreparedStatement prepStmt = null;
	try
	{
	    establecerConexion();
	    conexion.setAutoCommit(false); //CONCURRENCIA
	    //Evita que puedan manipular los datos mientras se actualiza algo
	    conexion.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
	    boolean si = false;
	    
	    // Selecciona el id del tipo que entra por parámetro
	    String tipo = "select id from tipos where NOMBRE = '" + type1 + "'";
	    prepStmt = conexion.prepareStatement(tipo);
	    ResultSet rs = prepStmt.executeQuery();
	    int type2 = 0;
	    if (rs.next())
		type2 = rs.getInt(1);

	    // Inserta en la BD la bodega
	    String sql = "insert into Bodegas (capacidad, id_tipo) values (" + capacity + ", " + type2 + ")";
	    prepStmt = conexion.prepareStatement(sql, new String []{"ID"});
	    prepStmt.executeUpdate();

	    rs = prepStmt.getGeneratedKeys();
	    int idd = 0;
	    if(rs.next())
	    {
	    	idd = rs.getInt(1);	    	
	    }
	    
	    String sqlBloquearItems = 
			    "SELECT I.ID_BODEGA, I.ID"
			    +"    FROM ITEMS I"
			    +"    WHERE I.ID_BODEGA = '" + idd + "' FOR UPDATE";
	    prepStmt = conexion.prepareStatement(sqlBloquearItems);
	    rs = prepStmt.executeQuery();

	    // Sentencia SQL para obtener todos los productos que están en una
	    // bodega
	    String sqlA = "select i.id, i.ID_BODEGA, p.id as idPres, p.peso, b.ID_TIPO, b.capacidad from "
		    + "items i  inner join PRESENTACIONES p " + "on i.id = p.id_item and i.ID_BODEGA is not null "
		    + "inner join bodegas b " + "on b.ID = i.ID_BODEGA and b.ID_TIPO = " + type2 + " order by ID_BODEGA, i.id";
	    prepStmt = conexion.prepareStatement(sqlA);
	    rs = prepStmt.executeQuery();

	    // Manejo de la Query
	    int idBod = 0;
	    int counter = 0;
	    ArrayList<Integer> listaIds = new ArrayList<Integer>();
	    String sqlBod = new String();
	    while (rs.next())
	    {
	    	int as = rs.getInt(1);
	    	listaIds.add(as);
	    	// Si el numero de bodega es igual a idBod, se suman los pesos
	    	if (rs.getInt(2) == idBod)
	    	{
	    		counter = counter + rs.getInt(4);
	    		sqlBod = "SELECT CAPACIDAD FROM BODEGAS WHERE ID = " + idBod;
	    	}
	    		

	    	// Si no es igual, se toma la suma de pesos aterior (counter)
	    	// Y se divide entre la cap. máx de la bodega.
	    	// Si la bodega está a más del 80% de su cap., se agregan items
	    	// a la bodega nueva.
	    	else
	    	{
	    		int w = listaIds.indexOf(as);
	    		listaIds.remove(w);
	    		// Obtiene la capacidad de la bodega con id idBod
	    		if (idBod != 0)
	    		{
	    			prepStmt = conexion.prepareStatement(sqlBod);
	    			ResultSet xs = prepStmt.executeQuery();
	    			xs.next();
	    			double capacidad = xs.getInt(1);
	    			
	    			int i = 0;
	    			while (counter / capacidad * 100 > 80)
	    			{
	    				si = true; 
	    				idsBodegas.add(idBod);
	    				
	    				// sacarlos de la bodega a la recién creada
	    				int idItem = listaIds.get(i);
	    				String sql4 = "select peso, pr.NOMBRE from presentaciones p INNER join items i on p.ID_ITEM = i.ID "
	    						+ "inner join productos pr on i.ID_PRODUCTO = pr.id where p.ID_ITEM = " + idItem;
	    				prepStmt = conexion.prepareStatement(sql4);
	    				ResultSet d = prepStmt.executeQuery();
	    				int peso = 0;
	    				while(d.next())
	    	    		{
	    					productosPasados.add(d.getString(2));
	    	    			peso = d.getInt(1);
	    	    			String sql3 = "update items set id_bodega = " + idd + " where id = " + idItem;
	    	    			prepStmt = conexion.prepareStatement(sql3);
	    	    			prepStmt.executeUpdate();
	    	    			counter = counter - peso;
	    	    		}
	    	    		i ++;	
	    	    	}
	    		}

	    		idBod = rs.getInt(2);
	    		counter = rs.getInt(4);
	    		listaIds = new ArrayList<Integer>();
	    		listaIds.add(rs.getInt(1));
	    	}
	    	
	    }
	    	
	    	prepStmt = conexion.prepareStatement(sqlBod);
	    	ResultSet xs = prepStmt.executeQuery();
	    	xs.next();
	    	double capacidad = xs.getInt(1);
	    	
	    	int i = 0;
	    	while (counter / capacidad * 100 > 80)
	    	{
	    		si = true;
	    		// sacarlos de la bodega a la recién creada
	    		int idItem = listaIds.get(i);
	    		String sql4 = "select peso from presentaciones where id_item = " + idItem;
	    		prepStmt = conexion.prepareStatement(sql4);
	    		ResultSet d = prepStmt.executeQuery();
	    		int peso = 0;
	    		while(d.next())
	    		{
	    			peso = d.getInt(1);
	    			String sql3 = "update items set id_bodega = " + idd + " where id = " + idItem;
	    			prepStmt = conexion.prepareStatement(sql3);
	    			prepStmt.executeUpdate();
	    			counter = counter - peso;
	    		}
	    		i ++;	
	    	}
	    

	    // Se lanza una query que arroje el total de bodegas de ese tipo
	    // junto con el total de cada productos que está ahí
	    // En java se cuenta el porcentaje de capacidad total usada de la
	    // bodega; si supera el 80%, el restante se agrega
	    // en la nueva bodega.
	    x = "La bodega ha sido agregada exitosamente";
	    conexion.commit();
	    Cabandes.getInstance().result = x;
	    if(si)
	    	enviarCorreo(idsBodegas, productosPasados, idd);
	    return x;
	}

	catch (Exception e)
	{
		try {
			conexion.rollback();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		Cabandes.getInstance().result = x;
		x = "Se ha producido un error";
		e.printStackTrace();
	    return x;
	}
	finally
	{
	    if (prepStmt != null)
		try
		{
		    prepStmt.close();
		}
		catch (SQLException exception)
		{
		}
	    try
	    {
		closeConnection(conexion);
	    }
	    catch (Exception e)
	    {
		e.printStackTrace();
	    }
	}

    }

    public void enviarCorreo(ArrayList<Integer> idsBodegas, ArrayList<String>productosPasados, int bodegaNueva)
    {
    	PreparedStatement prepStmt = null;
    	try	
    	{
    		int idBodega = 0;
    		establecerConexion();
    		
    		for(int i = 0; i < idsBodegas.size(); i ++)
    		{
    			idBodega = idsBodegas.get(i);
    		}
    		
    		 // ADMINISTRADORES CON PRODUCTOS DE PEDIDOS QUE PODRIAN SALIR LA BODEGA QUE SE VA A
    	    // CERRAR
    	    String sqlPedidosAdmin = "SELECT U.CORREO AS CORREO_ADMIN "
    		    + " FROM ITEMS I "
    		    + "  	INNER JOIN PEDIDOS_ADMIN_LOC PED ON PED.NUMERO_PEDIDO = I.NUM_PEDIDO_ADMIN_LOCAL "
    		    + "  	LEFT OUTER JOIN PRESENTACIONES PR ON PR.ID_ITEM = I.ID "
    		    + "  	INNER JOIN USUARIOS U ON U.ID = PED.ID_ADMIN_LOCAL "
    		    + "     INNER JOIN PRODUCTOS P ON P.ID = I.ID_PRODUCTO "

    		    + "   	LEFT OUTER JOIN (SELECT P.NOMBRE AS NOMBRE_PRODUCTO_BODEGA, I.ID_BODEGA, I.ID AS ID_ITEM_BODEGA, PR.PESO AS PESO_PESEN_BODEGA "
    		    + "                    FROM ITEMS I " + "                          INNER JOIN BODEGAS B ON I.ID_BODEGA = B.ID "
    		    + "                          INNER JOIN PRESENTACIONES PR ON PR.ID_ITEM = I.ID "
    		    + "                          INNER JOIN PRODUCTOS P ON P.ID = I.ID_PRODUCTO "
    		    + "                    WHERE I.ID_BODEGA IS NOT NULL) PB "
    		    + "        ON P.NOMBRE = PB.NOMBRE_PRODUCTO_BODEGA AND PB.PESO_PESEN_BODEGA = PR.PESO " + " WHERE PB.ID_BODEGA = '"
    		    + idBodega + "'" + " GROUP BY U.CORREO";

    	    prepStmt = conexion.prepareStatement(sqlPedidosAdmin);
    	    ResultSet rs = prepStmt.executeQuery();

    	    String mensaje = "Estimado administrador, Cabandes le informa que debido a un sobrecupo en la bodega "+idBodega+", los productos";
    	    String sd = "";		
    	    for(int i = 0; i < productosPasados.size(); i++)
    	    		{
    	    			sd = sd + productosPasados.get(i) + ",";
    	    		}
    	    String mensaje2 = " serán reubicados en la bodega" + bodegaNueva;
    	    
    	    while (rs.next())
    	    {
    		// NOTIFICAR ADMINS LOCAL
    		
    		String correoAdmin = rs.getString("CORREO_ADMIN");
    		
    		String sqlEnviarMensaje = "INSERT INTO BUZON_DE_SALIDA (DESTINATARIO, MENSAJE) VALUES ("
    					+ correoAdmin +", " + mensaje + sd + mensaje2 +") ";
    		
    		prepStmt = conexion.prepareStatement(sqlEnviarMensaje);
    		prepStmt.executeUpdate();
    	    }

    	    // PEDIDOS CON SUS COMPRADORES POR MAYOR, ITEMS Y LAS BODEGAS (SI
    	    // HAY) QUE TIENEN ITEMS QUE PUEDEN SATISFACER EL PEDIDO
    	    String sqlPedidosPorMayor = "SELECT U.CORREO AS CORREO_COMPRADOR"
    		    + " FROM ITEMS I "
    		    + " 	   INNER JOIN PEDIDOS_POR_MAYOR PED ON PED.NUMERO_PEDIDO = I.NUM_PEDIDO_POR_MAYOR"
    		    + " 	   LEFT OUTER JOIN PRESENTACIONES PR ON PR.ID_ITEM = I.ID"
    		    + "     INNER JOIN USUARIOS U ON U.ID = PED.ID_COMPRADOR_POR_MAYOR"
    		    + "     INNER JOIN PRODUCTOS P ON P.ID = I.ID_PRODUCTO"

    		    + "   	LEFT OUTER JOIN (SELECT P.NOMBRE AS NOMBRE_PRODUCTO_BODEGA, I.ID_BODEGA, I.ID AS ID_ITEM_BODEGA, PR.PESO AS PESO_PESEN_BODEGA"
    		    + "                    FROM ITEMS I " + "                          INNER JOIN BODEGAS B ON I.ID_BODEGA = B.ID"
    		    + "                          INNER JOIN PRESENTACIONES PR ON PR.ID_ITEM = I.ID"
    		    + "                          INNER JOIN PRODUCTOS P ON P.ID = I.ID_PRODUCTO"
    		    + "                    WHERE I.ID_BODEGA IS NOT NULL) PB"
    		    + "        ON P.NOMBRE = PB.NOMBRE_PRODUCTO_BODEGA AND PB.PESO_PESEN_BODEGA = PR.PESO" + " WHERE PB.ID_BODEGA = '"
    		    + idBodega + "'" + " GROUP BY U.CORREO";

    	    prepStmt = conexion.prepareStatement(sqlPedidosAdmin);
    	    rs = prepStmt.executeQuery();
    	    
    	    String men = "Estimado administrador, Cabandes le informa que debido a un sobrecupo en la bodega "+idBodega+", los productos";
    	    String ss = "";		
    	    for(int i = 0; i < productosPasados.size(); i++)
    	    		{
    	    			ss = ss + productosPasados.get(i) + ",";
    	    		}
    	    String men2 = " serán reubicados en la bodega" + bodegaNueva;
    	    
    	    while (rs.next())
    	    {
    		// NOTIFICAR COMPRADORES
    		
    		String correoComprador = rs.getString("CORREO_COMPRADOR");
    		
    		String sqlEnviarMensaje = "INSERT INTO BUZON_DE_SALIDA (DESTINATARIO, MENSAJE) VALUES ("
    					+ correoComprador +", " + men + ss + men2 + ") ";
    		
    		prepStmt = conexion.prepareStatement(sqlEnviarMensaje);
    		prepStmt.executeUpdate();
    	    }
    	    
    	    conexion.commit();
	
    	}
    	catch (Exception e)
    	{
    		try {
    			conexion.rollback();
    		} catch (SQLException e1) {
    			// TODO Auto-generated catch block
    			e1.printStackTrace();
    		}
    		
    	}
    	finally
    	{
    		try
    		{
    		    prepStmt.close();
    		}
    		catch (SQLException exception)
    		{
    		}
    	    try
    	    {
    		closeConnection(conexion);
    	    }
    	    catch (Exception e)
    	    {
    		e.printStackTrace();
    	    }
    	}
    }
    
    
    public void verProveedores()
    {
	// Envía la sentencia para ver todos los proveedores
	PreparedStatement prepStmt = null;
	try
	{
	    establecerConexion();
	    String sql = "select p.id, t.NOMBRE, p.SUMA_CALIFICACIONES, p.NUM_VECES_CALIFICADO, nvl(ppr.NUMERO_PEDIDO, 0) as numero_pedido, "
		    + "nvl(ppr.ID_PROVEEDOR, 0) as id_proveedor, nvl(o.COSTO_TOTAL, 0) as costo_total, "
		    + "u.NOMBRE as nombreUsuario, u.CORREO, u.NACIONALIDAD "
		    + "from PROVEEDORES p inner join tipos t on p.ID_TIPO = t.ID "
		    + "left outer join pedidos_proveedor_proveedores ppr on p.ID = ppr.id_proveedor "
		    + "left outer join ofertas o on p.ID = o.ID_PROVEEDOR " + "left outer join USUARIOS u on p.ID = u.ID";

	    prepStmt = conexion.prepareStatement(sql);
	    ResultSet rs = prepStmt.executeQuery();

	    ArrayList<Proveedor> proov = new ArrayList<Proveedor>();
	    ArrayList<Usuario> user = new ArrayList<Usuario>();

	    while (rs.next())
	    {
		int calificacion = 0;
		String tipoProducto = rs.getString(2);
		if (rs.getInt(4) == 0)
		{
		    calificacion = rs.getInt(3) / 1;
		}
		else
		{
		    calificacion = rs.getInt(3) / rs.getInt(4);
		}
		ArrayList<Oferta> ofertas = new ArrayList<Oferta>();
		ArrayList<PedidoProveedor> pedidos = new ArrayList<PedidoProveedor>();
		ArrayList<Item> items = new ArrayList<Item>();

		Usuario xd = new Usuario();
		xd.nombre = rs.getString(8);
		xd.correo = rs.getString(9);
		xd.nacionalidad = rs.getString(10);

		user.add(xd);

		PedidoProveedor s = new PedidoProveedor();
		s.numeroPedido = rs.getInt(5);
		Oferta x = new Oferta();
		x.costoTotal = rs.getInt(7);
		ofertas.add(x);
		pedidos.add(s);

		Proveedor p = new Proveedor(tipoProducto, calificacion, ofertas, pedidos, items);
		proov.add(p);
	    }

	    Cabandes.getInstance().darProveedores = proov;
	    Cabandes.getInstance().darUsuarios = user;

	}

	catch (Exception e)
	{
	    e.printStackTrace();
	}
	finally
	{
	    if (prepStmt != null)
		try
		{
		    prepStmt.close();
		}
		catch (SQLException exception)
		{
		}
	    try
	    {
		closeConnection(conexion);
	    }
	    catch (Exception e)
	    {
		e.printStackTrace();
	    }
	}

    }

    public void consultarLocales()
    {
	PreparedStatement prepStmt = null;
	try
	{
	    establecerConexion();

	    String sql = "SELECT * FROM LOCALES ORDER BY ID";

	    prepStmt = conexion.prepareStatement(sql);
	    ResultSet rs = prepStmt.executeQuery();

	    ArrayList<LocalVO> locales = new ArrayList<LocalVO>();

	    while (rs.next())
	    {
		String id = rs.getString("ID");
		LocalVO localVO = new LocalVO();
		localVO.id = id;
		
		locales.add(localVO);
	    }

	    Cabandes.getInstance().localesUltimaConsulta = locales;
	}
	catch (Exception e)
	{
	    e.printStackTrace();
	}
	finally
	{
	    if (prepStmt != null)
		try
		{
		    prepStmt.close();
		}
		catch (SQLException exception)
		{

		}
	    try
	    {
		closeConnection(conexion);
	    }
	    catch (Exception e)
	    {
		e.printStackTrace();
	    }
	}
    }
    
    public void visualizarLocales(int id, String fechaIni, String fechaFini)
    {
    	PreparedStatement prepStmt = null;
    	try
    	{
    	    establecerConexion();

    	    String sql = "select p.NOMBRE, i.ID_LOCAL, i.FECHA_INGRESO, i.NUM_PEDIDO_ADMIN_LOCAL, "
    	    		+ "pa.ID_ADMIN_LOCAL, pa.FECHA_SOLICITADA, "
    	    		+ "i.NUM_PEDIDO_POR_MAYOR, pm.ID_COMPRADOR_POR_MAYOR, pm.FECHA_SOLICITADA, pm.FECHA_REALIZACION "
    	    		+ "from items i "
    	    		+ "left outer join PEDIDOS_ADMIN_LOC pa on i.NUM_PEDIDO_ADMIN_LOCAL = pa.NUMERO_PEDIDO "
    	    		+ "left outer join PEDIDOS_POR_MAYOR pm on i.NUM_PEDIDO_POR_MAYOR = pm.NUMERO_PEDIDO "
    	    		+ "inner join PRODUCTOS p on i.ID_PRODUCTO = p.id "
    	    		+ "where NUM_PEDIDO_ADMIN_LOCAL is not null or NUM_PEDIDO_POR_MAYOR is not null "
    	    		+ "and pa.FECHA_SOLICITADA between '"+fechaIni+"' and '"+fechaFini+"' "
    	    		+ "or pm.FECHA_SOLICITADA between '"+fechaIni+"' and '"+fechaFini+"' "
    	    		+ "order by i.id_local";
    	    		
    	    prepStmt = conexion.prepareStatement(sql);
    	    long t1 = System.currentTimeMillis();
    	    ResultSet rs = prepStmt.executeQuery();
    	    tiempoFinal = System.currentTimeMillis() - t1;
    	    System.out.println("Tiempo total ejectado: "+tiempoFinal);
    	    ArrayList<PedidoVO> pedidos = new ArrayList<PedidoVO>();
    	    boolean bool = false;
    	    while(rs.next() && !bool)
    	    {
    	    	int idLocal = rs.getInt(2);
    	    	if(idLocal > id)
    	    		bool = true;
    	    	
    	    	if(idLocal == id)
    	    	{
    	    		PedidoVO pedido = new PedidoVO();
    	    		Integer a = rs.getInt(4);
    	    		Integer b = rs.getInt(7);
    	    		//Integer c = rs.getInt(11);
    	    		
    	    		pedido.nombreProducto = rs.getString(1);
    	    		
    	    		if (a != 0)
    	    		{
    	    			pedido.numeroPedido = String.valueOf(rs.getInt(4));
    	    			pedido.fechaRealizacion = rs.getDate(3);
    	    			pedido.idComprador = rs.getInt(5);
    	    			pedido.fechaSolicitud = rs.getDate(6);
    	    			pedido.tipoComprador = "Administrador de Local";
    	    			pedidos.add(pedido);
    	    		}
    	    		
    	    		else if(b != 0)
    	    		{
    	    			pedido.numeroPedido = String.valueOf(rs.getInt(7));
    					pedido.idComprador = rs.getInt(8);
    					pedido.fechaSolicitud = rs.getDate(9);
    					pedido.fechaRealizacion = rs.getDate(10);    					
    					pedido.tipoComprador = "Cliente por Mayor";
    					pedidos.add(pedido);
    	    		}
    	    		
//    	    		else if(c != 0)
//    	    		{
//    	    			pedido.numeroPedido = String.valueOf(rs.getInt(11));
//    	    			pedido.fechaSolicitud = rs.getDate(12);
//    	    			pedido.fechaRealizacion = rs.getDate(13);   	    			
//    	    			pedido.tipoComprador = "Proveedor";
//    	    			pedidos.add(pedido);
//    	    		}
    	    		
    	    	}
    	    	
    	    }
    	    Cabandes.getInstance().pedidoLocal = pedidos;
    	    tamanho = pedidos.size();
    	    
    	}
    	catch (Exception e)
    	{
    	    e.printStackTrace();
    	}
    	finally
    	{
    	    if (prepStmt != null)
    		try
    		{
    		    prepStmt.close();
    		}
    		catch (SQLException exception)
    		{

    		}
    	    try
    	    {
    		closeConnection(conexion);
    	    }
    	    catch (Exception e)
    	    {
    		e.printStackTrace();
    	    }
    	}
    }
    
    public synchronized void consultarProductosMasDinamicos (String fechaInicio, String fechaFin)
    {
	PreparedStatement prepStmt = null;
	try
	{
	    establecerConexion();
	    String sql = "SELECT P.NOMBRE, COUNT(H.ID_PRODUCTO) AS NUM FROM HISTORIAL_MOVIMIENTOS H INNER JOIN PRODUCTOS P ON H.ID_PRODUCTO = P.ID"
                        +" WHERE H.FECHA BETWEEN '"+fechaInicio+"' AND '"+fechaFin+"'"
                        +" GROUP BY H.ID_PRODUCTO, P.NOMBRE"
                        +" ORDER BY COUNT(H.ID_PRODUCTO) DESC";

	    prepStmt = conexion.prepareStatement(sql);
	    ResultSet rs = prepStmt.executeQuery();

	    ArrayList<ProductoVO> productos = new ArrayList<ProductoVO>();

	    while (rs.next())
	    {
		ProductoVO productoVO = new ProductoVO();
		productoVO.nombre = rs.getString("NOMBRE");
		productoVO.numMovimientos = rs.getString("NUM");

		productos.add(productoVO);
	    }
	    
	    Cabandes.getInstance().setListenerRespuestas(this);
	    Cabandes.getInstance().enviarSolicitud(Mensajes.PRODUCTOS_MAS_DINAMICOS+":"+fechaInicio+":"+fechaFin);
	    
	    try
	    {
		wait(5000);
	    }
	    catch (InterruptedException e)
	    {
		e.printStackTrace();
	    }
	    
	    if (respuestaOtroComponente != null && respuestaOtroComponente.split(":")[0].equals(Mensajes.PRODUCTOS_MAS_DINAMICOS)) 
	    {
		String[] productosMasDinamicosExternos = respuestaOtroComponente.split(":");
		
		for (int i = 1; i < productosMasDinamicosExternos.length; i++)
		{
		    String producto = productosMasDinamicosExternos[i];
		    String[] componentes = producto.split("-");
		    
		    ProductoVO productoVO = new ProductoVO();
		    productoVO.nombre = componentes[0];
		    productoVO.numMovimientos = componentes[1];
		    
		    productos.add(productoVO);
		}
	    }
	    TreeSet<ProductoVO> set = new TreeSet<ProductoVO>(new Comparator<ProductoVO>()
	    {
		@Override
		public int compare(ProductoVO p1, ProductoVO p2)
		{
		    return -1*((Integer) Integer.parseInt(p1.numMovimientos)).compareTo((Integer) Integer.parseInt(p2.numMovimientos));
		}
	    });
	    set.addAll(productos);
	    productos = new ArrayList<ProductoVO>();
	    productos.addAll(set);
	    
	    Cabandes.getInstance().productosMasDinamicosUltimaConsulta = productos;
	    respuestaOtroComponente = null;
	}
	catch (Exception e)
	{
	    e.printStackTrace();
	}
	finally
	{
	    if (prepStmt != null)
		try
		{
		    prepStmt.close();
		}
		catch (SQLException exception)
		{

		}
	    try
	    {
		closeConnection(conexion);
	    }
	    catch (Exception e)
	    {
		e.printStackTrace();
	    }
	}
    }

    @Override
    public synchronized void respuestaRecibida(String respuesta)
    {
	this.respuestaOtroComponente = respuesta;
	notify();
    }
    
    /**
     * Obtiene todos los productos del tipo del localvpara mostrar en la interfaz al abrir la pestaña
     * @param idLocal Id del local
     */
    
    public void obtenerProductosDeTipo(int idLocal)
    {
    	PreparedStatement prepStmt = null;
    	try
    	{
    		establecerConexion();

    		String sql = "select p.NOMBRE from locales l "
    				+ "inner join PRODUCTOS p on l.ID_TIPO = p.ID_TIPO "
    				+ "where l.ID = "+idLocal;

    		prepStmt = conexion.prepareStatement(sql);
    		ResultSet rs = prepStmt.executeQuery();
    		ArrayList<String> todosProductos = new ArrayList<String>();
    		String nombre = "";
    		while(rs.next())
    		{
    			nombre = rs.getString(1);
    			todosProductos.add(nombre);
    		}
    		
    		Cabandes.getInstance().todosProductos = todosProductos;

    	}
    	catch (Exception e)
    	{
    		e.printStackTrace();
    	}
    	finally
    	{
    		if (prepStmt != null)
    			try
    		{
    				prepStmt.close();
    		}
    		catch (SQLException exception)
    		{

    		}
    		try
    		{
    			closeConnection(conexion);
    		}
    		catch (Exception e)
    		{
    			e.printStackTrace();
    		}

    	}
    }
    
    /**
     * Recibe lo que el admin del local necesita de su pedido. Divide lo que recibe y lo envía a otro método.
     * @param pedido String de pedido
     * @param idLocal Id del local
     * @throws JMSException 
     */
    public synchronized String revisarCapacidad(String [] pedido, int idLocal, boolean afuera) throws JMSException
    {    	
    	listaPresentaciones = new ArrayList<String>();
    	//Recibe todos productos y cantidades
    	String item = "";
    	int cantidad = 0;
    	String x = "";
    	for(int i = 0; i < pedido.length; i ++)
		{	
			if(pedido[i] != "")
			{
				item = pedido[i];
				String qty = pedido[i+1];
				if(!qty.equals(""))
					cantidad = Integer.parseInt(qty);

				System.out.println(item);
				System.out.println(cantidad);
				System.out.println(idLocal);
				String y = tratarSatisfacerPedido(item, cantidad, idLocal); 
				x = x + y;
			}
		}
    	//Si se cumple, quiere decir que se cumplio el pedido en casa
    	String mensaje = "Se completó el pedido localemente. El número de confirmación es: ";
    	String resultado = "";
    	if(x.equals(""))
    		resultado = movimientosLocal(idLocal, mensaje);
    	
    	else if(!x.equals(""))
    		resultado = pedirAfuera(x, idLocal, afuera);
    	
    	    	
    	Cabandes.getInstance().afan = x;
    	Cabandes.getInstance().resultadoMierdero = resultado;
    	return resultado;
    }
    		
    /**
     * Realiza todos los movimientos de Pedidos de Admin. Llama al método que realiza 
     * movimientos de presentaciones e items
     * @param idLocal
     */
    public String movimientosLocal(int idLocal, String mensaje)
    {
    	//Crear pedido
    	String idPedido = "";
    	PreparedStatement prepStmt = null;
    	try
    	{
    		establecerConexion();
    		//Obtener id del admin del local
    		String sql = "select a.id from locales l inner join ADMINS_LOCAL a "
    				+ "on l.ID = a.ID_LOCAL "
    				+ "where l.id = "+idLocal;

    		prepStmt = conexion.prepareStatement(sql);
    		ResultSet rs = prepStmt.executeQuery();
    		rs.next();
    		int idAdmin = rs.getInt(1);

    		//Inserta el pedido en la tabla pedidos
    		sql = "insert into PEDIDOS_ADMIN_LOC "
    				+ "(FECHA_ESPERADA,ID_ADMIN_LOCAL,FECHA_REALIZACION,ENTREGADO,FECHA_SOLICITADA) "
    				+ "values ('31/07/14',"+idAdmin+",'07/07/14','Registrado','20/05/14')";

    		prepStmt = conexion.prepareStatement(sql, new String []{"NUMERO_PEDIDO"});
    		prepStmt.executeUpdate();
    		rs = prepStmt.getGeneratedKeys();
    		rs.next();
    		idPedido = rs.getString(1);

    		movimientosPedido(idPedido);

    	}
    	catch (Exception e)
    	{
    		e.printStackTrace();
    	}
    	finally
    	{
    		if (prepStmt != null)
    			try
    		{
    				prepStmt.close();
    		}
    		catch (SQLException exception)
    		{

    		}
    		try
    		{
    			closeConnection(conexion);
    		}
    		catch (Exception e)
    		{
    			e.printStackTrace();
    		}
    	}
		return mensaje+idPedido;
    }
    	
    /**
     * Realiza todos los movimientos de presentaciones de items
     * @param idPedido
     */
    public void movimientosPedido(String idPedido)
    {
    	String sql = "";
    	PreparedStatement prepStmt = null;
    	try{
    		establecerConexion();
    		for(String elId: listaPresentaciones)
    		{
    			sql = "update items set NUM_PEDIDO_ADMIN_LOCAL = "+idPedido
    					+ " where id = (select p.ID_ITEM from PRESENTACIONES p where p.ID = "+elId+")";
    			prepStmt = conexion.prepareStatement(sql);
    			prepStmt.executeUpdate();
    		}

    	}
    	catch (Exception e)
    	{
    		e.printStackTrace();
    	}
    	finally
    	{
    		if (prepStmt != null)
    			try
    		{
    				prepStmt.close();
    		}
    		catch (SQLException exception)
    		{

    		}
    		try
    		{
    			closeConnection(conexion);
    		}
    		catch (Exception e)
    		{
    			e.printStackTrace();
    		}
    	}
    }

    /**
     * Empieza a buscar localmente y satisfacer la cantidad DE CADA PRODUCTOS. ES LLAMADO UNA VEZ POR PRODUCTO.
     * @param item
     * @param cantidad
     * @param idLocal
     * @return Retorna vacío si se logró localmente, retorna el item y la cantidad faltante si no.
     */
    public String tratarSatisfacerPedido(String item, int cantidad, int idLocal)
    {    	
    	String x = "";
    	PreparedStatement prepStmt = null;
    	try
    	{
    		establecerConexion();
    		
   			//Se realiza el movimiento
   			//Selecciono las cantidades presentes en cada bodega para cambiarlas
    			
    		String sql = "SELECT PR.ID, P.NOMBRE AS NOMBRE, PR.NUMERO_UNIDADES, I.ID_BODEGA "
    				+ "FROM ITEMS I INNER JOIN PRODUCTOS P ON I.ID_PRODUCTO = P.ID "
    				+ "LEFT OUTER JOIN PRESENTACIONES PR ON I.ID = PR.ID_ITEM "
    				+ "LEFT OUTER JOIN BODEGAS B ON I.ID_BODEGA = B.ID "
    				+ "WHERE  P.NOMBRE = '"+item+"' and I.ID_BODEGA is not null "
    				+ "and PR.NUMERO_UNIDADES is not null";
    		
    		prepStmt = conexion.prepareStatement(sql);
    		ResultSet rs = prepStmt.executeQuery();
    		int restante = cantidad;
    		//Se realiza el movimiento
    		while(rs.next() && restante > 0)
    		{
    			int idPresentacion = rs.getInt(1);
    			int numeroUnits = rs.getInt(3);
    			int idBod = rs.getInt(4);
    			
    			if(numeroUnits > cantidad)
    			{
    				//Se actualiza inventario de bodega y se agrega a otra presentación.
    				int nuevaCap = numeroUnits-cantidad;
    				sql = "update presentaciones p set numero_unidades = "+nuevaCap+" where p.id = "+idPresentacion;

    				prepStmt = conexion.prepareStatement(sql);
    	    		prepStmt.executeUpdate();
    				
    	    		//Se busca una presentación dentro del local del admin para agregar los items movidos 
    				String sql2 = "select p.id, p.NUMERO_UNIDADES from items i "
    						+ "inner join presentaciones p on i.id = p.ID_ITEM "
    						+ "inner join productos pr on i.ID_PRODUCTO = pr.ID "
    						+ "where i.ID_LOCAL = "+idLocal+" and pr.NOMBRE = '"+item+"'";
    				
    				prepStmt = conexion.prepareStatement(sql2);
    	    		ResultSet rs2 = prepStmt.executeQuery();
    				rs2.next();
    				
    				String idPres = rs2.getString(1);
    				int numUnidades = rs2.getInt(2);	
    				int nuevoTotal = numUnidades + cantidad;
    				
    				//Se actualiza la presentación con la nueva cantidad de productos
    				sql = "update presentaciones p set p.NUMERO_UNIDADES = "+nuevoTotal+" where p.id = "+idPres;
    				prepStmt = conexion.prepareStatement(sql);
    		    	prepStmt.executeUpdate();
    		    	listaPresentaciones.add(idPres);
    				
    				restante = 0;
    			}
    			else
    			{
    				//Caso que la cantidad de items de la primera bodega no es suficiente
    				
    				String sql2 = "select p.id, p.NUMERO_UNIDADES from items i "
    						+ "inner join presentaciones p on i.id = p.ID_ITEM "
    						+ "inner join productos pr on i.ID_PRODUCTO = pr.ID "
    						+ "where i.ID_LOCAL = "+idLocal+" and pr.NOMBRE = '"+item+"'";
    				
    				prepStmt = conexion.prepareStatement(sql2);
    	    		ResultSet rs3 = prepStmt.executeQuery();
    				rs3.next();
    				
    				int idPres = rs3.getInt(1);
    				int numUnidades = rs3.getInt(2);	
    				int nuevoTotal = numUnidades + cantidad;
    				
    				sql = "update presentaciones p set p.NUMERO_UNIDADES = "+nuevoTotal+" where p.id = "+idPres;
					
    				prepStmt = conexion.prepareStatement(sql);
    		    	prepStmt.executeUpdate();
    				
    				sql = "delete from presentaciones p where p.id = "+idPresentacion;
    				prepStmt = conexion.prepareStatement(sql);
    				prepStmt.executeUpdate();    		    	
    				
    				restante = restante - numeroUnits; 
    			}
    		}
    		//No se cumplió con lo de casa
    		if(restante > 0)
    		{    			
    			x = item + "-" + restante+":";    			
    		}
    	}
    		catch (Exception e)
        	{
        		e.printStackTrace();
        	}
        	finally
        	{
        		if (prepStmt != null)
        		try
        		{
        			prepStmt.close();
        		}
        		catch (SQLException exception)
        		{

        		}
        		try
        		{
        			closeConnection(conexion);
        		}
        		catch (Exception e)
        		{
        			e.printStackTrace();
        		}

        	}
    	return x;
    }

    /**
     * Realiza todos los movimientos correspondientes cuando sale de Cabandes
     * @param lista
     * @param idLocal
     * @return
     * @throws JMSException
     */
    public synchronized String pedirAfuera(String lista, int idLocal, boolean afuera) throws JMSException
    {
    	//El pedido no pudo ser completado por la casa. ¡Hay que recurrir a otro Cabandes!
    	String resultado = "";
    	
 		Cabandes.getInstance().setListenerRespuestas(this);

 		Cabandes.getInstance().enviarSolicitud(Mensajes.SATISFACER_PEDIDO+":"+lista);

    	try
    	{
    		wait(5000);
    	}
    	catch (InterruptedException e)
    	{
    		e.printStackTrace();
    	}
    	
    	if (respuestaOtroComponente != null && respuestaOtroComponente.split(":")[0].equals(Mensajes.SATISFACER_PEDIDO)) 
    	{
    		String[] respuesta = respuestaOtroComponente.split(":");
    		if(respuesta[1] == null)
    		{
    			//El otro Cabandes logró satisfacer el pedido. Se registra pedido
    			String rta = "El pedido se logró completar mediante el apoyo de otro Cabandes en la ciudad."
    					+ "El número de confirmación es: ";
    			movimientosLocal(idLocal, rta);
    			
    		}
    		else if(!afuera)
    		{
    			for (int i = 1; i < respuesta.length; i++)
    			{
    				String producto = respuesta[i];
    				String[] componentes = producto.split("-");
					String nombreProducto = componentes[0];
					String cantidadesPendientes = componentes[1];
					
							
  					//Se guarda el registro de lo que faltó y se pedirá a proveedores
					
					PreparedStatement prepStmt = null;
		    		try
		    		{
		    			establecerConexion();

		    			//Inserta el pedido en la tabla pedidos
		    			String sql = "insert into PEDIDOS_PROVEEDOR (FECHA_ESPERADA, EFECTIVO,PRESENTACION_DESEADA,FECHA_SOLICITADO)"
		    					+ "values ('31/07/14','Pendiente por confirmar',1,'20/05/14')";

		    		    prepStmt = conexion.prepareStatement(sql, new String []{"NUMERO_PEDIDO"});
		    			prepStmt.executeUpdate();
		    			ResultSet rs = prepStmt.getGeneratedKeys();
		    			rs.next();
		    			
		    			String idPedido = rs.getString(1);
		    			
		    			for(String elId: listaPresentaciones)
		    			{
		    				sql = "update items set NUM_PEDIDO_ADMIN_LOCAL = "+idPedido
		    						+ " where id = (select p.ID_ITEM from PRESENTACIONES p where p.ID = "+elId+")";
		    				prepStmt = conexion.prepareStatement(sql);
		    				prepStmt.executeUpdate();
		    			}
		    			resultado = "El pedido no se logró satisfacer a través de toda la red Cabandes. "
		    					+ "Se ha solcitado un pedido a los proveedores y se le mantendrá en contacto. "
		    					+ "El número de confirmación de Cabandes es "+idPedido;

		    		}
		    		catch (Exception e)
		        	{
		        		e.printStackTrace();
		        	}
		        	finally
		        	{
		        		if (prepStmt != null)
		        		try
		        		{
		        			prepStmt.close();
		        		}
		        		catch (SQLException exception)
		        		{

		        		}
		        		try
		        		{
		        			closeConnection(conexion);
		        		}
		        		catch (Exception e)
		        		{
		        			e.printStackTrace();
		        		}
		        	}
		    	}
					
    			}
    		}
			return respuestaOtroComponente = null;
			
			//¡Cuando el otro Cabandes recurre a nosotros hay que enviar respuesta y habilitar escucha!
    }
    		    
}
