package dao;

public interface ListenerRespuestas
{
    public void respuestaRecibida (String respuesta);
}
