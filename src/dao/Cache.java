/**
 * 
 */
package dao;

import java.util.ArrayList;

import modelo.IItem;


/**
 * @author Diego
 * 
 */
public class Cache
{
    private static Cache instancia;

    public static Cache instance()
    {
	if (instancia == null)
	    instancia = new Cache();
	return instancia;
    }

    public String idSeleccionado;
    public String opcion;
    public ArrayList<IItem> itemsSeleccionados;
    public ArrayList<IItem> itemsVencidos;
    public String idLocalSeleccionado;

    public void limpiarInfoMovimientos()
    {
	idSeleccionado = null;
	opcion = null;
	itemsSeleccionados = null;
	itemsVencidos = null;
	idLocalSeleccionado = null;
    }

}
