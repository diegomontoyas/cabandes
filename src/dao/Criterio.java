/**
 * 
 */
package dao;

/**
 * @author Diego
 * 
 */
public class Criterio
{
    /**
     * Descripción del criterio
     */
    private String descripción;

    /**
     * Atributo al que hace referencia en la base de datos
     */
    private String atributoBD;

    public Criterio(String descripcion, String atributoBD)
    {
	this.descripción = descripcion;
	this.atributoBD = atributoBD;
    }

    public String darAtributoBD()
    {
	return atributoBD;
    }

    public String darDescripcion()
    {
	return descripción;
    }

    @Override
    public String toString()
    {
	return descripción;
    }
}
