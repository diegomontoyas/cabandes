package dao;

public class Mensajes
{
    public final static String CONSULTAR_BODEGAS = "CONSULTAR_BODEGAS";

    public final static String SATISFACER_PEDIDO = "SATISFACER_PEDIDO";

    public final static String CERRAR_BODEGA = "CERRAR_BODEGA";
    
    public final static String PRODUCTOS_MAS_DINAMICOS = "PRODUCTOS_MAS_DINAMICOS";

    public static final String CONSULTAR_MOVIMIENTOS = "CONSULTAR_MOVIMIENTOS";
   
}
