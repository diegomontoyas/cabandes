package dao;

import java.lang.reflect.Field;
import java.util.ArrayList;

public class CriteriosProducto
{
    public static final Criterio NOMBRE_PRODUCTO = new Criterio("Nombre de producto", "P.NOMBRE");

    public static final Criterio TIPO_PRODUCTO = new Criterio("Tipo de producto", "T.NOMBRE");

    public static final Criterio PRESENTACION = new Criterio("Presentación", "T.NOMBRE");

    public static final Criterio FECHA_DE_EXPIRACION = new Criterio("Fecha de expiración", "Calcular");

    public static final Criterio IDENTIFICADOR_DE_BODEGA = new Criterio("Identificador de bodega", "B.ID");

    public static final Criterio IDENTIFICADOR_DE_LOCAL_DE_VENTAS = new Criterio("Identificador de local de ventas", "L.ID");

    public static ArrayList<Criterio> darCriterios()
    {
	Field[] fields = CriteriosProducto.class.getDeclaredFields();
	ArrayList<Criterio> criterios = new ArrayList<Criterio>();
	CriteriosProducto instancia = new CriteriosProducto();

	for (Field field : fields)
	    try
	    {
		criterios.add((Criterio) field.get(instancia));
	    }
	    catch (IllegalArgumentException e)
	    {
		e.printStackTrace();
	    }
	    catch (IllegalAccessException e)
	    {
		e.printStackTrace();
	    }
	return criterios;
    }

    public static Criterio darCriterioConDescripcion(String descripcion)
    {
	ArrayList<Criterio> criterios = darCriterios();

	for (Criterio criterio : criterios)
	    if (criterio.toString().equals(descripcion))
		return criterio;

	return null;
    }
}
