package dao;

import java.lang.reflect.Field;
import java.util.ArrayList;

public class CriteriosPedidoAdminLocal
{
    public static final Criterio SATISFECHO = new Criterio("Satisfechos", "PEDIDOS_ADMIN_LOC.SATISFECHO");

    public static final Criterio RANGO_FECHAS = new Criterio("Rango de fechas", "PEDIDOS_ADMIN_LOC.FECHA_REALIZACION");

    public static final Criterio COSTO = new Criterio("Costo", "Calcular");

    public static final Criterio PRODUCTO = new Criterio("Producto", ""); // PENDIENTE!!

    public static final Criterio PRESENTACION = new Criterio("Presentación", ""); // PENDIENTE!!

    public static ArrayList<Criterio> darCriterios()
    {
	Field[] fields = CriteriosPedidoAdminLocal.class.getDeclaredFields();
	ArrayList<Criterio> criterios = new ArrayList<Criterio>();
	CriteriosPedidoAdminLocal instancia = new CriteriosPedidoAdminLocal();

	for (Field field : fields)
	    try
	    {
		criterios.add((Criterio) field.get(instancia));
	    }
	    catch (IllegalArgumentException e)
	    {
		e.printStackTrace();
	    }
	    catch (IllegalAccessException e)
	    {
		e.printStackTrace();
	    }
	return criterios;
    }

    public static Criterio darCriterioConDescripcion(String descripcion)
    {
	ArrayList<Criterio> criterios = darCriterios();

	for (Criterio criterio : criterios)
	    if (criterio.toString().equals(descripcion))
		return criterio;

	return null;
    }
}
