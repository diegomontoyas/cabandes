package vos;

import modelo.IItem;

public class ItemVO implements IItem
{
    public String ID;

    public String nombre;

    public String tipo;

    public int unidades;

    public String localBodega;

    public Integer idLocal;

    public Integer idBodega;

    public Integer pesoPresentacion;

    public double pesoTotal;

    public int idProducto;

    public String fechaIngreso;

    public ItemVO()
    {
    }

    @Override
    public String darID()
    {
	return ID;
    }

    @Override
    public String darNombre()
    {
	return nombre;
    }

    @Override
    public String darTipo()
    {
	return tipo;
    }

    @Override
    public int darCantidadUnidades()
    {
	return unidades;
    }

    @Override
    public String darFechaVencimiento()
    {
	return "";
    }

    @Override
    public String esDeBodegaOLocal()
    {
	return localBodega;
    }

    @Override
    public Integer darPeso()
    {
	return pesoPresentacion;
    }

    @Override
    public Integer darIDBodegaLocal()
    {
	return idBodega != null ? idBodega : idLocal != null ? idLocal : null;
    }
}
