package vos;

import java.util.Date;

import modelo.IPedidoAdministradorLocal;

public class PedidoVO implements IPedidoAdministradorLocal
{
    public String numeroPedido;
    public Date fechaRealizacion;
    public String nombreProducto;
    public Double costoTotal;
    public Date fechaEsperada;
    public boolean satisfecho;

    public int idComprador;
    public String tipoComprador;
    public Date fechaSolicitud;    
    
    public PedidoVO()
    {
    }

    @Override
    public String darNumeroPedido()
    {
	return numeroPedido;
    }

    @Override
    public String darFechaEsperada()
    {
	return fechaRealizacion != null ? fechaRealizacion.toString() : null;
    }

    @Override
    public String darFechaRealizacion()
    {
	return fechaRealizacion != null ? fechaRealizacion.toString() : null;
    }

    @Override
    public String darNombreAdministrador()
    {
	return null;
    }

    @Override
    public boolean fueSatisfecho()
    {
	return satisfecho;
    }

    @Override
    public String darNombreProducto()
    {
	return nombreProducto;
    }

    @Override
    public String darPesoPresentacion()
    {
	return null;
    }

	@Override
	public int darIdComprador() 
	{
		return idComprador;
	}

	@Override
	public String darTipoComprador() 
	{
		return tipoComprador;
	}

	@Override
	public Date darFechaSolicitud() 
	{
		return fechaSolicitud;
	}

}
