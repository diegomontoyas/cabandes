package vos;

public class BodegaVO
{

    public int id;
    public double capacidadEnUso;
    public double capacidadTotal;
    public double capacidadRestante;
    public double porcentajeUtilizado;
    public int tipo;
    public boolean propia = true;

    public BodegaVO(int iId, double iCapacidadEnUso, double iCapacidadTotal, double iCapacidadRestante)
    {
	id = iId;
	capacidadEnUso = iCapacidadEnUso;
	capacidadTotal = iCapacidadTotal;
	capacidadRestante = iCapacidadRestante;
    }
    
    public BodegaVO()
    {
    }
    
    @Override
    public String toString()
    {
        return id +"";
    }
}
