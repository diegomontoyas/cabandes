/**
 * 
 */
package test;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import junit.framework.TestCase;
import modelo.Cabandes;
import vos.BodegaVO;
import dao.ConsultaDAO;

/**
 * @author Diego
 * 
 */
public class TestCabandes extends TestCase
{
    ConsultaDAO consultaDAO = new ConsultaDAO(".");

    public void testConexion()
    {
	try
	{
	    consultaDAO.establecerConexion();
	}
	catch (SQLException e)
	{
	    e.printStackTrace();
	}
    } 

    // -----------------------------------------------
    // Pruebas


    //////////////////////// PRUEBAS ITERACION 3 ////////////////////////////
    
    public void testCommit() throws Exception
    {
	consultaDAO.establecerConexion();
	consultaDAO.conexion.commit();
	consultaDAO.closeConnection(consultaDAO.conexion);
    }
    
    public void testCerrarBodega()
    {
	ArrayList<PreparedStatement> prepStmts = new ArrayList<PreparedStatement>();
	
	ResultSet rs = null;
	try
	{
	    PreparedStatement prepStmt = null;
	    
	    consultaDAO.establecerConexion();
	    consultaDAO.conexion.setAutoCommit(false);
	    
            String sql= "INSERT INTO BODEGAS (CAPACIDAD, ID_TIPO) VALUES ('4444', '1')";
            
            prepStmt = consultaDAO.conexion.prepareStatement(sql, new String[]{"ID"});
            prepStmts.add(prepStmt);
	    int i = prepStmt.executeUpdate();
	    String idBodegaAgregada = null;
	    
	    if (i>0)
	    {
		rs = prepStmt.getGeneratedKeys();   
		while(rs.next()) { idBodegaAgregada = rs.getString(1); }
		System.out.println(idBodegaAgregada);
	    }
            
	    ArrayList<String> idsProductosAgregados = new ArrayList<String>();

            sql = "INSERT INTO ITEMS (ID_PRODUCTO, ID_BODEGA) VALUES ('1', '"+idBodegaAgregada+"')";
            prepStmt = consultaDAO.conexion.prepareStatement(sql, new String[]{"ID"});
            prepStmts.add(prepStmt);
	    i = prepStmt.executeUpdate();  
	    
	    if (i>0)
	    {
		rs = prepStmt.getGeneratedKeys();   
		while(rs.next()) 
		{
		    idsProductosAgregados.add(rs.getString(1)) ; 
		    int idItem = Integer.parseInt(rs.getString(1));
		    
		    sql = "INSERT INTO PRESENTACIONES (NUMERO_UNIDADES, COSTO, PESO, ID_ITEM) "
		    	+ "VALUES ('10', '1200', '10', '"+idItem+"')";
	            prepStmt = consultaDAO.conexion.prepareStatement(sql);
	            prepStmts.add(prepStmt);
	            prepStmt.executeUpdate();
		}
	    }
            
            sql = " INSERT INTO ITEMS (ID_PRODUCTO, ID_BODEGA) VALUES ('2', '"+idBodegaAgregada+"')";
            prepStmt = consultaDAO.conexion.prepareStatement(sql, new String[]{"ID"});
            prepStmts.add(prepStmt);
	    i = prepStmt.executeUpdate();  
	    
	    if (i>0)
	    {
		rs = prepStmt.getGeneratedKeys();   
		while(rs.next()) { idsProductosAgregados.add(rs.getString(1)) ; }
	    }
	    
	    consultaDAO.conexion.commit();
	    
	    //Se comprueba que exista la bodega y que su capacidad en uso no sea cero
	    
	    consultaDAO.consultarBodegas(null, null);
	    boolean hayBodega = false;
	    
	    for (BodegaVO bodegaVO : Cabandes.getInstance().darBodegasUltimaConsulta())
	    {
		if (bodegaVO.id == Integer.parseInt(idBodegaAgregada))
		{
		    hayBodega = true;
		    if (bodegaVO.capacidadEnUso == 0)
		    {
			fail();
			break;
		    }
		}
	    }
	    
	    consultaDAO.closeConnection(consultaDAO.conexion);
	    
	    //Se cierra la bodega
	    
	    consultaDAO.cerrarBodega(idBodegaAgregada, true, true);
	    
	    //Se verifica que ahora la bodega tenga su capacidad en uso en 0
	    
	    consultaDAO.consultarBodegas(null, null);
	    
	    for (BodegaVO bodegaVO : Cabandes.getInstance().darBodegasUltimaConsulta())
	    {
		if (bodegaVO.id == Integer.parseInt(idBodegaAgregada))
		{
		    if (bodegaVO.capacidadEnUso != 0)
		    {
			fail();
			break;
		    }
		}
	    }
	    
	    //Se verifica que no haya ahora ningun item con el id de la bodega que se cerro
	    
	    consultaDAO.establecerConexion();
	    sql = " SELECT * FROM ITEMS I WHERE I.ID = '"+idBodegaAgregada+"'";
	    prepStmt = consultaDAO.conexion.prepareStatement(sql);
	    prepStmts.add(prepStmt);
	    rs = prepStmt.executeQuery();
	    if (rs.isBeforeFirst() ) 
	    {   
		try
		{
		    rs.getString("ID");
		    fail();
		}
		catch (Exception e)
		{
		    // TODO: handle exception
		}	
	    }    
	    
	    consultaDAO.establecerConexion();
	    sql = "DELETE FROM BODEGAS B WHERE B.ID = '"+idBodegaAgregada+"'";
	    prepStmt = consultaDAO.conexion.prepareStatement(sql);
	    prepStmts.add(prepStmt);
	    prepStmt.executeUpdate();
	}
	catch (SQLException e)
	{
	    try
	    {
		consultaDAO.conexion.rollback();
	    }
	    catch (SQLException e1)
	    {
		e1.printStackTrace();
	    }
	    e.printStackTrace();
	    fail();
	}
	catch (Exception e1)
	{
	    e1.printStackTrace();
	}
	finally
	{
	    for (PreparedStatement prepStmt : prepStmts)
	    {
		if (prepStmt != null)
		try
		{
		    prepStmt.close();
		}
		catch (SQLException exception)
		{

		}
	    }
	   
	    try
	    {
		consultaDAO.closeConnection(consultaDAO.conexion);
	    }
	    catch (Exception e)
	    {
		e.printStackTrace();
	    }
	}
    }
    
    
    public void testRegistrarBodega()
    {
    	PreparedStatement prepStmt = null;
    	try
    	{
    	    consultaDAO.establecerConexion();
    	    consultaDAO.conexion.setAutoCommit(false);	    
    	    
    	    //Agrega bodega vacía a llenar
    	    String sql = "insert into bodegas (capacidad, id_tipo) values ("+100+","+1+")";
    	    prepStmt = consultaDAO.conexion.prepareStatement(sql, new String[]{"ID"});
    	    prepStmt.executeUpdate();
    	    ResultSet rs = prepStmt.getGeneratedKeys();
    	    rs.next();
    	    int keyBodega = rs.getInt(1); 
    	    
    	    //Agrega item a la bodega
    	    sql = "insert into ITEMS (ID_PRODUCTO,ID_LOCAL,ID_BODEGA,FECHA_INGRESO,NUM_PEDIDO_ADMIN_LOCAL,NUM_PEDIDO_POR_MAYOR,NUM_PEDIDO_PROVEEDOR)"
    	    		+" values (1,null,"+keyBodega+",'20/05/2014',null,null,null)";
    	    prepStmt = consultaDAO.conexion.prepareStatement(sql, new String[]{"ID"});
    	    prepStmt.executeUpdate();
    	    rs = prepStmt.getGeneratedKeys();
    	    rs.next();
    	    int keyItem = rs.getInt(1);
    	    
    	    //Agrega presentaciones al item
    	    sql = "insert into PRESENTACIONES (NUMERO_UNIDADES,COSTO,PESO,ID_ITEM) values (1,6000,20,"+keyItem+")";
    	    prepStmt = consultaDAO.conexion.prepareStatement(sql);
    	    prepStmt.executeUpdate();

    	    sql = "insert into PRESENTACIONES (NUMERO_UNIDADES,COSTO,PESO,ID_ITEM) values (1,8000,40,"+keyItem+")";
    	    prepStmt = consultaDAO.conexion.prepareStatement(sql);
    	    prepStmt.executeUpdate();
    	    
    	    sql = "insert into PRESENTACIONES (NUMERO_UNIDADES,COSTO,PESO,ID_ITEM) values (1,4000,15,"+keyItem+")";
    	    prepStmt = consultaDAO.conexion.prepareStatement(sql);
    	    prepStmt.executeUpdate();
    	    
    	    //Agrega item a la bodega
    	    sql = "insert into ITEMS (ID_PRODUCTO,ID_LOCAL,ID_BODEGA,FECHA_INGRESO,NUM_PEDIDO_ADMIN_LOCAL,NUM_PEDIDO_POR_MAYOR,NUM_PEDIDO_PROVEEDOR)"
    	    		+" values (2,null,"+keyBodega+",'23/05/2014',null,null,null)";
    	    prepStmt = consultaDAO.conexion.prepareStatement(sql, new String[]{"ID"});
    	    prepStmt.executeUpdate();
    	    rs = prepStmt.getGeneratedKeys();
    	    rs.next();
    	    int keyItem2 = rs.getInt(1);
    	    
    	    //Agrega presentaciones al item
    	    sql = "insert into PRESENTACIONES (NUMERO_UNIDADES,COSTO,PESO,ID_ITEM) values (1,6500,15,"+keyItem2+")";
    	    prepStmt = consultaDAO.conexion.prepareStatement(sql);
    	    prepStmt.executeUpdate();
    	    
    	    sql = "insert into PRESENTACIONES (NUMERO_UNIDADES,COSTO,PESO,ID_ITEM) values (1,5000,10,"+keyItem2+")";
    	    prepStmt = consultaDAO.conexion.prepareStatement(sql);
    	    prepStmt.executeUpdate();
    	    
    	    String estado = "select i.id, i.ID_BODEGA, p.id as idPres, p.peso, b.ID_TIPO, b.capacidad from"
    	    		+ " items i  inner join PRESENTACIONES p on i.id = p.id_item and i.ID_BODEGA is not null"
    	    		+ " inner join bodegas b on b.ID = i.ID_BODEGA and b.ID = "+keyBodega+" order by ID_BODEGA, i.id";
    	    prepStmt = consultaDAO.conexion.prepareStatement(estado);
    	    rs = prepStmt.executeQuery();
    	    int suma = 0;
    	    while(rs.next())
    	    {
    	    	suma = suma + rs.getInt(4);
    	    }
    	    System.out.println("Peso inicial: "+suma);
    	    
    	    consultaDAO.conexion.commit();
    	    
    	    String s = consultaDAO.registrarBodega("Perecederos", 200);
    	    System.out.println(s);
    	        	    
    	   probarDespues(keyBodega);
    	    
    	}
    	
    	catch (SQLException e)
    	{
    	    try
    	    {
    		consultaDAO.conexion.rollback();
    	    }
    	    catch (SQLException e1)
    	    {
    		e1.printStackTrace();
    	    }
    	    e.printStackTrace();
    	    fail();
    	}
    	finally
    	{
    		try {
				prepStmt.close();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
    		try
    		{
    			consultaDAO.closeConnection(consultaDAO.conexion);
    		}
    		catch (Exception e)
    		{
    			e.printStackTrace();
    		}
    	}
    }

    public void probarDespues(int keyBodega)
    {
    	PreparedStatement prepStmt = null;
    	try
    	{
    		ResultSet rs = null;
    	    consultaDAO.establecerConexion();
    	    consultaDAO.conexion.setAutoCommit(false);	    
    		
    		String estadoFinal = "select i.id, i.ID_BODEGA, p.id as idPres, p.peso, b.ID_TIPO, b.capacidad from"
    				+ " items i  inner join PRESENTACIONES p on i.id = p.id_item and i.ID_BODEGA is not null"
    				+ " inner join bodegas b on b.ID = i.ID_BODEGA and b.ID = "+keyBodega+" order by ID_BODEGA, i.id";

    		prepStmt = consultaDAO.conexion.prepareStatement(estadoFinal);
    		rs = prepStmt.executeQuery();
    		int sumaFinal = 0;
    		while(rs.next())
    		{
    			sumaFinal = sumaFinal + rs.getInt(4);
    		}
    		System.out.println("Peso Final: "+sumaFinal);

    		
    		//Borra la bodega y los items que quedaron en ella
    		
    		String sql = "delete from bodegas where id  = "+keyBodega;
    		prepStmt = consultaDAO.conexion.prepareStatement(sql);
    		prepStmt.executeUpdate();
    		consultaDAO.conexion.commit();
    	}

    	catch (SQLException e)
    	{
    		try
    		{
    			consultaDAO.conexion.rollback();
    		}
    		catch (SQLException e1)
    		{
    			e1.printStackTrace();
    		}
    		e.printStackTrace();
    		fail();
    	}
    	finally
    	{

    		try
    		{
    			prepStmt.close();
    			consultaDAO.closeConnection(consultaDAO.conexion);
    		}
    		catch (Exception e)
    		{
    			e.printStackTrace();
    		}
    	}
    }


}
