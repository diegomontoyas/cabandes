/**
 * 
 */
package test;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import junit.framework.TestCase;
import vos.BodegaVO;
import vos.ItemVO;
import dao.ConsultaDAO;

/**
 * @author Diego
 *
 */
public class Loader extends TestCase
{
    /**
     * Instancia exclusiva para obtener una conexión para el Test. No deben
     * llamarse metodos de ConsultaDAO sobre ella.
     */
    ConsultaDAO conexionPruebas = new ConsultaDAO(".");
    
    ConsultaDAO consultaDAO = new ConsultaDAO(".");
    
    ResultSet rs = null;
    
    private double randomBetween(double Min, double Max)
    {
	return Min + (int)(Math.random() * ((Max - Min) + 1));
    }
    
    //NO PROBADO AÚN. 
    public void testLlenarAbsurdamenteLaBaseDeDatos()
    {		
	try
	{	
	    String respuesta1 = "";
	    int ii = 0;
	    
	    ArrayList<String> idsBodegas = new ArrayList<String>();
	    
	    conexionPruebas.establecerConexion();
	    conexionPruebas.conexion.setAutoCommit(false);
	    
	    String bodegas = "SELECT * FROM BODEGAS";
	    
	    PreparedStatement prepStmtBodegas = conexionPruebas.conexion.prepareStatement(bodegas);
	    rs = prepStmtBodegas.executeQuery();
	    
	    ArrayList<BodegaVO> bodegaVOs = new ArrayList<BodegaVO>();
	    
	    while (rs.next())
	    {
		//POR CADA BODEGA...
		
		int idBodega = rs.getInt("ID");
		int idTipoBodega = rs.getInt("ID_TIPO");
		idsBodegas.add(idBodega +"");
		
		BodegaVO bodegaVO = new BodegaVO();
		bodegaVO.id = idBodega;
		bodegaVO.tipo = idTipoBodega;	
		
		bodegaVOs.add(bodegaVO);
	    }
	    
	    String productos = "SELECT * FROM PRODUCTOS";
	    
	    PreparedStatement prepStmtProductos = conexionPruebas.conexion.prepareStatement(productos);
	    rs = prepStmtProductos.executeQuery();
	    
	    ArrayList<ItemVO> itemVOs = new ArrayList<ItemVO>();
	    
	    while (rs.next())
	    {
		//AGREGAR PRODUCTOS A BODEGAS 
		
		int idProducto = rs.getInt("ID");
		int idTipoProducto = rs.getInt("ID_TIPO");
		
		ItemVO itemVO = new ItemVO();
		itemVO.idProducto = idProducto;
		itemVO.tipo = idTipoProducto+"";
		
		itemVOs.add(itemVO);
	    }
	    
	    for (BodegaVO bodegaVO : bodegaVOs)
	    {	
		for (ItemVO itemVO : itemVOs)
		{
		    double numProductosAAgregar = randomBetween(600, 700);
		    
		    if (String.valueOf(bodegaVO.tipo).equals(itemVO.tipo))
		    {
			for(int i=0; i<numProductosAAgregar; i++)
			{    
			    String sqlInsertar = " INSERT INTO ITEMS (ID_PRODUCTO, ID_BODEGA) VALUES ('"+itemVO.idProducto+"', '"+bodegaVO.id+"'); ";
			    respuesta1 += sqlInsertar; 
			    ii++;
			    System.out.println(ii);
			    
			    /*ItemVO itemAAgregar = new ItemVO();
			    itemAAgregar.idProducto = idProducto;
			    
			    ItemVO itemVO = consultaDAO.registrarEntradaItemABodega(idBodega, itemAAgregar, false);
			    
			    double numPresentacionesAAgregar = randomBetween(1, 10);
			    
			    ii++;
			    System.out.println(ii);
			    
			    if (itemVO != null)
			    {
				for(int j=0; j<numPresentacionesAAgregar; j++)
				{
				    
				    //Y LUEGO AGREGAR PRESENTACIONES A CADA ITEM
				    
				    String sql = "INSERT INTO PRESENTACIONES (NUMERO_UNIDADES, COSTO, PESO, ID_ITEM) " + "VALUES ('"+ randomBetween(1, 300) +"', '"+randomBetween(1000, 10000)+"', '"+randomBetween(0.5, 2)+"', '" + itemVO.ID + "')";
				    PreparedStatement preparedStatementPresentaciones = conexionPruebas.conexion.prepareStatement(sql);
				    preparedStatementPresentaciones.executeUpdate();
				    preparedStatementPresentaciones.close();		
				}
			    }
			    else
			    {
				break; //Pasar al siguiente producto porque este no es del tipo de la bodega
			    }*/
			    if (ii >= 400000) break;
			}
		    }
		    if (ii >= 400000) break;
		}
		if (ii >= 400000) break;
	    }	    
	    
	    conexionPruebas.conexion.commit();
	    
	    /*//AGREGAR UNAS POCAS ENTRADAS AL HISTORIAL
	    
	    PreparedStatement prepStmt = null;
	    
	    for (int i = 0; i < 500000; i++)
	    {
		System.out.println("Historial: "+i);
		
		int e = (int) randomBetween(1, 5);
		
		if (e == 1)
		{
		    String agregarEntrada = "INSERT INTO HISTORIAL_MOVIMIENTOS (ID_PRODUCTO, ID_BODEGA_ORIGEN, CANTIDAD_TOTAL) "
			    + " VALUES ('"+randomBetween(1, 60)+"', '"+idsBodegas.get(i%idsBodegas.size())+"','"+randomBetween(1, 50)+"') ";
		    
		    prepStmt = conexionPruebas.conexion.prepareStatement(agregarEntrada);
		    prepStmt.executeUpdate();
		}
		else if (e == 2)
		{
		    String agregarEntrada = "INSERT INTO HISTORIAL_MOVIMIENTOS (ID_PRODUCTO, ID_BODEGA_DESTINO, CANTIDAD_TOTAL) "
			    + " VALUES ('"+randomBetween(1, 60)+"', '"+idsBodegas.get(i%idsBodegas.size())+"','"+randomBetween(1, 50)+"') ";
		    
		    prepStmt = conexionPruebas.conexion.prepareStatement(agregarEntrada);
		    prepStmt.executeUpdate();
		}
		else if (e == 3)
		{
		    String agregarEntrada = "INSERT INTO HISTORIAL_MOVIMIENTOS (ID_PRODUCTO, ID_LOCAL_ORIGEN, CANTIDAD_TOTAL) "
			    + " VALUES ('"+randomBetween(1, 60)+"', '"+randomBetween(1, 40)+"','"+randomBetween(1, 50)+"') ";
		    
		    prepStmt = conexionPruebas.conexion.prepareStatement(agregarEntrada);
		    prepStmt.executeUpdate();
		}
		if (e == 4)
		{
		    String agregarEntrada = "INSERT INTO HISTORIAL_MOVIMIENTOS (ID_PRODUCTO, ID_BODEGA_ORIGEN, ID_LOCAL_DESTINO, CANTIDAD_TOTAL) "
			    + " VALUES ('"+randomBetween(1, 60)+"', '"+idsBodegas.get(i%idsBodegas.size())+"', '"+randomBetween(1, 40)+"','"+randomBetween(1, 50)+"') ";
		    
		    prepStmt = conexionPruebas.conexion.prepareStatement(agregarEntrada);
		    prepStmt.executeUpdate();
		}
		if (e== 5)
		{
		    String agregarEntrada = "INSERT INTO HISTORIAL_MOVIMIENTOS (ID_PRODUCTO, ID_BODEGA_ORIGEN, ID_BODEGA_DESTINO, CANTIDAD_TOTAL) "
			    + " VALUES ('"+randomBetween(1, 60)+"', '"+idsBodegas.get(i%idsBodegas.size())+"', '"+idsBodegas.get((i+20)%idsBodegas.size())+"', '"+randomBetween(1, 50)+"') ";
		    
		    prepStmt = conexionPruebas.conexion.prepareStatement(agregarEntrada);
		    prepStmt.executeUpdate();
		}
		prepStmt.close();
	    }
	    conexionPruebas.conexion.commit();*/
	    
	}
	catch(Exception e)
	{
	    e.printStackTrace();
	    
	    try
	    {
		conexionPruebas.conexion.rollback();
	    }
	    catch (SQLException e1)
	    {
		e1.printStackTrace();
	    }
	}
    }
}
