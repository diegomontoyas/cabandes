/**
 * 
 */
package test;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import junit.framework.TestCase;
import dao.ConsultaDAO;

/**
 * @author Diego
 * 
 */
public class TestIteracion4 extends TestCase
{
	ConsultaDAO consultaDAO = new ConsultaDAO(".");

	/**
	 * Instancia exclusiva para obtener una conexión para el Test. No deben
	 * llamarse metodos de ConsultaDAO sobre ella.
	 */
	ConsultaDAO conexionPruebas = new ConsultaDAO(".");

	ArrayList<PreparedStatement> prepStmts = new ArrayList<PreparedStatement>();
	ResultSet rs = null;

	public ArrayList<String> construirEscenario1()
	{		
		ArrayList<String> idsProductosAgregados = new ArrayList<String>();


		return idsProductosAgregados;
	}

	public void testVisualizarMovimientos1()
	{
		String idBodegaAgregada = "";
		ArrayList<String> ids = new ArrayList<String>();

		try
		{
			//CONFIGURACIÓN ESCENARIO PRUEBA

			conexionPruebas = new ConsultaDAO(".");
			conexionPruebas.establecerConexion();

			PreparedStatement prepStmt = null;

			String sql = "INSERT INTO BODEGAS (CAPACIDAD, ID_TIPO) VALUES ('4444', '1')";

			prepStmt = conexionPruebas.conexion.prepareStatement(sql, new String[] { "ID" });
			prepStmts.add(prepStmt);
			int i = prepStmt.executeUpdate();

			if (i > 0)
			{
				rs = prepStmt.getGeneratedKeys();
				while (rs.next())
				{
					idBodegaAgregada = rs.getString(1);
				}
				System.out.println(idBodegaAgregada);
			}

			sql = "INSERT INTO ITEMS (ID_PRODUCTO, ID_LOCAL) VALUES ('1', '1')";
			prepStmt = conexionPruebas.conexion.prepareStatement(sql, new String[] { "ID" });
			prepStmts.add(prepStmt);
			i = prepStmt.executeUpdate();

			if (i > 0)
			{
				rs = prepStmt.getGeneratedKeys();
				while (rs.next())
				{
					ids.add(rs.getString(1));
					int idItem = Integer.parseInt(rs.getString(1));

					sql = "INSERT INTO PRESENTACIONES (NUMERO_UNIDADES, COSTO, PESO, ID_ITEM) " + "VALUES ('10', '1200', '10', '" + idItem + "')";
					prepStmt = conexionPruebas.conexion.prepareStatement(sql, new String[] { "ID" });
					prepStmts.add(prepStmt);
					i = prepStmt.executeUpdate();

					if (i > 0)
					{
						rs = prepStmt.getGeneratedKeys();
						while (rs.next())
						{
							ids.add(rs.getString(1));
						}
					}
				}
			}


			sql = "INSERT INTO ITEMS (ID_PRODUCTO, ID_BODEGA) VALUES ('1', '" + idBodegaAgregada + "')";
			prepStmt = conexionPruebas.conexion.prepareStatement(sql, new String[] { "ID" });
			prepStmts.add(prepStmt);
			i = prepStmt.executeUpdate();

			if (i > 0)
			{
				rs = prepStmt.getGeneratedKeys();
				while (rs.next())
				{
					ids.add(rs.getString(1));
					int idItem = Integer.parseInt(rs.getString(1));

					sql = "INSERT INTO PRESENTACIONES (NUMERO_UNIDADES, COSTO, PESO, ID_ITEM) " + "VALUES ('10', '1200', '10', '" + idItem + "')";
					prepStmt = conexionPruebas.conexion.prepareStatement(sql);
					prepStmts.add(prepStmt);
					prepStmt.executeUpdate();
				}
			}

			sql = " INSERT INTO ITEMS (ID_PRODUCTO, ID_BODEGA) VALUES ('2', '" + idBodegaAgregada + "')";
			prepStmt = conexionPruebas.conexion.prepareStatement(sql, new String[] { "ID" });
			prepStmts.add(prepStmt);
			i = prepStmt.executeUpdate();

			if (i > 0)
			{
				rs = prepStmt.getGeneratedKeys();
				while (rs.next())
				{
					ids.add(rs.getString(1));
				}
			}			

			//PRUEBAS FUNCIONALES DEL HISTORIAL DE MOVIMIENTOS

			String idBodegaDestino = "1";

			boolean pudoVenderse = consultaDAO.registrarVentaPresentacion(ids.get(1));

			boolean pudoMoverseBodegaALocal = consultaDAO.registrarMovimientoItemBodegaALocal(ids.get(2), idBodegaDestino);

			boolean pudoVenderseDesdeLocal = consultaDAO.registrarVentaItemACompradorMayorista(ids.get(3)); //<<--TIENE QUE SER DE UN ITEM QUE ESTABA EN BODEGA

			if (pudoVenderse && pudoMoverseBodegaALocal && pudoVenderseDesdeLocal)
			{
				sql = " SELECT * FROM HISTORIAL_MOVIMIENTOS WHERE ID_ITEM = '" + ids.get(0) + "'";
				prepStmt = conexionPruebas.conexion.prepareStatement(sql);
				prepStmts.add(prepStmt);
				rs = prepStmt.executeQuery();

				boolean esta = false;

				while (rs.next())
				{
					esta = true;
				}
				if (!esta) fail();

				sql = " SELECT * FROM HISTORIAL_MOVIMIENTOS WHERE ID_ITEM = '" + ids.get(2) + "'";
				prepStmt = conexionPruebas.conexion.prepareStatement(sql);
				prepStmts.add(prepStmt);
				rs = prepStmt.executeQuery();

				esta = false;

				while (rs.next())
				{
					esta = true;
				}
				if (!esta) fail();

				sql = " SELECT * FROM HISTORIAL_MOVIMIENTOS WHERE ID_ITEM = '" + ids.get(3) + "'";
				prepStmt = conexionPruebas.conexion.prepareStatement(sql);
				prepStmts.add(prepStmt);
				rs = prepStmt.executeQuery();

				esta = false;

				while (rs.next())
				{
					esta = true;
				}
				if (!esta) fail();
			}
			else
			{
				fail();
			}
		}
		catch (SQLException e)
		{
			try
			{
				conexionPruebas.conexion.rollback();
			}
			catch (SQLException e1)
			{
				e1.printStackTrace();
			}
			e.printStackTrace();
		}
		finally
		{
			//Cerrar statements
			for (PreparedStatement prepStmt : prepStmts)
			{
				if (prepStmt != null)
					try
				{
						prepStmt.close();
				}
				catch (SQLException exception)
				{

				}
			}
			//reiniciar prepStmts

			prepStmts = new ArrayList<PreparedStatement>();

			try
			{
				conexionPruebas.closeConnection(conexionPruebas.conexion);
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}

	}
	
	public void testTiempos()
	{
		try
		{
			conexionPruebas = new ConsultaDAO(".");
			conexionPruebas.establecerConexion();

			PreparedStatement prepStmt = null;

			//PRUEBAS TEMPORALES

			SimpleDateFormat dt = new SimpleDateFormat("dd-MMM-yy"); 

			String min = " SELECT MIN(FECHA) AS FECHA FROM HISTORIAL_MOVIMIENTOS";
			prepStmt = conexionPruebas.conexion.prepareStatement(min);
			prepStmts.add(prepStmt);
			rs = prepStmt.executeQuery();

			String fechaMin = null;

			while (rs.next())
			{
				fechaMin = dt.format(rs.getDate("FECHA"));
			}

			String max = " SELECT MAX(FECHA) AS FECHA FROM HISTORIAL_MOVIMIENTOS";
			prepStmt = conexionPruebas.conexion.prepareStatement(max);
			prepStmts.add(prepStmt);
			rs = prepStmt.executeQuery();

			String fechaMax = null;

			while (rs.next())
			{
				fechaMax = dt.format(rs.getDate("FECHA"));
			}
			System.out.println(fechaMin);
			System.out.println(fechaMax);

			//REQ 1: HISTORIAL MOVIMIENTOS

			consultaDAO.consultarHistorialMovimientosConBodegaDestino(fechaMin, fechaMax, "2", false);	
			
			consultaDAO.consultarHistorialMovimientosConBodegaDestino("01-Feb-13", "01-May-14", "2", false);	
			
			consultaDAO.consultarHistorialMovimientosDeProducto(fechaMin, fechaMax, "Manzana", false);
			
			consultaDAO.consultarHistorialMovimientosDeProducto("01-Feb-13", "01-May-14", "Manzana", false);

			//REQ 2: HISTORIAL MOVIMIENTOS NEGADO

			consultaDAO.consultarHistorialMovimientosConBodegaDestino(fechaMin, fechaMax, "3", true);
			
			consultaDAO.consultarHistorialMovimientosConBodegaDestino("01-Feb-13", "01-May-14", "2", true);	
			
			consultaDAO.consultarHistorialMovimientosDeProducto(fechaMin, fechaMax, "Manzana", true);

			consultaDAO.consultarHistorialMovimientosDeProducto("01-Feb-13", "01-May-14", "Manzana", true);

			//REQ 3: VISUALIZAR BODEGAS 2.0

			consultaDAO.consultarBodegas("Perecederos", 100);

			consultaDAO.consultarBodegas("No Perecederos", 100);

			consultaDAO.consultarBodegas("Refrigerados", 100);
		}
		catch (SQLException e)
		{
			try
			{
				conexionPruebas.conexion.rollback();
			}
			catch (SQLException e1)
			{
				e1.printStackTrace();
			}
			e.printStackTrace();
		}
		finally
		{
			//Cerrar statements
			for (PreparedStatement prepStmt : prepStmts)
			{
				if (prepStmt != null)
					try
				{
						prepStmt.close();
				}
				catch (SQLException exception)
				{

				}
			}
			//reiniciar prepStmts

			prepStmts = new ArrayList<PreparedStatement>();

			try
			{
				conexionPruebas.closeConnection(conexionPruebas.conexion);
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}

	}
	
	 public void testProbarPedidoLocal()
   {
   	PreparedStatement prepStmt = null;
   	try
   	{
   	    
   	    conexionPruebas = new ConsultaDAO("/Applications/eclipse/jboss-as-7.1.1.Final/standalone/deployments/Cabandes.war");
   	    conexionPruebas.establecerConexion();
   	    
   	    conexionPruebas.visualizarLocales(5, "01/03/14", "01/05/14");
   	    long tiempo = conexionPruebas.tiempoFinal;
   	    int tamanho = conexionPruebas.tamanho;
   	    System.out.println(tiempo +";"+ tamanho);
   	    
   	    conexionPruebas.visualizarLocales(13, "01/03/14", "01/05/14");
   	    long tiempo2 = conexionPruebas.tiempoFinal;
   	    int tamanho2 = conexionPruebas.tamanho;
   	    System.out.println(tiempo2 + ";"+ tamanho2);
   	    
   	    conexionPruebas.visualizarLocales(2, "01/12/12", "07/12/12");
   	    long tiempo3 = conexionPruebas.tiempoFinal;
   	    int tamanho3 = conexionPruebas.tamanho;
   	    System.out.println(tiempo3 + ";"+ tamanho3);
   	    
   	    conexionPruebas.visualizarLocales(2, "01/02/11", "07/05/14");
   	    long tiempo4 = conexionPruebas.tiempoFinal;
   	    int tamanho4 = conexionPruebas.tamanho;
   	    System.out.println(tiempo4 + ";"+ tamanho4);
   	    
   	 
   	}
   	    
   	catch (Exception e)
   	{
   	    e.printStackTrace();
   	}
   	finally
   	{
   	    if (prepStmt != null)
   		try
   		{
   		    prepStmt.close();
   		}
   		catch (SQLException exception)
   		{

   		}
   	    try
   	    {
   		conexionPruebas.closeConnection(conexionPruebas.conexion);
   	    }
   	    catch (Exception e)
   	    {
   		e.printStackTrace();
   	    }
   	}
   	
   }
   

}
